import React from 'react'
import axios from 'axios'
import { FormattedMessage } from 'react-intl'
import { Message } from 'semantic-ui-react'

import ResourceState from 'lib/store/resourceState'
import { setCSRFToken } from 'lib/store/slices/authSlice'
import Spinner from 'lib/components/spinner'
import EmailLink from 'lib/components/emailLink'

import { getPublicConfig } from './config'
import Query from './query'

/**
 * Utility function make requests to the backend API.
 *
 * @param {object}      action    Action data to construct the request
 * @param {string|null} csrfToken CSRF token to use or null to request a new token
 * @param {function}    dispatch  Dispatch function to use for interacting with the redux store
 */
const makeApiRequest = (action, csrfToken, dispatch) => {
  const {
    resource,
    method = 'GET',
    query = {},
    dataType = 'application/json',
    data = {},
  } = action

  const { apiUrl } = getPublicConfig()

  // Prepend prefix for requests
  const url = `${apiUrl}/${resource}?${Query.buildQueryString(query)}`

  let promise

  // These actions require a CSRF Token header
  if (['POST', 'PATCH', 'DELETE'].includes(method)) {
    const makeRequest = preparedCsrfToken => {
      switch (method) {
        case 'POST':
          return axios.post(url, data, {
            headers: {
              'Content-Type': dataType,
              'X-CSRFToken': preparedCsrfToken,
            },
            withCredentials: true,
          })
        case 'PATCH':
          return axios.patch(url, data, {
            headers: {
              'Content-Type': dataType,
              'X-CSRFToken': preparedCsrfToken,
            },
            withCredentials: true,
          })
        case 'DELETE':
          return axios.delete(url, {
            headers: {
              'X-CSRFToken': preparedCsrfToken,
            },
            withCredentials: true,
          })
        default:
          return Promise.reject(new Error(`Unknown method: ${method}`))
      }
    }

    if (!csrfToken) {
      promise = axios
        .get(`${apiUrl}/auth/getcsrf`, { withCredentials: true })
        .then(response => {
          const newCsrfToken = response.headers['x-csrftoken']
          dispatch(setCSRFToken(newCsrfToken))
          return makeRequest(newCsrfToken)
        })
    } else {
      promise = makeRequest(csrfToken)
    }
  } else if (method === 'GET') {
    promise = axios.get(url, { withCredentials: true })
  } else {
    promise = Promise.reject(new Error(`Unknown method: ${method}`))
  }

  return promise
}

/**
 * Ensures that the data is loaded.
 * Returns loading or error content otherwise.
 *
 * @param {array} list Response from custom store hooks with item order [storedItem, status, error, reloadFunction]
 */
const requireDataLoaded = list => {
  if (
    list.some(
      ([, state]) =>
        state === ResourceState.Unknown || state === ResourceState.Loading
    )
  ) {
    return (
      <div>
        <Spinner centered size="large" />
      </div>
    )
  }

  const reloadItems = () => {
    list.forEach(([, state, , reload]) => {
      if (state === ResourceState.Error) {
        reload()
      }
    })
  }

  const errorItem = list.find(([, state]) => state === ResourceState.Error)

  if (errorItem) {
    const [, , error] = errorItem
    return (
      <Message negative size="large">
        <FormattedMessage
          id="errorLoadingData"
          values={{
            code: (error && error.code) || 0,
            button: reloadItems,
          }}
          defaultMessage={
            <FormattedMessage
              id="errorUnknown"
              values={{ code: error.code, contactEmail: <EmailLink /> }}
            />
          }
        />
      </Message>
    )
  }

  return null
}

export { makeApiRequest, requireDataLoaded }
