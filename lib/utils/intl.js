const removeLocalePart = pathname => {
  const i = pathname.indexOf(`/`, 1)
  return pathname.substring(i)
}

export { removeLocalePart }
