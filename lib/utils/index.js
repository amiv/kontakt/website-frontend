const isBrowser = typeof window !== `undefined`

const isDevEnvironment =
  process.env.NODE_ENV !== 'production' &&
  !process.env.REACT_PRODUCTION_DEBUGGING

const isObject = value => typeof value === 'object' && value !== null

const translateMessage = (messages, intl) => {
  if (intl.locale in messages) {
    return messages[intl.locale]
  }

  if (intl.defaultLocale in messages) {
    return messages[intl.defaultLocale]
  }
  const keys = Object.keys(messages)
  if (keys.length > 0) {
    const language = Object.keys(messages)[0]
    return messages[language]
  }
  return ''
}

const sleep = milliseconds => {
  // eslint-disable-next-line no-promise-executor-return
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const rateLimit = (func, wait) => {
  let timeout
  let lastCalled = 0
  return function outer(...args) {
    const context = this
    const callFunc = () => {
      timeout = null
      lastCalled = Date.now()
      func.apply(context, args)
    }
    const callNow = Date.now() - wait > lastCalled
    clearTimeout(timeout)

    if (callNow) {
      callFunc()
    } else {
      timeout = setTimeout(callFunc, wait)
    }
  }
}

export {
  isBrowser,
  isDevEnvironment,
  isObject,
  sleep,
  rateLimit,
  translateMessage,
}
