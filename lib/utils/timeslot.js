const getTimeslot = (availableTimeslots, day, boothSize, category) => {
  return availableTimeslots.find(
    timeslot =>
      timeslot.day === day &&
      timeslot.size === boothSize &&
      timeslot.category === category
  )
}

export { getTimeslot }
