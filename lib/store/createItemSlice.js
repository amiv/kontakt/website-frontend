/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import ResourceState from './resourceState'
import { setLogoutSuccess } from './slices/authSlice'

const initialBaseState = {
  item: null,
  state: ResourceState.Unknown,
  error: null,
}

const createItemSlice = ({
  name,
  isConfidential = false,
  initialState = {},
  reducers = {},
}) => {
  const extraReducers =
    isConfidential &&
    (builder => {
      builder.addCase(setLogoutSuccess, state => {
        Object.assign(state, initialState)
      })
    })

  const slice = createSlice({
    name,
    initialState: { ...initialBaseState, ...initialState },
    reducers: {
      setStart: state => {
        state.state = ResourceState.Loading
      },
      setSuccess: (state, { payload: item }) => {
        state.item = item
        state.error = null
        state.state = ResourceState.Loaded
      },
      setFailure: (state, { payload: error }) => {
        state.error = error
        state.state = ResourceState.Error
      },
      ...reducers,
    },
    extraReducers: {
      ...extraReducers,
      [HYDRATE]: (state, action) => {
        return {
          ...state,
          ...action.payload[name],
        }
      },
    },
  })

  slice.selectors = {
    selectItem: state => state[name].item,
    selectState: state => state[name].state,
    selectError: state => state[name].error,
  }

  return slice
}

export default createItemSlice
