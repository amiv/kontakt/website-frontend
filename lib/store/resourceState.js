// ResourceState enum
const ResourceState = Object.freeze({
  Unknown: 'unknown',
  Loading: 'loading',
  Error: 'error',
  Loaded: 'loaded',
})

export default ResourceState
