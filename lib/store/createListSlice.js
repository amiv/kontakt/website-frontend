/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import ResourceState from './resourceState'
import { setLogoutSuccess } from './slices/authSlice'

const initialBaseState = {
  items: [],
  state: ResourceState.Unknown,
  error: null,
}

const createListSlice = ({
  name,
  isConfidential = false,
  initialState = {},
  reducers = {},
}) => {
  const extraReducers =
    isConfidential &&
    (builder => {
      builder.addCase(setLogoutSuccess, state => {
        Object.assign(state, initialState)
      })
      builder.addCase(HYDRATE, (state, action) => {
        return {
          ...state,
          ...action.payload[name],
        }
      })
    })

  const slice = createSlice({
    name,
    initialState: { ...initialBaseState, ...initialState },
    reducers: {
      setStart: state => {
        state.state = ResourceState.Loading
      },
      setSuccess: (state, { payload: items }) => {
        state.items = items
        state.error = null
        state.state = ResourceState.Loaded
      },
      setFailure: (state, { payload: error }) => {
        state.error = error
        state.state = ResourceState.Error
      },
      ...reducers,
    },
    extraReducers,
  })

  slice.selectors = {
    selectItems: state => state[name].items,
    selectState: state => state[name].state,
    selectError: state => state[name].error,
  }

  return slice
}

export default createListSlice
