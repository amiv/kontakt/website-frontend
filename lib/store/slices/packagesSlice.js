import createListSlice from '../createListSlice'

const packages = createListSlice({ name: 'packages' })

export const { setStart, setSuccess, setFailure } = packages.actions

export default packages.reducer

// actions
/**
 * Load data for all past fairs
 */
export const loadPackages = () => dispatch => {
  return dispatch({
    types: [setStart(), setSuccess(), setFailure()],
    resource: 'packages/current',
    method: 'GET',
  })
}

// selectors
export const {
  selectItems: selectPackages,
  selectState,
  selectError,
} = packages.selectors
