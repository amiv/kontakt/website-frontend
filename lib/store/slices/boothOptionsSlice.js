import createListSlice from '../createListSlice'

const boothOptions = createListSlice({ name: 'booth_options' })

export const { setStart, setSuccess, setFailure } = boothOptions.actions

export default boothOptions.reducer

// actions
export const loadBoothOptions =
  (silent = false) =>
  dispatch => {
    let types

    if (silent) {
      types = [null, setSuccess(), null]
    } else {
      types = [setStart(), setSuccess(), setFailure()]
    }

    return dispatch({
      types,
      resource: 'booths/options',
      method: 'GET',
    })
  }

// selectors
export const {
  selectItems: selectBoothOptions,
  selectState,
  selectError,
} = boothOptions.selectors
