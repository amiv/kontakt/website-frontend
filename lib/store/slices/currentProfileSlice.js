/* eslint-disable no-param-reassign */
import createItemSlice from '../createItemSlice'
import ResourceState from '../resourceState'

const currentProfile = createItemSlice({
  name: 'current_profile',
  isConfidential: true,
  reducers: {
    setFailure: (state, { payload: error }) => {
      // NotFound is an expected error when no signup exists for the current fair.
      if (error && error.code === 404) {
        state.error = undefined
        state.state = ResourceState.Loaded
      } else {
        state.error = error
        state.state = ResourceState.Error
      }
    },
  },
})

export const { setStart, setSuccess, setFailure } = currentProfile.actions

export default currentProfile.reducer

// actions
/**
 * Load data for current fair
 */
export const loadCurrentProfile = () => dispatch => {
  return dispatch({
    types: [setStart(), setSuccess(), setFailure()],
    resource: 'company_profiles/current_for_user',
    method: 'GET',
  })
}

export const setCurrentProfile = profile => setSuccess(profile)

// selectors
export const {
  selectItem: selectCurrentProfile,
  selectState,
  selectError,
} = currentProfile.selectors
