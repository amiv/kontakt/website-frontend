import createItemSlice from '../createItemSlice'

const company = createItemSlice({ name: 'company', isConfidential: true })

export const { setStart, setSuccess, setFailure } = company.actions

export default company.reducer

// actions
/**
 * Load data for given company
 */
export const loadCompany = companyId => dispatch => {
  return dispatch({
    types: [setStart(), setSuccess(), setFailure()],
    resource: `companies/${companyId}`,
    method: 'GET',
  })
}

/**
 * Update company information
 * @param {integer} companyId
 * @param {object} data company data (billing information and correspondence email address)
 */
export const updateCompany =
  (
    companyId,
    {
      billing_recipient,
      billing_address_street,
      billing_address_postal_code,
      billing_address_city,
      billing_address_country,
      billing_comment,
      correspondence_email_address,
    }
  ) =>
  dispatch => {
    return dispatch({
      types: [setStart(), setSuccess(), setFailure()],
      resource: `companies/${companyId}`,
      method: 'PATCH',
      data: {
        billing_recipient,
        billing_address_street,
        billing_address_postal_code,
        billing_address_city,
        billing_address_country,
        billing_comment,
        correspondence_email_address,
      },
    })
  }

// selectors
export const {
  selectItem: selectCompany,
  selectState,
  selectError,
} = company.selectors
