import createListSlice from '../createListSlice'

const pastParticipations = createListSlice({
  name: 'past_participations',
  isConfidential: true,
})

export const { setStart, setSuccess, setFailure } = pastParticipations.actions

export default pastParticipations.reducer

// actions
/**
 * Load data for all past participations
 */
export const loadPastParticipations = () => dispatch => {
  return dispatch({
    types: [setStart(), setSuccess(), setFailure()],
    resource: 'participations/past',
    method: 'GET',
  })
}

// selectors
export const {
  selectItems: selectPastParticipations,
  selectState,
  selectError,
} = pastParticipations.selectors
