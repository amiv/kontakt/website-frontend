import createListSlice from '../createListSlice'

const booths = createListSlice({ name: 'booths' })

export const { setStart, setSuccess, setFailure } = booths.actions

export default booths.reducer

// actions
/**
 * Load data for all past fairs
 */
export const loadBooths =
  (silent = false) =>
  dispatch => {
    let types

    if (silent) {
      types = [null, setSuccess(), null]
    } else {
      types = [setStart(), setSuccess(), setFailure()]
    }

    return dispatch({
      types,
      resource: 'booths/available',
      method: 'GET',
    })
  }

// selectors
export const {
  selectItems: selectAvailableBooths,
  selectState,
  selectError,
} = booths.selectors
