/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit'
import ResourceState from '../resourceState'

const initialState = {
  isAuthenticated: false,
  csrfToken: null,
  user: null,
  state: ResourceState.Unknown,
  error: null,
}

const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setStart: state => {
      state.state = ResourceState.Loading
    },
    setLoginSuccess: (state, { payload: user }) => {
      state.isAuthenticated = true
      state.user = user
      state.error = null
      state.state = ResourceState.Loaded
    },
    setLogoutSuccess: state => {
      // Reset state to initialState
      state.isAuthenticated = initialState.isAuthenticated
      state.csrfToken = initialState.csrfToken
      state.user = initialState.user
      state.state = initialState.state
      state.error = initialState.error
    },
    setCSRFToken: (state, { payload: token }) => {
      state.csrfToken = token
    },
    setLoginCheckFailure: state => {
      state.state = ResourceState.Loaded
    },
    setFailure: (state, { payload: error }) => {
      state.error = error
      state.state = ResourceState.Error
    },
  },
})

export const {
  setStart,
  setLoginSuccess,
  setLogoutSuccess,
  setCSRFToken,
  setLoginCheckFailure,
  setFailure,
} = auth.actions

export default auth.reducer

// actions
/**
 * Logout authenticated user
 * @param {boolean} force clear local data only. Do not logout at server.
 */
export const logout =
  (force = false) =>
  (dispatch, getState) => {
    const { auth: { user } = {} } = getState()
    if (force || !user) {
      return dispatch(setLogoutSuccess())
    }
    return dispatch({
      types: [setStart(), setLogoutSuccess(), setFailure()],
      disableAuthHandling: true,
      resource: 'auth/logout',
      method: 'DELETE',
    })
  }

/**
 * Login with given credentials
 * @param {string} email    email address to identify the user account
 * @param {string} password password to identify the user
 */
export const login = (email, password) => dispatch => {
  return dispatch({
    types: [setStart(), setLoginSuccess(), setFailure()],
    disableAuthHandling: true,
    resource: 'auth/login',
    method: 'POST',
    data: { email, password },
  })
}

/**
 * Check if there is an active user session
 */
export const checkLogin = () => dispatch => {
  return dispatch({
    types: [setStart(), setLoginSuccess(), setLoginCheckFailure()],
    disableAuthHandling: true,
    resource: 'auth/check',
    method: 'GET',
  })
}

// selectors
export const selectIsAuthenticated = state => state.auth.isAuthenticated
export const selectAuthenticatedUser = state => state.auth.user
export const selectState = state => state.auth.state
export const selectError = state => state.auth.error
export const selectCSRFToken = state => state.auth.state.csrfToken
