/* eslint-disable no-param-reassign */
import createItemSlice from '../createItemSlice'
import ResourceState from '../resourceState'

const currentParticipation = createItemSlice({
  name: 'current_participation',
  isConfidential: true,
  reducers: {
    setFailure: (state, { payload: error }) => {
      // NotFound is an expected error when no signup exists for the current fair.
      if (error && error.code === 404) {
        state.error = undefined
        state.state = ResourceState.Loaded
      } else {
        state.error = error
        state.state = ResourceState.Error
      }
    },
  },
})

export const { setStart, setSuccess, setFailure } = currentParticipation.actions

export default currentParticipation.reducer

// actions
/**
 * Load data for current fair
 */
export const loadCurrentParticipation = () => dispatch => {
  return dispatch({
    types: [setStart(), setSuccess(), setFailure()],
    resource: 'participations/current',
    method: 'GET',
  })
}

export const setCurrentParticipation = participation =>
  setSuccess(participation)

// selectors
export const {
  selectItem: selectCurrentParticipation,
  selectState,
  selectError,
} = currentParticipation.selectors
