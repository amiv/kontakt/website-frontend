import createListSlice from '../createListSlice'

const pastFairs = createListSlice({ name: 'past_fairs' })

export const { setStart, setSuccess, setFailure } = pastFairs.actions

export default pastFairs.reducer

// actions
/**
 * Load data for all past fairs
 */
export const loadPastFairs = () => dispatch => {
  return dispatch({
    types: [setStart(), setSuccess(), setFailure()],
    resource: 'fairs/past',
    method: 'GET',
  })
}

// selectors
export const {
  selectItems: selectPastFairs,
  selectState,
  selectError,
} = pastFairs.selectors
