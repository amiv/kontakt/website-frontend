import createItemSlice from '../createItemSlice'

const currentFair = createItemSlice({ name: 'current_fair' })

export const { setStart, setSuccess, setFailure } = currentFair.actions

export default currentFair.reducer

// actions
/**
 * Load data for current fair
 */
export const loadCurrentFair = () => dispatch => {
  return dispatch({
    types: [setStart(), setSuccess(), setFailure()],
    resource: 'fairs/current',
    method: 'GET',
  })
}

// selectors
export const {
  selectItem: selectCurrentFair,
  selectState,
  selectError,
} = currentFair.selectors
