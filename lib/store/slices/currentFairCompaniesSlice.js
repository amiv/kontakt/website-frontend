import createItemSlice from '../createItemSlice'

const currentFairCompanies = createItemSlice({
  name: 'current_fair_companies',
})

export const { setStart, setSuccess, setFailure } = currentFairCompanies.actions

export default currentFairCompanies.reducer

// actions
/**
 * Load data for companies participating at the current fair
 */
export const loadCurrentFairCompanies = () => dispatch => {
  return dispatch({
    types: [setStart(), setSuccess(), setFailure()],
    resource: `fairs/current/companies`,
    method: 'GET',
  })
}

// selectors
export const {
  selectItem: selectCurrentFairCompanies,
  selectState,
  selectError,
} = currentFairCompanies.selectors
