import { configureStore, combineReducers } from '@reduxjs/toolkit'
import thunk from 'redux-thunk'
import { createWrapper } from 'next-redux-wrapper'

import { isBrowser } from 'lib/utils'
import apiMiddleware from './apiMiddleware'
import authReducer from './slices/authSlice'
import currentFairReducer from './slices/currentFairSlice'
import pastFairsReducer from './slices/pastFairsSlice'
import currentFairCompaniesReducer from './slices/currentFairCompaniesSlice'
import currentParticipationReducer from './slices/currentParticipationSlice'
import pastParticipationsReducer from './slices/pastParticipationsSlice'
import currentProfileReducer from './slices/currentProfileSlice'
import boothOptionsReducer from './slices/boothOptionsSlice'
import boothsReducer from './slices/boothsSlice'
import companyReducer from './slices/companySlice'
import packagesReducer from './slices/packagesSlice'

let store

// preloadedState will be passed in by the plugin
const initStore = preloadedState => {
  const rootReducer = combineReducers({
    auth: authReducer,
    current_fair: currentFairReducer,
    past_fairs: pastFairsReducer,
    current_fair_companies: currentFairCompaniesReducer,
    current_participation: currentParticipationReducer,
    past_participations: pastParticipationsReducer,
    current_profile: currentProfileReducer,
    booth_options: boothOptionsReducer,
    booths: boothsReducer,
    company: companyReducer,
    packages: packagesReducer,
  })

  const middleware = [apiMiddleware, thunk]

  // In development, use the browser's Redux dev tools extension if installed
  const isDevelopment = process.env.NODE_ENV === 'development'
  const applyDevTools = isDevelopment && isBrowser && window.devToolsExtension

  return configureStore({
    reducer: rootReducer,
    middleware,
    preloadedState,
    devTools: applyDevTools,
  })
}

export const initializeStore = preloadedState => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export const wrapper = createWrapper(initializeStore)
