import { makeApiRequest } from 'lib/utils/api'
import { logout } from './slices/authSlice'

/*
This below is the format the middleware accepts.
When a type is set to null, it will be ignored!
{
  types: [ACTION_PENDING, { type: ACTION_SUCCESS, additionalProp: 'andItsValue' }, ACTION_FAILED],
  resource: 'auth',
  method: 'POST',
  query: {'ajax': true},
  data: {username, password},
  dataType: 'application/json'
}
*/
const apiMiddleware = store => next => async action => {
  // If its not an core-app async action, pass next.
  if (
    typeof action.types === 'undefined' ||
    typeof action.resource === 'undefined'
  )
    return next(action)

  // get action types
  const [pendingType, successType, errorType] = action.types

  const dispatchNext = (preparedAction, additionalProps) => {
    if (!preparedAction) return
    if (typeof preparedAction === 'object') {
      next({ ...preparedAction, ...additionalProps })
    } else {
      next({ type: preparedAction, ...additionalProps })
    }
  }

  // Dispatch the pending action
  dispatchNext(pendingType)

  // Make the api request
  const state = store.getState()

  try {
    const response = await makeApiRequest(
      action,
      state.auth.csrfToken,
      dispatchNext
    )

    // Dispatch the success action
    dispatchNext(successType, { payload: response.data })

    return response.data
  } catch (err) {
    // automatically logout when receiving any HTTP status code 401
    if (
      err.response &&
      err.response.status === 401 &&
      !action.disableAuthHandling
    ) {
      store.dispatch(logout(true))
    }

    // Dispatch the error action
    dispatchNext(errorType, {
      payload: {
        code: err.response ? err.response.status : 503,
        ...(err.response ? err.response.data : {}),
      },
    })
  }

  return undefined
}

export default apiMiddleware
