import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { Label, Image } from 'semantic-ui-react'

import classes from './information.module.less'

const Information = ({ fair }) => {
  const sections = ['startupBooth', 'equipment', 'whatToBring']

  return (
    <>
      <div className={classes.container}>
        <React.Fragment>
          <h3>
            <FormattedMessage
              id={`companyInfos.information.smallBooth.title`}
            />
          </h3>
          <p>
            <FormattedMessage
              id={`companyInfos.information.smallBooth.text`}
              values={{ fairName: fair.kontakt_title }}
            />
            <br />
            <br />
            <div style={{ position: 'relative', display: 'inline-block' }}>
              <Image src="/images/booth_1.jpg" size="medium" />
              <Label color="black" attached="bottom">
                <FormattedMessage id="companyInfos.information.caption1" />
              </Label>
            </div>
            &nbsp;&nbsp;
            <div style={{ position: 'relative', display: 'inline-block' }}>
              <Image src="/images/booth_2.jpg" size="medium" />
              <Label color="black" attached="bottom">
                <FormattedMessage id="companyInfos.information.caption2" />
              </Label>
            </div>
          </p>
        </React.Fragment>
        <React.Fragment>
          <h3>
            <FormattedMessage
              id={`companyInfos.information.largeBooth.title`}
            />
          </h3>
          <p>
            <FormattedMessage
              id={`companyInfos.information.largeBooth.text`}
              values={{ fairName: fair.kontakt_title }}
            />
            <br />
            <br />
            <div style={{ position: 'relative', display: 'inline-block' }}>
              <Image src="/images/booth_3.jpg" size="large" />
              <Label color="black" attached="bottom">
                <FormattedMessage id="companyInfos.information.caption3" />
              </Label>
            </div>
            &nbsp;&nbsp;
            <div style={{ position: 'relative', display: 'inline-block' }}>
              <Image src="/images/booth_4.jpg" size="large" />
              <Label color="black" attached="bottom">
                <FormattedMessage id="companyInfos.information.caption4" />
              </Label>
            </div>
          </p>
        </React.Fragment>
        {sections.map(key => (
          <React.Fragment key={key}>
            <h3>
              <FormattedMessage id={`companyInfos.information.${key}.title`} />
            </h3>
            <p>
              <FormattedMessage
                id={`companyInfos.information.${key}.text`}
                values={{ fairName: fair.kontakt_title }}
              />
            </p>
          </React.Fragment>
        ))}
      </div>
    </>
  )
}

Information.propTypes = {
  fair: PropTypes.object.isRequired,
}

export default Information
