import React from 'react'
import PropTypes from 'prop-types'
import { useIntl } from 'react-intl'

import { Tab } from 'semantic-ui-react'
import Concept from './concept'
import Services from './services'
import Information from './information'
import Location from './location'
import Signup from './signup'
import Prices from './prices'
import Timeline from './timeline'
import TimelineFair from './timeline_fair'

const CompanyInformation = ({
  fair,
  packages,
  boothOptions,
  showTimeline,
  showTimelineFair,
}) => {
  const intl = useIntl()

  const renderPane = content => <Tab.Pane>{content}</Tab.Pane>

  const getPane = (titleId, content) => ({
    menuItem: intl.formatMessage({ id: titleId }),
    render: () => renderPane(content),
  })

  const panes = [
    getPane('companyInfos.concept.title', <Concept fair={fair} />),
    getPane('companyInfos.services.title', <Services />),
    getPane('companyInfos.location.title', <Location />),
    getPane('companyInfos.information.title', <Information fair={fair} />),
    getPane('companyInfos.signup.title', <Signup fair={fair} />),
    getPane(
      'companyInfos.price.title',
      <Prices fair={fair} boothOptions={boothOptions} packages={packages} />
    ),
  ]

  if (showTimeline) {
    panes.push(getPane('companyInfos.timeline.title', <Timeline fair={fair} />))
  }

  if (showTimelineFair) {
    panes.push(
      getPane('companyInfos.timelineFair.title', <TimelineFair fair={fair} />)
    )
  }

  return <Tab panes={panes} />
}

CompanyInformation.propTypes = {
  fair: PropTypes.object.isRequired,
  boothOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  packages: PropTypes.arrayOf(PropTypes.object).isRequired,
  showTimeline: PropTypes.bool,
  showTimelineFair: PropTypes.bool,
}

CompanyInformation.defaultProps = {
  showTimeline: false,
}

export default CompanyInformation
