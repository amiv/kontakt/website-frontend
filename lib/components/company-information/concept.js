import React from 'react'
import PropTypes from 'prop-types'
import { Grid, Image } from 'semantic-ui-react'
import { FormattedMessage, useIntl } from 'react-intl'

import classes from './concept.module.less'

const Concept = ({ fair }) => {
  const intl = useIntl()

  const timeFormatOptions = {
    hour: 'numeric',
    hour12: intl.locale.startsWith('en'),
  }

  return (
    <>
      <div className={classes.container}>
        <p>
          <FormattedMessage
            id="companyInfos.concept.introduction"
            values={{ fairName: fair.kontakt_title }}
          />
        </p>
        <p>
          <FormattedMessage
            id="companyInfos.concept.description"
            values={{
              startTime: intl.formatTime(
                new Date(2021, 2, 2, 11),
                timeFormatOptions
              ),
              endTime: intl.formatTime(
                new Date(2021, 2, 2, 17),
                timeFormatOptions
              ),
              fairName: fair.kontakt_title,
            }}
          />
        </p>
        <p>
          <FormattedMessage id="companyInfos.concept.organizers" />
        </p>
      </div>
      <br />
      <Grid columns={2} padded="vertically">
        <Grid.Row>
          <Grid.Column>
            <Image src="/images/concept1.jpg" fluid />
          </Grid.Column>
          <Grid.Column>
            <Image src="/images/concept2.jpg" fluid />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Image src="/images/concept3.jpg" fluid />
          </Grid.Column>
          <Grid.Column>
            <Image src="/images/concept4.jpg" fluid />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </>
  )
}

Concept.propTypes = {
  fair: PropTypes.object.isRequired,
}

export default Concept
