import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

import classes from './signup.module.less'

const Signup = ({ fair }) => {
  const sections = ['new', 'old']

  return (
    <div className={classes.container}>
      {sections.map(key => (
        <React.Fragment key={key}>
          <h3>
            <FormattedMessage id={`companyInfos.signup.${key}.title`} />
          </h3>
          <p>
            <FormattedMessage
              id={`companyInfos.signup.${key}.text`}
              values={{ fairName: fair.kontakt_title }}
            />
          </p>
        </React.Fragment>
      ))}
    </div>
  )
}

Signup.propTypes = {
  fair: PropTypes.object.isRequired,
}

export default Signup
