import React from 'react'
import { FormattedMessage } from 'react-intl'

import classes from './services.module.less'

const Services = () => {
  const sections = [
    'booths',
    'packages',
    'fairguide',
    'catering',
    'parking',
    'advertisement',
    'supportingProgram',
  ]

  return (
    <>
      <div className={classes.container}>
        {sections.map(key => (
          <React.Fragment key={key}>
            <h3>
              <FormattedMessage id={`companyInfos.services.${key}.title`} />
            </h3>
            <p>
              <FormattedMessage id={`companyInfos.services.${key}.text`} />
            </p>
          </React.Fragment>
        ))}
      </div>
    </>
  )
}

export default Services
