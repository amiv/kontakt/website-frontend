import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { FormattedMessage, useIntl } from 'react-intl'

import classes from './timeline.module.less'

const TimelineItem = ({ startTime, endTime, formatDate, title, children }) => {
  const intl = useIntl()

  const getData = date => {
    return intl.formatDate(date, formatDate)
  }

  const startMonth = startTime && getData(startTime)
  const endMonth = getData(endTime)
  let timeStr

  if (!endTime) {
    timeStr = ''
  } else if (startMonth && startMonth !== endMonth) {
    timeStr = `${startMonth} - ${endMonth}`
  } else {
    timeStr = getData(endTime)
  }

  return (
    <li>
      <div className={classes.time}>{timeStr}</div>
      <div className={classes.itemContent}>
        <h4>{title}</h4>
        {children}
      </div>
    </li>
  )
}

TimelineItem.propTypes = {
  startTime: PropTypes.objectOf(Date),
  endTime: PropTypes.objectOf(Date).isRequired,
  formatDate: PropTypes.object,
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
}

const Timeline = ({ fair }) => {
  const intl = useIntl()
  const f = (id, values) => intl.formatMessage({ id }, values)

  const dateFormatOptions = {
    month: 'long',
    day: 'numeric',
  }
  const timeFormatOptions = {
    hour: 'numeric',
    hour12: intl.locale.startsWith('en'),
  }

  const registrationStartDate = new Date(fair.registration_start)
  const registrationEndDate = new Date(fair.registration_end)
  const dataInputDate = new Date(fair.profile_deadline)
  const readyToPrintDate = new Date(fair.fairguide_changes_deadline)
  return (
    <ul className={classes.timeline}>
      <TimelineItem
        startTime={registrationStartDate}
        endTime={registrationEndDate}
        formatDate={{ month: 'long' }}
        title={f('companyInfos.timeline.inscription.title')}
      >
        <FormattedMessage
          id="companyInfos.timeline.inscription.text"
          values={{
            fairName: fair.kontakt_title,
            registrationStart: intl.formatDate(
              registrationStartDate,
              dateFormatOptions
            ),
            registrationEnd: intl.formatDate(
              registrationEndDate,
              dateFormatOptions
            ),
            registrationLink: function RegistrationLink(...chunks) {
              return (
                <Link href="/company-portal/registration">
                  <a>{chunks}</a>
                </Link>
              )
            },
          }}
        />
      </TimelineItem>
      <TimelineItem
        title={f('companyInfos.timeline.dataInput.title')}
        formatDate={{ month: 'long' }}
        endTime={dataInputDate}
      >
        <FormattedMessage
          id="companyInfos.timeline.dataInput.text"
          values={{
            date: intl.formatDate(dataInputDate, dateFormatOptions),
          }}
        />
      </TimelineItem>
      <TimelineItem
        endTime={readyToPrintDate}
        formatDate={{ month: 'long' }}
        title={f('companyInfos.timeline.readyToPrint.title')}
      >
        <FormattedMessage
          id="companyInfos.timeline.readyToPrint.text"
          values={{
            date: intl.formatDate(readyToPrintDate, dateFormatOptions),
          }}
        />
      </TimelineItem>
      <TimelineItem
        startTime={new Date(2025, 7, 1)}
        endTime={new Date(2021, 8, 15)}
        formatDate={{ month: 'short' }}
        title={f('companyInfos.timeline.beforeTheFair.title')}
      >
        <FormattedMessage id="companyInfos.timeline.beforeTheFair.text" />
      </TimelineItem>
      <TimelineItem
        startTime={new Date(fair.date_start)}
        endTime={new Date(fair.date_end)}
        formatDate={{ month: 'short' }}
        title={f('companyInfos.timeline.fair.title')}
      >
        <FormattedMessage
          id="companyInfos.timeline.fair.text"
          values={{
            fairName: fair.kontakt_title,
            date1: intl.formatDate(fair.date_start, {
              ...dateFormatOptions,
              year: undefined,
            }),
            date2: intl.formatDate(fair.date_end, dateFormatOptions),
            startTime: intl.formatTime(
              new Date(2021, 2, 2, 11),
              timeFormatOptions
            ),
            endTime: intl.formatTime(
              new Date(2021, 2, 2, 17),
              timeFormatOptions
            ),
          }}
        />
      </TimelineItem>
    </ul>
  )
}

Timeline.propTypes = {
  fair: PropTypes.object.isRequired,
}

export default Timeline
