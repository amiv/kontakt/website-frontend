/* eslint-disable prettier/prettier */
import React from 'react'
import PropTypes from 'prop-types'
import { Table } from 'semantic-ui-react'
import { FormattedMessage, useIntl } from 'react-intl'

import classes from './prices.module.less'

const PricesTable = ({ fair, boothOptions }) => {
  const boothVariants = ['small', 'large']

  return (
    <>
      <Table celled structured>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell />
            <Table.HeaderCell />
            <Table.HeaderCell colSpan={boothOptions.length} textAlign="center">
              <FormattedMessage id="companyInfos.price.booths.categoryHeader" />
              (*)
            </Table.HeaderCell>
          </Table.Row>
          <Table.Row>
            <Table.HeaderCell />
            <Table.HeaderCell>
              <FormattedMessage id="companyInfos.price.booths.offerHeader" />
            </Table.HeaderCell>
            {boothOptions.map(option => (
              <Table.HeaderCell key={option.id} textAlign="right">
                {option.booth_category}
              </Table.HeaderCell>
            ))}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {boothVariants.map((boothVariant, index) => (
              <Table.Row key={`one_day_${boothVariant}`}>
                {index === 0 && (
                  <Table.Cell rowSpan="3">
                    <FormattedMessage
                      id={`companyInfos.price.booths.one_day`}
                    />
                  </Table.Cell>
                )}
                <Table.Cell>
                  <FormattedMessage
                    id={`companyInfos.price.booths.${boothVariant}_booth`}
                  />
                </Table.Cell>
                {boothOptions.map(option => (
                  <Table.Cell key={option.id} textAlign="right">
                    {option[`price_one_day_${boothVariant}`] || '-'}
                  </Table.Cell>
                ))}
              </Table.Row>
            ))
          }
          <Table.Row>
            <Table.Cell>
              <FormattedMessage id="companyInfos.price.booths.startup" />
            </Table.Cell>
            <Table.Cell textAlign='right'>
              -
            </Table.Cell>
            <Table.Cell textAlign="right" colSpan={boothOptions.length}>
              {fair.startup_discount}
            </Table.Cell>
          </Table.Row>
          {boothVariants.map((boothVariant, index) => (
              <Table.Row key={`two_days_${boothVariant}`}>
                {index === 0 && (
                  <Table.Cell rowSpan="2">
                    <FormattedMessage
                      id={"companyInfos.price.booths.two_days"}
                    />
                  </Table.Cell>
                )}
                <Table.Cell>
                  <FormattedMessage
                    id={`companyInfos.price.booths.${boothVariant}_booth`}
                  />
                </Table.Cell>
                {boothOptions.map(option => (
                  <Table.Cell key={option.id} textAlign="right">
                    {option[`price_two_days_${boothVariant}`] || '-'}
                  </Table.Cell>
                ))}
              </Table.Row>
            ))
          }
        </Table.Body>
      </Table>
      <p className={classes.vatHint}>
        (*) <FormattedMessage id="companyInfos.price.excludingVAT" />
        <br />
        (**) <FormattedMessage id="companyInfos.price.booths.startupBooth" />
        <br />
        (***) <FormattedMessage id="companyInfos.price.only2days" />
      </p>
    </>
  )
}

PricesTable.propTypes = {
  fair: PropTypes.object.isRequired,
  boothOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
}

const Prices = ({ fair, boothOptions, packages }) => {
  const intl = useIntl()

  return (
    /* <FormattedMessage id="companyInfos.price.rework" /> */
     <div className={classes.container}>
      <PricesTable fair={fair} boothOptions={boothOptions} />
      <h3>
        <FormattedMessage id="companyInfos.price.packages.title" />
      </h3>
      <p>
        <FormattedMessage
          id="companyInfos.price.packages.text"
          values = {{ fairName: fair.kontakt_title }}
        />
      </p>
      <Table celled structured>
        <Table.Body>
          {packages.map(packageItem => {
            const isGerman = intl.locale === 'de'
            const title = isGerman ? packageItem.title_de : packageItem.title_en
            const description = isGerman
              ? packageItem.description_de
              : packageItem.description_en

            return (
              <Table.Row key={packageItem.id}>
                <Table.Cell>
                  <h4 className={classes.packageTitle}>{title}</h4>
                  <p className={classes.packageText}>{description}</p>
                </Table.Cell>
                <Table.Cell textAlign="right">{`${packageItem.price} CHF (*)`}</Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
      <p className={classes.vatHint}>
        (*) <FormattedMessage id="companyInfos.price.excludingVAT" />
        <br />
        (**) <FormattedMessage id="companyPortal.registration.boothPremiumPackageInfo" />
      </p>

    </div>
  )
}

Prices.propTypes = {
  fair: PropTypes.object.isRequired,
  boothOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  packages: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default Prices
