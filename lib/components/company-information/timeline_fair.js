import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'

import classes from './timeline.module.less'

const TimelineItem = ({ startHour, endTime, formatDate, title, children }) => {
  const intl = useIntl()

  const getData = date => {
    return intl.formatDate(date, formatDate)
  }

  const endMonth = getData(endTime)
  let timeStr

  if (startHour && startHour !== endMonth) {
    timeStr = `${startHour} - ${endMonth}`
  } else {
    timeStr = getData(endTime)
  }

  return (
    <li>
      <div className={classes.time}>{timeStr}</div>
      <div className={classes.itemContent}>
        <h4>{title}</h4>
        {children}
      </div>
    </li>
  )
}

TimelineItem.propTypes = {
  startHour: PropTypes.string,
  endTime: PropTypes.objectOf(Date).isRequired,
  formatDate: PropTypes.object,
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
}

const TimelineFair = ({ fair }) => {
  const intl = useIntl()
  const f = (id, values) => intl.formatMessage({ id }, values)

  const timeFormatOptions = {
    hour: 'numeric',
    hour12: intl.locale.startsWith('en'),
  }

  return (
    <ul className={classes.timeline}>
      <TimelineItem
        startHour={'9'}
        endTime={new Date(2025, 2, 26, 11)}
        formatDate={{ hour: 'numeric' }}
        title={f('companyInfos.timelineFair.setup.title')}
      >
        <FormattedMessage id="companyInfos.timelineFair.setup.text" />
      </TimelineItem>
      <TimelineItem
        startHour={f('companyInfos.timelineFair.fair.time')}
        endTime={new Date(2025, 2, 26, 17)}
        formatDate={{ hour: 'numeric' }}
        title={fair.kontakt_title}
      >
        <FormattedMessage
          id="companyInfos.timelineFair.fair.text"
          values={{
            startTime: intl.formatTime(
              new Date(2021, 2, 2, 11),
              timeFormatOptions
            ),
            endTime: intl.formatTime(
              new Date(2021, 2, 2, 17),
              timeFormatOptions
            ),
          }}
        />
      </TimelineItem>
      <TimelineItem
        startHour={f('companyInfos.timelineFair.disassembly.time')}
        endTime={new Date(2025, 2, 26, 19)}
        formatDate={{ hour: 'numeric' }}
        title={f('companyInfos.timelineFair.disassembly.title')}
      >
        <FormattedMessage id="companyInfos.timelineFair.disassembly.text" />
      </TimelineItem>
      <TimelineItem
        startHour={f('companyInfos.timelineFair.bbq.time')}
        endTime={new Date(2025, 2, 26, 20)}
        formatDate={{ hour: 'numeric' }}
        title={f('companyInfos.timelineFair.bbq.title')}
      >
        <FormattedMessage id="companyInfos.timelineFair.bbq.text" />
      </TimelineItem>
    </ul>
  )
}

TimelineFair.propTypes = {
  fair: PropTypes.object.isRequired,
}

export default TimelineFair
