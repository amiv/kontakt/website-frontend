import React from 'react'
import { Label, Image } from 'semantic-ui-react'
import { FormattedMessage } from 'react-intl'

import dynamic from 'next/dynamic'

import VenueAddress from 'lib/components/venueAddress'

const Location = () => {
  const VenuePageNoSSR = dynamic(() => import('lib/components/map'), {
    ssr: false,
  })

  return (
    <>
      <FormattedMessage id="companyInfos.location.text" />
      <br />
      <br />
      <div>
        <main>
          <div id="map">
            <VenuePageNoSSR />
          </div>
          <br />
        </main>
        <VenueAddress />
      </div>
      <h2>
        <FormattedMessage id="venue.subtitle" />
      </h2>
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/CLA_aussen.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption1" />
        </Label>
      </div>
      &nbsp;&nbsp;&nbsp;
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/LEE_aussen.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption2" />
        </Label>
      </div>
      &nbsp;&nbsp;&nbsp;
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/Kontakt_aussen.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption3" />
        </Label>
      </div>
      <br />
      <br />
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/Map_Zentrum_cropped.jpg" size="huge" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.map" />
        </Label>
      </div>
      <h2>
        <FormattedMessage id="venue.subtitle2" />
      </h2>
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/CLA_D_edited.jpg" size="large" />
        <Label color="black" attached="bottom">
          CLA
        </Label>
      </div>
      &nbsp;&nbsp;&nbsp;
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/LEE_E_edited.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption4" />
        </Label>
      </div>
      &nbsp;&nbsp;&nbsp;
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/LEE_F_edited.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption5" />
        </Label>
      </div>
    </>
  )
}

export default Location
