import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import { Divider, Form } from 'semantic-ui-react'
import { validate as validateEmail } from 'email-validator'

import Textbox from './textbox'
import classes from './settingsForm.module.less'

const SettingsForm = ({ company, onChange }) => {
  const [correspondenceEmail, setCorrespondenceEmail] = useState('')
  const [billingRecipient, setBillingRecipient] = useState('')
  const [billingStreet, setBillingStreet] = useState('')
  const [billingPostalCode, setBillingPostalCode] = useState('')
  const [billingCity, setBillingCity] = useState('')
  const [billingCountry, setBillingCountry] = useState('')
  const [billingComment, setBillingComment] = useState('')
  const intl = useIntl()

  useEffect(() => {
    if (company) {
      setCorrespondenceEmail(company.correspondence_email_address || '')
      setBillingRecipient(company.billing_recipient || '')
      setBillingStreet(company.billing_address_street || '')
      setBillingPostalCode(company.billing_address_postal_code || '')
      setBillingCity(company.billing_address_city || '')
      setBillingCountry(company.billing_address_country || '')
      setBillingComment(company.billing_comment || '')
    }
  }, [company])

  const isCorrespondenceEmailValid =
    correspondenceEmail.length === 0 || validateEmail(correspondenceEmail)
  const isBillingStreetValid = billingStreet.length > 0
  const isBillingPostalCodeValid = billingPostalCode.length > 0 // TODO: improve validation here!
  const isBillingCityValid = billingCity.length > 0
  const isBillingCountryValid = billingCountry.length > 0

  const updateValue = (e, name, func) => {
    func(e.target.value)

    // Separate validation to run immediately. Other variables are only evaluated again when the
    // component is redrawn.
    const _isCorrespondenceEmailValid =
      correspondenceEmail.length === 0 || validateEmail(correspondenceEmail)
    const _isBillingStreetValid = billingStreet.length > 0
    const _isBillingPostalCodeValid = billingPostalCode.length > 0 // TODO: improve validation here!
    const _isBillingCityValid = billingCity.length > 0
    const _isBillingCountryValid = billingCountry.length > 0
    const isFormValid =
      _isCorrespondenceEmailValid &&
      _isBillingStreetValid &&
      _isBillingPostalCodeValid &&
      _isBillingCityValid &&
      _isBillingCountryValid

    onChange({
      value: {
        correspondence_email_address: correspondenceEmail,
        billing_recipient: billingRecipient,
        billing_address_street: billingStreet,
        billing_address_postal_code: billingPostalCode,
        billing_address_city: billingCity,
        billing_address_country: billingCountry,
        billing_comment: billingComment,
        [name]: e.target.value,
      },
      isValid: isFormValid,
    })
  }

  if (company) {
    return (
      <Form>
        <Divider horizontal className={classes.divider}>
          <FormattedMessage id="companyPortal.settings.correspondence" />
        </Divider>
        <Textbox marginTop marginBottom>
          <FormattedMessage id="companyPortal.settings.correspondenceText" />
        </Textbox>
        <Form.Group widths="equal">
          <Form.Input
            fluid
            label={intl.formatMessage({ id: 'correspondenceEmail' })}
            value={correspondenceEmail}
            placeholder="email@company.com"
            type="email"
            onChange={e =>
              updateValue(
                e,
                'correspondence_email_address',
                setCorrespondenceEmail
              )
            }
            error={!isCorrespondenceEmailValid}
          />
        </Form.Group>
        <Divider horizontal className={classes.divider}>
          <FormattedMessage id="companyPortal.settings.invoice" />
        </Divider>
        <Form.Group widths="equal">
          <Form.Input
            fluid
            label={intl.formatMessage({
              id: 'companyPortal.settings.billing.recipient',
            })}
            value={billingRecipient}
            placeholder={intl.formatMessage({
              id: 'companyPortal.settings.billing.recipientPlaceholder',
            })}
            onChange={e =>
              updateValue(e, 'billing_recipient', setBillingRecipient)
            }
          />
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Input
            fluid
            label={intl.formatMessage({
              id: 'companyPortal.settings.billing.street',
            })}
            value={billingStreet}
            error={!isBillingStreetValid}
            placeholder="Universitätstrasse 6"
            onChange={e =>
              updateValue(e, 'billing_address_street', setBillingStreet)
            }
          />
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Input
            fluid
            label={intl.formatMessage({
              id: 'companyPortal.settings.billing.postalCode',
            })}
            value={billingPostalCode}
            error={!isBillingPostalCodeValid}
            placeholder="8092"
            onChange={e =>
              updateValue(
                e,
                'billing_address_postal_code',
                setBillingPostalCode
              )
            }
          />
          <Form.Input
            fluid
            label={intl.formatMessage({
              id: 'companyPortal.settings.billing.city',
            })}
            value={billingCity}
            error={!isBillingCityValid}
            placeholder="Zürich"
            onChange={e =>
              updateValue(e, 'billing_address_city', setBillingCity)
            }
          />
        </Form.Group>
        <Form.Group widths="equal">
          <Form.Input
            fluid
            label={intl.formatMessage({
              id: 'companyPortal.settings.billing.country',
            })}
            value={billingCountry}
            error={!isBillingCountryValid}
            placeholder="Switzerland"
            onChange={e =>
              updateValue(e, 'billing_address_country', setBillingCountry)
            }
          />
        </Form.Group>
        <Form.Group widths="equal">
          <Form.TextArea
            fluid
            label={intl.formatMessage({
              id: 'companyPortal.settings.billing.comment',
            })}
            value={billingComment}
            placeholder={intl.formatMessage({
              id: 'companyPortal.settings.billing.commentPlaceholder',
            })}
            onChange={e => updateValue(e, 'billing_comment', setBillingComment)}
          />
        </Form.Group>
      </Form>
    )
  }

  return null
}

SettingsForm.propTypes = {
  company: PropTypes.object,
  onChange: PropTypes.func.isRequired,
}

export default SettingsForm
