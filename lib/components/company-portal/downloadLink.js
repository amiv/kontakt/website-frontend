import React from 'react'
import PropTypes from 'prop-types'

import classes from './downloadLink.module.less'

const DownloadLink = ({ type, label, href, ...props }) => {
  return (
    <a
      className={classes.link}
      target="_blank"
      rel="noreferrer"
      href={href}
      {...props}
    >
      <span className={classes.type}>{type.toUpperCase()}</span>
      {` ${label}`}
    </a>
  )
}

DownloadLink.propTypes = {
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
}

export default DownloadLink
