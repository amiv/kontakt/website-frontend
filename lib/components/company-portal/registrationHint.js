import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { useIntl } from 'react-intl'
import { Message } from 'semantic-ui-react'

import EmailLink from '../emailLink'

const RegistrationHint = ({ fair, participation }) => {
  const intl = useIntl()

  let title = null
  let text = null
  let negative = false
  let positive = false
  let warning = false

  const now = Date.now()
  const registrationStart = new Date(fair.registration_start)
  const registrationEnd = new Date(fair.registration_end)

  const dateFormatOptions = {
    weekday: 'long',
    day: '2-digit',
    month: 'long',
    year: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }
  const registrationComplete =
    participation &&
    participation.accepted_participation_terms &&
    participation.accepted_permitted_items_terms

  if (registrationStart > now) {
    title = intl.formatMessage(
      { id: 'companyPortal.registration.notStarted.title' },
      { fairName: fair.kontakt_title }
    )
    text = intl.formatMessage(
      { id: 'companyPortal.registration.notStarted.text' },
      { date: intl.formatDate(registrationStart, dateFormatOptions) }
    )
  } else if (registrationEnd <= now) {
    if (!participation) {
      title = intl.formatMessage(
        { id: 'companyPortal.registration.ended.title' },
        { fairName: fair.kontakt_title }
      )
      text = intl.formatMessage(
        { id: 'companyPortal.registration.ended.text' },
        { contactEmail: <EmailLink /> }
      )
      negative = true
    } else if (!registrationComplete) {
      title = intl.formatMessage(
        { id: 'companyPortal.registration.acceptTermsNeeded.title' },
        { fairName: fair.kontakt_title }
      )
      text = intl.formatMessage(
        { id: 'companyPortal.registration.acceptTermsNeeded.text' },
        {
          a: function RegistrationLink(...chunks) {
            return (
              <Link href="/company-portal/registration">
                <a>{chunks}</a>
              </Link>
            )
          },
        }
      )
      negative = true
    }
  } else if (!participation) {
    title = intl.formatMessage(
      { id: 'companyPortal.registration.open.title' },
      { fairName: fair.kontakt_title }
    )
    text = intl.formatMessage(
      { id: 'companyPortal.registration.open.text' },
      {
        a: function RegistrationLink(...chunks) {
          return (
            <Link href="/company-portal/registration">
              <a>{chunks}</a>
            </Link>
          )
        },
      }
    )
    positive = true
  } else if (!registrationComplete) {
    title = intl.formatMessage(
      { id: 'companyPortal.registration.acceptTermsNeeded.title' },
      { fairName: fair.kontakt_title }
    )
    text = intl.formatMessage(
      { id: 'companyPortal.registration.acceptTermsNeeded.text' },
      {
        a: function RegistrationLink(...chunks) {
          return (
            <Link href="/company-portal/registration">
              <a>{chunks}</a>
            </Link>
          )
        },
      }
    )
    warning = true
  }

  if (title && text) {
    return (
      <Message negative={negative} positive={positive} warning={warning}>
        <Message.Header>{title}</Message.Header>
        <p>{text}</p>
      </Message>
    )
  }

  return null
}

RegistrationHint.propTypes = {
  fair: PropTypes.object,
  participation: PropTypes.object,
}

export default RegistrationHint
