import React from 'react'
import PropTypes from 'prop-types'
import Router from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import { useIntl } from 'react-intl'
import { Grid, Menu } from 'semantic-ui-react'

import { getPublicConfig } from 'lib/utils/config'
import { requireDataLoaded } from 'lib/utils/api'
import useCurrentParticipation from 'lib/hooks/useCurrentParticipation'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import useCompany from 'lib/hooks/useCompany'
import ResourceState from 'lib/store/resourceState'
import {
  logout,
  selectState as selectAuthState,
} from 'lib/store/slices/authSlice'

import Layout from '../layout'

const CompanyPortalLayout = ({ children, activeItem, ...props }) => {
  const authState = useSelector(selectAuthState)
  const currentFairItems = useCurrentFair()
  const companyItems = useCompany()
  const currentParticipationItems = useCurrentParticipation()
  const dispatch = useDispatch()
  const { contactEmail } = getPublicConfig()
  const intl = useIntl()

  const [fair] = currentFairItems
  const [participation] = currentParticipationItems

  const handleLogoutClick = e => {
    e.preventDefault()
    dispatch(logout())
  }

  let content = requireDataLoaded([
    currentFairItems,
    companyItems,
    currentParticipationItems,
  ])

  if (!content) {
    // When this value is set, the registration is not open.
    let registrationError = null

    const now = Date.now()
    const registrationStart = new Date(fair.registration_start)
    const registrationEnd = new Date(fair.registration_end)
    if (registrationStart > now) {
      const dateFormatOptions = {
        weekday: 'long',
        day: 'numeric',
        month: 'long',
        year: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
      }

      registrationError = intl.formatMessage(
        { id: 'companyPortal.registration.notStarted' },
        { date: intl.formatDate(registrationStart, dateFormatOptions) }
      )
    } else if (registrationEnd < now && !participation) {
      registrationError = intl.formatMessage(
        { id: 'companyPortal.registration.ended' },
        { contactEmail }
      )
    }

    const registrationComplete =
      participation &&
      participation.accepted_participation_terms &&
      participation.accepted_permitted_items_terms

    const navigateMenuItem = e => {
      e.preventDefault()
      const path = e.target.attributes.href.value
      Router.push(path)
    }

    const locale = intl.locale.split('-')[0]

    content = (
      <Grid>
        <Grid.Column width={4}>
          <Menu fluid pointing vertical>
            <Menu.Item
              name={intl.formatMessage({
                id: 'companyPortal.menu.home',
              })}
              active={activeItem === 'home'}
              href={`/${locale}/company-portal`}
              onClick={navigateMenuItem}
            />
            <Menu.Item
              name={intl.formatMessage({
                id: 'companyPortal.menu.registration',
              })}
              active={activeItem === 'registration'}
              href={`/${locale}/company-portal/registration`}
              onClick={navigateMenuItem}
              disabled={registrationError !== null}
              title={registrationError}
            />
            <Menu.Item
              name={intl.formatMessage({
                id: 'companyPortal.menu.account',
              })}
              active={activeItem === 'account'}
              href={`/${locale}/company-portal/account`}
              onClick={navigateMenuItem}
            />
            <Menu.Item
              name={intl.formatMessage({ id: 'companyPortal.menu.settings' })}
              active={activeItem === 'settings'}
              href={`/${locale}/company-portal/settings`}
              onClick={navigateMenuItem}
            />
            <Menu.Item
              name={intl.formatMessage({
                id: 'companyPortal.menu.companyProfile',
              })}
              active={activeItem === 'profile'}
              disabled={!registrationComplete}
              href={`/${locale}/company-portal/profile`}
              onClick={navigateMenuItem}
            />
            <Menu.Item
              name={intl.formatMessage({ id: 'companyPortal.menu.purchases' })}
              active={activeItem === 'purchases'}
              href={`/${locale}/company-portal/purchases`}
              onClick={navigateMenuItem}
            />
            <Menu.Item
              name={intl.formatMessage({ id: 'companyPortal.menu.logout' })}
              disabled={authState === ResourceState.Loading}
              href={`/${locale}/logout`}
              onClick={handleLogoutClick}
            />
          </Menu>
        </Grid.Column>

        <Grid.Column stretched width={12}>
          {children}
        </Grid.Column>
      </Grid>
    )
  }

  return (
    <Layout {...props} authenticatedOnly>
      {content}
    </Layout>
  )
}

CompanyPortalLayout.propTypes = {
  children: PropTypes.node.isRequired,
  activeItem: PropTypes.oneOf([
    'home',
    'registration',
    'account',
    'profile',
    'settings',
    'purchases',
  ]),
}

CompanyPortalLayout.defaultProps = {
  authenticatedOnly: false,
}

export default CompanyPortalLayout
