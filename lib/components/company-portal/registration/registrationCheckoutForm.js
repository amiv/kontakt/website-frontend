import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import {
  Checkbox,
  Divider,
  Form,
  GridRow,
  Header,
  Image,
  TextArea,
} from 'semantic-ui-react'

import DownloadLink from '../downloadLink'
import RegistrationOverviewTable from './registrationOverviewTable'

import classes from './registrationCheckoutForm.module.less'
import Textbox from '../textbox'

const RegistrationCheckoutForm = ({
  fair,
  title: propTitle,
  booths,
  packages,
  startupDiscount,
  totalPrice,
  comment,
  acceptedTAC,
  acceptedTermsPermittedItems,
  onChange,
  readOnly,
}) => {
  const intl = useIntl()

  const defaultTitle = intl.formatMessage({
    id: 'companyPortal.registration.checkout.subtitle',
  })
  const title = propTitle || defaultTitle

  const handleTACChange = () => {
    onChange({
      acceptedTAC: !acceptedTAC,
      acceptedTermsPermittedItems,
      comment,
    })
  }

  const handleTermsPermittedItemsChange = () => {
    onChange({
      acceptedTAC,
      acceptedTermsPermittedItems: !acceptedTermsPermittedItems,
      comment,
    })
  }

  const handleCommentChange = e => {
    onChange({
      acceptedTAC,
      acceptedTermsPermittedItems,
      comment: e.target.value,
    })
  }

  return (
    <div>
      <Header size="medium">{title}</Header>
      <Textbox marginBottom>
        <FormattedMessage id="companyPortal.registration.checkout.selectedOptions" />
      </Textbox>

      <RegistrationOverviewTable
        fair={fair}
        booths={booths}
        packages={packages}
        startupDiscount={startupDiscount}
        totalPrice={totalPrice}
      />
      <GridRow className={classes.row}>
        <Textbox>
          <FormattedMessage id="companyPortal.registration.checkout.PremiumPackageNotifier1" />
        </Textbox>
        <Image src="/images/booth_layout.jpg" size="massive" centered />
        <Textbox>
          <FormattedMessage
            id="companyPortal.registration.checkout.PremiumPackageNotifier2"
            values={{ fairName: fair.kontakt_title }}
          />
        </Textbox>
      </GridRow>
      <GridRow className={classes.row}>
        {readOnly && comment && (
          <React.Fragment>
            <h4>
              <FormattedMessage id="companyPortal.registration.checkout.commentLabel" />
            </h4>
            <p className={classes.comment}>{comment}</p>
            <Divider className={classes.divider} />
          </React.Fragment>
        )}
        {!readOnly && (
          // Form is required so that the semantic-ui styles are applied correctly.
          <Form>
            <TextArea
              placeholder={intl.formatMessage(
                {
                  id: 'companyPortal.registration.checkout.commentPlaceholder',
                },
                { fairName: fair.kontakt_title }
              )}
              value={comment}
              onChange={handleCommentChange}
            />
          </Form>
        )}
      </GridRow>

      <GridRow className={classes.row}>
        <Textbox marginBottom>
          <FormattedMessage id="companyPortal.registration.checkout.noContract" />
        </Textbox>

        <div>
          <DownloadLink
            type="pdf"
            label="Teilnahmebedingungen Firmenmessen"
            href="/legal/VSETH-Firmenmessen-AGB.pdf"
          />
          <br />
          <DownloadLink
            type="pdf"
            label="Conditions of participation for company fairs"
            href="/legal/VSETH-Firmenmessen-GTC.pdf"
          />
        </div>

        <div>
          <Checkbox
            checked={acceptedTAC}
            readOnly={readOnly}
            onChange={handleTACChange}
            label={intl.formatMessage({
              id: 'companyPortal.registration.checkout.acceptedTAC',
            })}
          />
          <br />
          <Checkbox
            checked={acceptedTermsPermittedItems}
            readOnly={readOnly}
            onChange={handleTermsPermittedItemsChange}
            label={intl.formatMessage({
              id: 'companyPortal.registration.checkout.acceptedTermsPermittedItems',
            })}
          />
        </div>
      </GridRow>
    </div>
  )
}

RegistrationCheckoutForm.propTypes = {
  fair: PropTypes.object.isRequired,
  title: PropTypes.string,
  comment: PropTypes.string,
  acceptedTAC: PropTypes.bool,
  acceptedTermsPermittedItems: PropTypes.bool,
  booths: PropTypes.arrayOf(PropTypes.object).isRequired,
  packages: PropTypes.arrayOf(PropTypes.object).isRequired,
  totalPrice: PropTypes.number.isRequired,
  startupDiscount: PropTypes.number,
  onChange: PropTypes.func,
  readOnly: PropTypes.bool,
}

RegistrationCheckoutForm.defaultProps = {
  onChange: () => {},
  readOnly: false,
  acceptedTAC: false,
  acceptedTermsPermittedItems: false,
}

export default RegistrationCheckoutForm
