import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import {
  Divider,
  Form,
  GridRow,
  Header,
  Message,
  Radio,
} from 'semantic-ui-react'

import { getTimeslot } from 'lib/utils/timeslot'

import classes from './registrationDaysForm.module.less'

export const DaySelectionEnum = Object.freeze({
  First: 'FIRST',
  Second: 'SECOND',
  Third: 'THIRD',
  Both: 'BOTH',
  Second_and_Third: 'SECOND_AND_THIRD',
})

export const BoothSizeSelectionEnum = Object.freeze({
  Small: 'SMALL',
  Large: 'LARGE',
})

export const TableTypeSelectionEnum = Object.freeze({
  Low: 'LOW',
  High: 'HIGH',
})

export const SignupTableTypeSelectionEnum = Object.freeze({
  Low: 0,
  High: 1,
})

const RegistrationDaysForm = ({
  fair,
  availableTimeslots,
  tableType,
  day,
  boothSize,
  category,
  onChange,
}) => {
  const intl = useIntl()

  const getSelectedTimeslot = () =>
    getTimeslot(availableTimeslots, day, boothSize, category)

  const handleDayChange = (e, { value }) => {
    onChange({
      tableType,
      day: value,
      boothSize,
      category,
    })
  }

  const handleBoothSizeChange = (e, { value }) => {
    onChange({
      tableType,
      day,
      boothSize: value,
      category,
    })
  }

  const handleTableTypeChange = (e, { value }) => {
    onChange({
      tableType: value,
      day,
      boothSize,
      category,
    })
  }

  const handleCategoryChange = (e, { value }) => {
    onChange({
      tableType,
      day,
      boothSize,
      category: value,
    })
  }

  const filteredTimeslots = availableTimeslots.filter(
    timeslot => timeslot.day === day && timeslot.size === boothSize
  )
  console.log(availableTimeslots)
  console.log(filteredTimeslots)

  const isComplete =
    day !== undefined &&
    boothSize !== undefined &&
    tableType !== undefined &&
    category !== undefined
  const isValid = isComplete && getSelectedTimeslot() !== undefined

  return (
    <div>
      <Header size="medium">
        <FormattedMessage id="companyPortal.registration.days.title" />
      </Header>
      <p>
        <FormattedMessage
          id="companyPortal.registration.days.text"
          values={{ fairName: fair.kontakt_title, break: <br /> }}
        />
      </p>

      <Form.Group inline>
        <Form.Field
          control={Radio}
          label={intl.formatDate(fair.date_start, {
            weekday: 'long',
            month: 'long',
            day: '2-digit',
            year: 'numeric',
          })}
          value={DaySelectionEnum.First}
          checked={day === DaySelectionEnum.First}
          onChange={handleDayChange}
        />
        <Form.Field
          control={Radio}
          label={intl.formatDate(fair.date_end, {
            weekday: 'long',
            month: 'long',
            day: '2-digit',
            year: 'numeric',
          })}
          value={DaySelectionEnum.Second}
          checked={day === DaySelectionEnum.Second}
          onChange={handleDayChange}
        />
        {fair.number_of_days === 3 && (
          <Form.Field
            control={Radio}
            label={intl.formatDate(fair.date_end, {
              weekday: 'long',
              month: 'long',
              day: '2-digit',
              year: 'numeric',
            })}
            value={DaySelectionEnum.Third}
            checked={day === DaySelectionEnum.Third}
            onChange={handleDayChange}
          />
        )}
        <Form.Field
          control={Radio}
          label={intl.formatMessage({
            id: 'companyPortal.registration.days.both', // e.g., "First and Second"
          })}
          value={DaySelectionEnum.Both}
          checked={day === DaySelectionEnum.Both}
          onChange={handleDayChange}
        />
        <Form.Field
          control={Radio}
          label={intl.formatMessage({
            id: 'companyPortal.registration.days.secondAndThird', // e.g., "Second and Third"
          })}
          value={DaySelectionEnum.Second_and_Third}
          checked={day === DaySelectionEnum.Second_and_Third}
          onChange={handleDayChange}
        />
      </Form.Group>
      <p>
        <FormattedMessage
          id="companyPortal.registration.days.boothSize"
          values={{
            smallDimensions: '2.5 x 2.2 m',
            largeDimensions: '5 x 2.2 m',
          }}
        />
      </p>
      <Form.Group inline>
        <Form.Field
          control={Radio}
          label={intl.formatMessage({
            id: 'companyPortal.boothLarge',
          })}
          value={BoothSizeSelectionEnum.Large}
          checked={boothSize === BoothSizeSelectionEnum.Large}
          onChange={handleBoothSizeChange}
        />
        <Form.Field
          control={Radio}
          label={intl.formatMessage({
            id: 'companyPortal.boothSmall',
          })}
          value={BoothSizeSelectionEnum.Small}
          checked={boothSize === BoothSizeSelectionEnum.Small}
          onChange={handleBoothSizeChange}
        />
      </Form.Group>
      <p>
        <FormattedMessage id="companyPortal.registration.days.tableType" />
      </p>
      <Form.Group inline>
        <Form.Field
          control={Radio}
          label={intl.formatMessage({
            id: 'companyPortal.tableLow',
          })}
          value={SignupTableTypeSelectionEnum.Low}
          checked={tableType === SignupTableTypeSelectionEnum.Low}
          onChange={handleTableTypeChange}
        />
        <Form.Field
          control={Radio}
          label={intl.formatMessage({
            id: 'companyPortal.tableHigh',
          })}
          value={SignupTableTypeSelectionEnum.High}
          checked={tableType === SignupTableTypeSelectionEnum.High}
          onChange={handleTableTypeChange}
        />
      </Form.Group>

      <Divider horizontal>
        <FormattedMessage id="companyPortal.registration.days.categories.title" />
      </Divider>

      <p>
        <FormattedMessage
          id="companyPortal.registration.days.categories.label"
          values={{ break: <br /> }}
        />
      </p>
      <Form.Group inline>
        {filteredTimeslots.length > 0 ? (
          filteredTimeslots.map(timeslot => (
            <Form.Field
              key={timeslot.category}
              control={Radio}
              label={`${intl.formatMessage(
                { id: 'companyPortal.registration.days.category' },
                {
                  categoryName: timeslot.category,
                  price: timeslot.price,
                  freeSpaces: timeslot.available_spaces,
                }
              )} (*)`}
              value={timeslot.category}
              disabled={timeslot.available_spaces === 0}
              checked={category === timeslot.category}
              onChange={handleCategoryChange}
            />
          ))
        ) : (
          <p>
            <FormattedMessage id="companyPortal.registration.days.noCategories" />
          </p>
        )}
      </Form.Group>
      <p>
        <FormattedMessage id="companyPortal.registration.days.startupDiscount" />
      </p>
      <p className={classes.vatHint}>
        (*) <FormattedMessage id="companyPortal.registration.excludingVAT" />
      </p>

      {isComplete && !isValid && (
        <GridRow>
          <Message negative size="small" className={classes.error}>
            <FormattedMessage id="companyPortal.registration.days.selectionInvalid" />
          </Message>
        </GridRow>
      )}
    </div>
  )
}

RegistrationDaysForm.propTypes = {
  fair: PropTypes.object.isRequired,
  availableTimeslots: PropTypes.arrayOf(PropTypes.object).isRequired,
  tableType: PropTypes.number,
  day: PropTypes.string,
  boothSize: PropTypes.string,
  category: PropTypes.string,
  onChange: PropTypes.func.isRequired,
}

export default RegistrationDaysForm
