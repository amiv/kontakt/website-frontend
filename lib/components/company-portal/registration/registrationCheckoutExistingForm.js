import React from 'react'
import PropTypes from 'prop-types'

import RegistrationCheckoutForm from './registrationCheckoutForm'

const RegistrationCheckoutExistingForm = ({
  title,
  fair,
  company,
  participation,
  comment: propComment,
  acceptedTAC: propAcceptedTAC,
  acceptedTermsPermittedItems: propAcceptedTermsPermittedItems,
  onChange,
  readOnly,
}) => {
  const comment =
    propComment !== undefined ? propComment : participation.message
  const acceptedTAC =
    propAcceptedTAC !== undefined
      ? propAcceptedTAC
      : participation.accepted_participation_terms
  const acceptedTermsPermittedItems =
    propAcceptedTermsPermittedItems !== undefined
      ? propAcceptedTermsPermittedItems
      : participation.accepted_permitted_items_terms

  return (
    <RegistrationCheckoutForm
      fair={fair}
      title={title}
      comment={comment}
      acceptedTAC={acceptedTAC}
      acceptedTermsPermittedItems={acceptedTermsPermittedItems}
      startupDiscount={company.startup ? fair.startup_discount : undefined}
      booths={participation.booths_booked.map(booth_booked => ({
        id: booth_booked.id,
        category: booth_booked.booth_option.booth_category,
        boothSize: booth_booked.booth_size,
        tableType: booth_booked.table_type,
        day: booth_booked.day,
        price: booth_booked.price,
      }))}
      packages={participation.packages_booked.map(package_booked => ({
        id: package_booked.id,
        title_de: package_booked.package_option.title_de,
        title_en: package_booked.package_option.title_en,
        price: package_booked.package_option.price,
      }))}
      totalPrice={participation.total_price}
      onChange={onChange}
      readOnly={readOnly}
    />
  )
}

RegistrationCheckoutExistingForm.propTypes = {
  participation: PropTypes.object.isRequired,
  fair: PropTypes.object.isRequired,
  company: PropTypes.object.isRequired,
  title: PropTypes.string,
  comment: PropTypes.string,
  acceptedTAC: PropTypes.bool,
  acceptedTermsPermittedItems: PropTypes.bool,
  onChange: PropTypes.func,
  readOnly: PropTypes.bool,
}

RegistrationCheckoutExistingForm.defaultProps = {
  readOnly: false,
}

export default RegistrationCheckoutExistingForm
