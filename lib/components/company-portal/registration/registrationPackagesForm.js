import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { Divider, Grid, Header } from 'semantic-ui-react'

import Package from './package'

import classes from './registrationPackagesForm.module.less'

const RegistrationPackagesForm = ({
  fair,
  availablePackages,
  selectedPackages,
  onChange,
}) => {
  const mutexPackageGroups = useMemo(() => {
    const packageGroups = []

    availablePackages.forEach(item => {
      if (item.mutex_package_id === null) return
      const index = packageGroups.findIndex(
        items =>
          items.find(subitem => subitem.id === item.mutex_package_id) !==
          undefined
      )
      if (index < 0) {
        packageGroups.push([item])
      } else {
        packageGroups[index].push(item)
      }
    })
    return packageGroups
  }, [availablePackages])

  const handlePackageChange = ({ id, value }) => {
    let newSelection = selectedPackages
    if (value && !selectedPackages.includes(id)) {
      newSelection.push(id)
      const packageItem = availablePackages.find(item => item.id === id)
      if (packageItem && packageItem.mutex_package_id) {
        newSelection = newSelection.filter(
          item => item !== packageItem.mutex_package_id
        )
      }
    } else if (!value && selectedPackages.includes(id)) {
      newSelection = newSelection.filter(item => item !== id)
    }

    onChange(newSelection)
  }

  return (
    <div>
      <Header size="medium">
        <FormattedMessage id="companyPortal.registration.packages.title" />
      </Header>
      <p className={classes.text}>
        <FormattedMessage
          id="companyPortal.registration.packages.text"
          values={{ fairName: fair.kontakt_title }}
        />
      </p>
      {availablePackages
        .filter(item => item.mutex_package_id === null)
        .map(item => (
          <Package
            key={item.id}
            packageItem={item}
            onChange={handlePackageChange}
            checked={selectedPackages.includes(item.id)}
          />
        ))}

      {mutexPackageGroups.map(([item1, item2], i) => {
        return (
          <div key={i} className={classes.mutexPackages}>
            <Grid columns={2} relaxed="very">
              <Grid.Column>
                <Package
                  packageItem={item1}
                  onChange={handlePackageChange}
                  checked={selectedPackages.includes(item1.id)}
                />
              </Grid.Column>
              <Grid.Column>
                <Package
                  packageItem={item2}
                  onChange={handlePackageChange}
                  checked={selectedPackages.includes(item2.id)}
                />
              </Grid.Column>
            </Grid>
            <Divider vertical>
              <FormattedMessage id="or" />
            </Divider>
          </div>
        )
      })}
      <p className={classes.vatHint}>
        (*) <FormattedMessage id="companyPortal.registration.excludingVAT" />
      </p>
      <p className={classes.boothPlusHint}>
        (**)
        <FormattedMessage id="companyPortal.registration.boothPlusPackageInfo" />
      </p>
      <p className={classes.boothPlusHint}>
        (***)
        <FormattedMessage id="companyPortal.registration.boothPremiumPackageInfo" />
      </p>
    </div>
  )
}

RegistrationPackagesForm.propTypes = {
  fair: PropTypes.object.isRequired,
  availablePackages: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedPackages: PropTypes.arrayOf(PropTypes.number),
  onChange: PropTypes.func.isRequired,
}

RegistrationPackagesForm.defaultProps = {
  selectedPackages: [],
}

export default RegistrationPackagesForm
