import React from 'react'
import PropTypes from 'prop-types'
import { useIntl } from 'react-intl'
import { Checkbox } from 'semantic-ui-react'

import classes from './package.module.less'

const Package = ({ packageItem, checked, onChange }) => {
  const intl = useIntl()

  const handleClicked = () => {
    onChange({ id: packageItem.id, value: !checked })
  }

  const isGerman = intl.locale === 'de'
  const title = isGerman ? packageItem.title_de : packageItem.title_en
  const description = isGerman
    ? packageItem.description_de
    : packageItem.description_en

  return (
    <div className={classes.container} onClick={handleClicked}>
      <div>
        <Checkbox
          checked={checked}
          onChange={handleClicked}
          onClick={handleClicked}
        />
      </div>

      <div>
        <div className={classes.header}>{title}</div>
        <div className={classes.description}>{description}</div>
        <div className={classes.price}>{`${packageItem.price} CHF (*)`}</div>
      </div>
    </div>
  )
}

Package.propTypes = {
  packageItem: PropTypes.object.isRequired,
  onChange: PropTypes.func,
  checked: PropTypes.bool,
}

Package.defaultProps = {
  onChange: () => {},
}

export default Package
