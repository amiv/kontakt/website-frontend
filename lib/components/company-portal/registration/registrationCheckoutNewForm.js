import React from 'react'
import PropTypes from 'prop-types'
import { getTimeslot } from 'lib/utils/timeslot'

import RegistrationCheckoutForm from './registrationCheckoutForm'

const RegistrationCheckoutNewForm = ({
  fair,
  availableTimeslots,
  availablePackages,
  day,
  boothSize,
  tableType,
  category,
  packages,
  comment,
  acceptedTAC,
  acceptedTermsPermittedItems,
  onChange,
}) => {
  const getSelectedTimeslot = () =>
    getTimeslot(availableTimeslots, day, boothSize, category)

  let totalPrice = 0

  packages.forEach(packageId => {
    const item = availablePackages.find(pkgItem => pkgItem.id === packageId)
    totalPrice += item ? item.price : 0
  })
  totalPrice += getSelectedTimeslot().price

  const selectedTimeslot = getSelectedTimeslot()

  return (
    <RegistrationCheckoutForm
      fair={fair}
      booths={[
        { category, boothSize, tableType, day, price: selectedTimeslot.price },
      ]}
      packages={packages
        .map(packageId => {
          const item = availablePackages.find(
            pkgItem => pkgItem.id === packageId
          )
          if (!item) return null
          return {
            id: packageId,
            title_de: item.title_de,
            title_en: item.title_en,
            price: item.price,
          }
        })
        .filter(item => item)}
      totalPrice={totalPrice}
      acceptedTAC={acceptedTAC}
      acceptedTermsPermittedItems={acceptedTermsPermittedItems}
      comment={comment}
      onChange={onChange}
    />
  )
}

RegistrationCheckoutNewForm.propTypes = {
  fair: PropTypes.object.isRequired,
  comment: PropTypes.string,
  acceptedTAC: PropTypes.bool,
  acceptedTermsPermittedItems: PropTypes.bool,
  availableTimeslots: PropTypes.arrayOf(PropTypes.object).isRequired,
  availablePackages: PropTypes.arrayOf(PropTypes.object).isRequired,
  day: PropTypes.string.isRequired,
  boothSize: PropTypes.string.isRequired,
  tableType: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  packages: PropTypes.arrayOf(PropTypes.number).isRequired,
  onChange: PropTypes.func,
}

export default RegistrationCheckoutNewForm
