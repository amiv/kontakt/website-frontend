import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import {
  Button,
  Form,
  GridRow,
  Icon,
  Message,
  Segment,
  Step,
} from 'semantic-ui-react'

import { setCurrentParticipation } from 'lib/store/slices/currentParticipationSlice'
import { selectCSRFToken } from 'lib/store/slices/authSlice'
import { loadBooths } from 'lib/store/slices/boothsSlice'
import { getTimeslot } from 'lib/utils/timeslot'
import { makeApiRequest } from 'lib/utils/api'

import ActionBar from '../actionBar'
import EmailLink from '../../emailLink'
import RegistrationDaysForm from './registrationDaysForm'
import RegistrationPackagesForm from './registrationPackagesForm'
import RegistrationCheckoutNewForm from './registrationCheckoutNewForm'
import RegistrationCheckoutExistingForm from './registrationCheckoutExistingForm'

import classes from './registrationForm.module.less'

const StepEnum = Object.freeze({
  Days: 'days',
  Packages: 'packages',
  Checkout: 'checkout',
})

const RegistrationForm = ({
  fair,
  company,
  availablePackages,
  availableTimeslots,
  participation,
}) => {
  const [step, setStep] = useState(StepEnum.Days)
  const [selectedDay, setSelectedDay] = useState(undefined)
  const [selectedBoothSize, setSelectedBoothSize] = useState(undefined)
  const [selectedCategory, setSelectedCategory] = useState(undefined)
  const [selectedTableType, setSelectedTableType] = useState(undefined)
  const [selectedPackages, setSelectedPackages] = useState([])
  const [acceptedTAC, setAcceptedTAC] = useState(false)
  const [acceptedTermsPermittedItems, setAcceptedTermsPermittedItems] =
    useState(false)
  const [comment, setComment] = useState(undefined)
  const [isBusy, setIsBusy] = useState(false)
  const [error, setError] = useState(null)
  const [success, setSuccess] = useState(false)
  const csrfToken = useSelector(selectCSRFToken)
  const dispatch = useDispatch()

  // If participation already exists, but terms are not accepted,
  // jump directly to checkout step.
  useEffect(() => {
    if (participation) {
      setAcceptedTAC(participation.accepted_participation_terms)
      setAcceptedTermsPermittedItems(
        participation.accepted_permitted_items_terms
      )
      setComment(participation.message)
      setStep(StepEnum.Checkout)
    }
  }, [participation])

  const changeToStep = targetStep => {
    setStep(targetStep)

    if (targetStep === StepEnum.Days) {
      dispatch(loadBooths(true))
    }
  }

  const handleProceedClicked = () => {
    if (step === StepEnum.Days) {
      changeToStep(StepEnum.Packages)
    } else if (step === StepEnum.Packages) {
      changeToStep(StepEnum.Checkout)
    }
  }

  const handleBackClicked = () => {
    if (step === StepEnum.Checkout) {
      changeToStep(StepEnum.Packages)
    } else if (step === StepEnum.Packages) {
      changeToStep(StepEnum.Days)
    }
  }

  const handleSubmitClickedNew = () => {
    const data = {
      table_type: selectedTableType,
      packages: selectedPackages,
      message: comment || '',
      accepted_participation_terms: acceptedTAC || false,
      accepted_permitted_items_terms: acceptedTermsPermittedItems || false,
      timeslot: getTimeslot(
        availableTimeslots,
        selectedDay,
        selectedBoothSize,
        selectedCategory
      ).id,
    }

    const action = {
      method: 'POST',
      resource: 'participations/signup',
      data,
    }
    return makeApiRequest(action, csrfToken, dispatch)
  }

  const handleSubmitClickedExisting = () => {
    const data = {
      message: comment || '',
      accepted_participation_terms: acceptedTAC || false,
      accepted_permitted_items_terms: acceptedTermsPermittedItems || false,
    }

    const action = {
      method: 'PATCH',
      resource: `participations/${participation.id}`,
      data,
    }
    return makeApiRequest(action, csrfToken, dispatch)
  }

  const handleSubmitClicked = e => {
    e.preventDefault()

    setIsBusy(true)

    const promise = participation
      ? handleSubmitClickedExisting()
      : handleSubmitClickedNew()

    promise
      .then(({ data }) => {
        setSuccess(true)
        setError(null)
        dispatch(setCurrentParticipation(data))
      })
      .catch(err => {
        setError({
          code: err.response ? err.response.status : 503,
          ...(err.response ? err.response.data : {}),
        })
      })
      .finally(() => {
        setIsBusy(false)
      })
  }

  let formContent = null
  let isFormValid = true
  let showBackButton = true

  if (step === StepEnum.Days) {
    const handleDaysFormChange = ({ tableType, day, boothSize, category }) => {
      if (tableType !== selectedTableType) {
        setSelectedTableType(tableType)
      }
      if (day !== selectedDay) {
        setSelectedDay(day)
      }
      if (boothSize !== selectedBoothSize) {
        setSelectedBoothSize(boothSize)
      }
      if (category !== selectedCategory) {
        setSelectedCategory(category)
      }
    }

    isFormValid =
      selectedTableType !== undefined &&
      selectedDay !== undefined &&
      selectedBoothSize !== undefined &&
      selectedCategory !== undefined &&
      getTimeslot(
        availableTimeslots,
        selectedDay,
        selectedBoothSize,
        selectedCategory
      )

    formContent = (
      <RegistrationDaysForm
        fair={fair}
        availableTimeslots={availableTimeslots}
        tableType={selectedTableType}
        day={selectedDay}
        boothSize={selectedBoothSize}
        category={selectedCategory}
        onChange={handleDaysFormChange}
      />
    )
  } else if (step === StepEnum.Packages) {
    const handlePackagesFormChange = newSelectedPackages => {
      setSelectedPackages([...newSelectedPackages])
    }

    formContent = (
      <RegistrationPackagesForm
        fair={fair}
        availablePackages={availablePackages}
        selectedPackages={selectedPackages}
        onChange={handlePackagesFormChange}
      />
    )
  } else if (step === StepEnum.Checkout && !participation) {
    const handleCheckoutFormChange = ({
      acceptedTAC: newAcceptedTAC,
      acceptedTermsPermittedItems: newAcceptedTermsPermittedItems,
      comment: newComment,
    }) => {
      if (newAcceptedTAC !== acceptedTAC) {
        setAcceptedTAC(newAcceptedTAC)
      }
      if (newAcceptedTermsPermittedItems !== acceptedTermsPermittedItems) {
        setAcceptedTermsPermittedItems(newAcceptedTermsPermittedItems)
      }
      if (newComment !== comment) {
        setComment(newComment)
      }
    }

    isFormValid = acceptedTAC && acceptedTermsPermittedItems

    formContent = (
      <RegistrationCheckoutNewForm
        fair={fair}
        availableTimeslots={availableTimeslots}
        availablePackages={availablePackages}
        day={selectedDay}
        boothSize={selectedBoothSize}
        tableType={selectedTableType}
        category={selectedCategory}
        packages={selectedPackages}
        comment={comment}
        acceptedTAC={acceptedTAC}
        acceptedTermsPermittedItems={acceptedTermsPermittedItems}
        onChange={handleCheckoutFormChange}
      />
    )
  } else if (step === StepEnum.Checkout) {
    const handleCheckoutFormChange = ({
      acceptedTAC: newAcceptedTAC,
      acceptedTermsPermittedItems: newAcceptedTermsPermittedItems,
      comment: newComment,
    }) => {
      if (newAcceptedTAC !== acceptedTAC) {
        setAcceptedTAC(newAcceptedTAC)
      }
      if (newAcceptedTermsPermittedItems !== acceptedTermsPermittedItems) {
        setAcceptedTermsPermittedItems(newAcceptedTermsPermittedItems)
      }
      if (newComment !== comment) {
        setComment(newComment)
      }
    }

    isFormValid = acceptedTAC && acceptedTermsPermittedItems
    showBackButton = false

    formContent = (
      <RegistrationCheckoutExistingForm
        fair={fair}
        company={company}
        participation={participation}
        comment={comment}
        acceptedTAC={acceptedTAC}
        acceptedTermsPermittedItems={acceptedTermsPermittedItems}
        onChange={handleCheckoutFormChange}
      />
    )
  }

  return (
    <Form>
      <GridRow>
        <Step.Group stackable="tablet" fluid>
          <Step
            disabled={!showBackButton}
            active={step === StepEnum.Days}
            onClick={() => changeToStep(StepEnum.Days)}
          >
            <Icon name="calendar alternate outline" />
            <Step.Content>
              <Step.Title>
                <FormattedMessage id="companyPortal.registration.days.title" />
              </Step.Title>
              <Step.Description>
                <FormattedMessage id="companyPortal.registration.days.description" />
              </Step.Description>
            </Step.Content>
          </Step>

          <Step
            disabled={!showBackButton || step === StepEnum.Days}
            active={step === StepEnum.Packages}
            onClick={() => changeToStep(StepEnum.Packages)}
          >
            <Icon name="options" />
            <Step.Content>
              <Step.Title>
                <FormattedMessage id="companyPortal.registration.packages.title" />
              </Step.Title>
              <Step.Description>
                <FormattedMessage id="companyPortal.registration.packages.description" />
              </Step.Description>
            </Step.Content>
          </Step>

          <Step
            disabled={step === StepEnum.Days || step === StepEnum.Packages}
            active={step === StepEnum.Checkout}
          >
            <Icon name="info" />
            <Step.Content>
              <Step.Title>
                <FormattedMessage id="companyPortal.registration.checkout.title" />
              </Step.Title>
              <Step.Description>
                <FormattedMessage id="companyPortal.registration.checkout.description" />
              </Step.Description>
            </Step.Content>
          </Step>
        </Step.Group>
      </GridRow>
      <Segment>
        {formContent}
        <ActionBar>
          {success && (
            <Message positive size="small">
              <FormattedMessage id="companyPortal.registration.success" />
            </Message>
          )}
          {error && (
            <Message negative size="small">
              <FormattedMessage
                id={`companyPortal.registration.error${error.code}`}
                defaultMessage={
                  <FormattedMessage
                    id="errorUnknown"
                    values={{
                      code: error.code || 0,
                      contactEmail: <EmailLink />,
                    }}
                  />
                }
              />
            </Message>
          )}
          {step !== StepEnum.Days && showBackButton && (
            <Button
              className={classes.actionLeft}
              size="small"
              onClick={handleBackClicked}
            >
              <FormattedMessage id="back" />
            </Button>
          )}
          {step !== StepEnum.Checkout && (
            <Button
              primary
              className={classes.actionRight}
              size="small"
              disabled={!isFormValid}
              onClick={handleProceedClicked}
            >
              <FormattedMessage id="proceed" />
            </Button>
          )}
          {step === StepEnum.Checkout && (
            <Button
              className={classes.actionRight}
              size="small"
              color="green"
              disabled={!isFormValid}
              loading={isBusy}
              onClick={handleSubmitClicked}
            >
              <FormattedMessage id="submit" />
            </Button>
          )}
        </ActionBar>
      </Segment>
    </Form>
  )
}

RegistrationForm.propTypes = {
  fair: PropTypes.object,
  company: PropTypes.object,
  availableTimeslots: PropTypes.arrayOf(PropTypes.object),
  availablePackages: PropTypes.arrayOf(PropTypes.object),
  participation: PropTypes.object,
}

export default RegistrationForm
