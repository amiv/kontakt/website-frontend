import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'
import { Table } from 'semantic-ui-react'

import classes from './registrationOverviewTable.module.less'

import {
  BoothSizeSelectionEnum,
  TableTypeSelectionEnum,
  DaySelectionEnum,
  SignupTableTypeSelectionEnum,
} from './registrationDaysForm'

const RegistrationOverviewTable = ({
  fair,
  booths,
  packages,
  totalPrice,
  startupDiscount,
}) => {
  const intl = useIntl()
  const f = (id, values) => intl.formatMessage({ id }, values)

  return (
    <>
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              <FormattedMessage id="companyPortal.registration.checkout.position" />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <FormattedMessage id="companyPortal.registration.checkout.amount" />
            </Table.HeaderCell>
            <Table.HeaderCell textAlign="right">
              <FormattedMessage id="companyPortal.registration.checkout.unitCost" />
            </Table.HeaderCell>
            <Table.HeaderCell textAlign="right">
              <FormattedMessage id="price" /> (*)
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {booths.map(booth => {
            let amountKey

            if (booth.day === DaySelectionEnum.First) {
              amountKey = 'firstDay'
            } else if (booth.day === DaySelectionEnum.Second) {
              amountKey = 'secondDay'
            } else if (booth.day === DaySelectionEnum.Both) {
              amountKey = 'bothDays'
            } else if (booth.day === DaySelectionEnum.Third) {
              amountKey = 'thirdDay'
            } else if (booth.day === DaySelectionEnum.Second_and_Third) {
              amountKey = 'secondAndThirdDay'
            }
            // TODO: Add support for Second_and_Third

            return (
              <Table.Row key={booth.id}>
                <Table.Cell>
                  <FormattedMessage
                    id="companyPortal.registration.checkout.booth"
                    values={{
                      fairName: fair.kontakt_title,
                      category: booth.category,
                      boothSize:
                        booth.boothSize === BoothSizeSelectionEnum.Small
                          ? f('companyPortal.boothSmall')
                          : f('companyPortal.boothLarge'),
                      tableType:
                        booth.tableType === TableTypeSelectionEnum.Low ||
                        booth.tableType === SignupTableTypeSelectionEnum.Low
                          ? f('companyPortal.tableLow')
                          : f('companyPortal.tableHigh'),
                    }}
                  />
                </Table.Cell>
                <Table.Cell>
                  <FormattedMessage
                    id={`companyPortal.registration.checkout.${amountKey}`}
                  />
                </Table.Cell>
                <Table.Cell textAlign="right">
                  {booth.day === DaySelectionEnum.Both
                    ? booth.price / 2
                    : booth.price}{' '}
                  CHF
                </Table.Cell>
                <Table.Cell textAlign="right">{booth.price} CHF</Table.Cell>
              </Table.Row>
            )
          })}

          {packages.map(_package => {
            const isGerman = intl.locale === 'de'
            const title = isGerman ? _package.title_de : _package.title_en

            return (
              <Table.Row key={_package.id}>
                <Table.Cell colSpan={3}>{title}</Table.Cell>
                <Table.Cell textAlign="right">{_package.price} CHF</Table.Cell>
              </Table.Row>
            )
          })}

          {startupDiscount && (
            <Table.Row>
              <Table.Cell colSpan={3}>
                <FormattedMessage id="companyPortal.registration.checkout.startupDiscount" />
              </Table.Cell>
              <Table.Cell textAlign="right">{-startupDiscount} CHF</Table.Cell>
            </Table.Row>
          )}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="3">
              <FormattedMessage id="totalPrice" />
            </Table.HeaderCell>
            <Table.HeaderCell textAlign="right">
              {totalPrice} CHF
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <p className={classes.vatHint}>
        (*) <FormattedMessage id="companyPortal.registration.excludingVAT" />
      </p>
    </>
  )
}

RegistrationOverviewTable.propTypes = {
  fair: PropTypes.object.isRequired,
  booths: PropTypes.arrayOf(PropTypes.object).isRequired,
  packages: PropTypes.arrayOf(PropTypes.object).isRequired,
  totalPrice: PropTypes.number.isRequired,
  startupDiscount: PropTypes.number,
}

export default RegistrationOverviewTable
