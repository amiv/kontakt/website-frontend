import React from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'

import classes from './downloadLink.module.less'

const Textbox = ({ children, marginTop, marginBottom, ...props }) => {
  return (
    <p
      className={clsx(marginTop && classes.top, marginBottom && classes.bottom)}
      {...props}
    >
      {children}
    </p>
  )
}

Textbox.propTypes = {
  children: PropTypes.node,
  marginTop: PropTypes.bool,
  marginBottom: PropTypes.bool,
}

Textbox.defaultProps = {
  marginTop: false,
  marginBottom: false,
}

export default Textbox
