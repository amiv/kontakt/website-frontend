import React from 'react'
import Link from 'next/link'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { Message } from 'semantic-ui-react'

const CompanyProfileHint = ({ fair, participation }) => {
  const now = Date.now()
  const registrationStart = new Date(fair.registration_start)

  if (
    registrationStart < now &&
    participation &&
    participation.accepted_participation_terms &&
    participation.accepted_permitted_items_terms
  ) {
    return (
      <Message warning>
        <Message.Header>
          <FormattedMessage id="companyPortal.profile.hint.title" />
        </Message.Header>
        <p>
          <FormattedMessage
            id="companyPortal.profile.hint.text"
            values={{
              profileLink: function ProfileLink(...chunks) {
                return (
                  <Link href="/company-portal/profile">
                    <a>{chunks}</a>
                  </Link>
                )
              },
            }}
          />
        </p>
      </Message>
    )
  }

  return null
}

CompanyProfileHint.propTypes = {
  fair: PropTypes.object,
  participation: PropTypes.object,
}

export default CompanyProfileHint
