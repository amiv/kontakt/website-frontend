import React from 'react'
import PropTypes from 'prop-types'
import { GridRow } from 'semantic-ui-react'

import classes from './actionBar.module.less'

const ActionBar = ({ children }) => {
  return <GridRow className={classes.container}>{children}</GridRow>
}

ActionBar.propTypes = {
  children: PropTypes.node.isRequired,
}

export default ActionBar
