import React from 'react'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import { FormattedMessage, useIntl } from 'react-intl'
import { Menu, Segment } from 'semantic-ui-react'
import ActionBar from './actionBar'

const AccountLayout = ({ children, actionComponent, activeItem }) => {
  const router = useRouter()
  const intl = useIntl()

  return (
    <React.Fragment>
      <Segment>
        <h2>
          <FormattedMessage id="companyPortal.account.title" />
        </h2>
        <p>
          <FormattedMessage id="companyPortal.account.text" />
        </p>
        <Menu pointing secondary>
          <Menu.Item
            name={intl.formatMessage({
              id: 'companyPortal.account.contactInformation.title',
            })}
            active={activeItem === 'contact'}
            onClick={() => router.push('/company-portal/account')}
          />
          <Menu.Item
            name={intl.formatMessage({
              id: 'companyPortal.account.changePassword.title',
            })}
            active={activeItem === 'password'}
            onClick={() =>
              router.push('/company-portal/account/change-password')
            }
          />
        </Menu>
        {children}
      </Segment>
      <ActionBar>{actionComponent}</ActionBar>
    </React.Fragment>
  )
}

AccountLayout.propTypes = {
  children: PropTypes.node.isRequired,
  actionComponent: PropTypes.node.isRequired,
  activeItem: PropTypes.oneOf(['contact', 'password']),
}

export default AccountLayout
