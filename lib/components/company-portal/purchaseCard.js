import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage, useIntl } from 'react-intl'

import classes from './purchaseCard.module.less'

const PurchaseCard = ({ fair, participation, ...props }) => {
  const intl = useIntl()

  const dateFormatOptions = {
    weekday: 'long',
    day: '2-digit',
    month: 'long',
    year: 'numeric',
  }

  return (
    <div className={classes.container} {...props}>
      <h3 className={classes.header}>{fair.kontakt_title}</h3>
      {participation.booths_booked.map(booth_booked => {
        let dayString

        if (booth_booked.day === 'BOTH') {
          dayString = `${intl.formatDate(
            fair.date_start,
            dateFormatOptions
          )} ${intl.formatMessage({ id: 'and' })} ${intl.formatDate(
            fair.date_end,
            dateFormatOptions
          )}`
        } else if (booth_booked.day === 'SECOND') {
          dayString = intl.formatDate(fair.date_end, dateFormatOptions)
        } else {
          dayString = intl.formatDate(fair.date_start, dateFormatOptions)
        }
        // TODO: fix to three days

        return (
          <div className={classes.booth} key={booth_booked.id}>
            <h4 className={classes.boothDays}>{dayString}</h4>
            <h4>
              <FormattedMessage id="companyPortal.purchases.category" />{' '}
              {booth_booked.booth_option.booth_category}
            </h4>
            <h4>
              {booth_booked.booth_size === 'LARGE' ? (
                <FormattedMessage id="companyPortal.boothLarge" />
              ) : (
                <FormattedMessage id="companyPortal.boothSmall" />
              )}
            </h4>
            <h4>
              {booth_booked.table_type === 'LOW' ? (
                <FormattedMessage id="companyPortal.tableLow" />
              ) : (
                <FormattedMessage id="companyPortal.tableHigh" />
              )}
            </h4>
          </div>
        )
      })}
      {participation.packages_booked.map(_package => {
        const isGerman = intl.locale === 'de'
        const title = isGerman
          ? _package.package_option.title_de
          : _package.package_option.title_en

        return <h4 key={_package.package_id}>{title}</h4>
      })}
    </div>
  )
}

PurchaseCard.propTypes = {
  participation: PropTypes.object.isRequired,
  fair: PropTypes.object.isRequired,
}

export default PurchaseCard
