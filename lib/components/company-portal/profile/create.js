import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { Button, Header, Message, Segment } from 'semantic-ui-react'

import { makeApiRequest } from 'lib/utils/api'
import { selectCSRFToken } from 'lib/store/slices/authSlice'
import { setCurrentProfile } from 'lib/store/slices/currentProfileSlice'
import EmailLink from 'lib/components/emailLink'

import classes from './create.module.less'

const CreateProfileComponent = () => {
  const [isBusy, setIsBusy] = useState(false)
  const [error, setError] = useState(null)
  const csrfToken = useSelector(selectCSRFToken)
  const dispatch = useDispatch()
  const handleCreateClicked = e => {
    e.preventDefault()

    setIsBusy(true)

    const action = {
      method: 'POST',
      resource: 'company_profiles/create',
    }
    const promise = makeApiRequest(action, csrfToken, dispatch)

    promise
      .then(({ data }) => {
        setError(null)
        dispatch(setCurrentProfile(data))
      })
      .catch(err => {
        setError({
          code: err.response ? err.response.status : 503,
          ...(err.response ? err.response.data : {}),
        })
      })
      .finally(() => {
        setIsBusy(false)
      })
  }

  return (
    <Segment placeholder>
      <Header className={classes.createHeader}>
        <FormattedMessage id="companyPortal.profile.notExisting" />
      </Header>
      {error && (
        <Message negative size="small">
          <FormattedMessage
            id={`companyPortal.profile.error${error.code}`}
            defaultMessage={
              <FormattedMessage
                id="errorUnknown"
                values={{
                  code: error.code || 0,
                  contactEmail: <EmailLink />,
                }}
              />
            }
          />
        </Message>
      )}
      <Button primary loading={isBusy} onClick={handleCreateClicked}>
        <FormattedMessage id="companyPortal.profile.create" />
      </Button>
    </Segment>
  )
}

export default CreateProfileComponent
