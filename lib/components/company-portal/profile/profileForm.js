import React, { useEffect, useState, useRef } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage, useIntl } from 'react-intl'
import {
  Button,
  Form,
  Grid,
  GridRow,
  Input,
  Message,
  Segment,
} from 'semantic-ui-react'

import { getPublicConfig } from 'lib/utils/config'
import { selectCSRFToken } from 'lib/store/slices/authSlice'
import { setCurrentProfile } from 'lib/store/slices/currentProfileSlice'
import { makeApiRequest } from 'lib/utils/api'

import ActionBar from '../actionBar'
import EmailLink from '../../emailLink'

import classes from './profileForm.module.less'

const ProfileForm = ({ fair, company, participation, profile }) => {
  const [logoFile, setLogoFile] = useState(null)
  const [logoError, setLogoError] = useState(null)
  const [advertisementFile, setAdvertisementFile] = useState(null)
  const [advertisementError, setAdvertisementError] = useState(null)
  const [contactForStudents, setContactForStudents] = useState('')
  const [website, setWebsite] = useState('')
  const [employeesSwitzerland, setEmployeesSwitzerland] = useState('')
  const [employeesWorldwide, setEmployeesWorldwide] = useState('')
  const [aboutUs, setAboutUs] = useState('')
  const [focus, setFocus] = useState('')
  const [interests, setInterests] = useState({})
  const [offers, setOffers] = useState({})
  const [locations, setLocations] = useState('')
  const [isBusy, setIsBusy] = useState(false)
  const [error, setError] = useState(null)
  const [success, setSuccess] = useState(false)
  const csrfToken = useSelector(selectCSRFToken)
  const dispatch = useDispatch()
  const intl = useIntl()
  const previewLinkRef = useRef(null)

  const { apiUrl, contractorUrl } = getPublicConfig()
  const hasFairguideAdvertisement =
    participation &&
    participation.packages_booked &&
    participation.packages_booked.some(
      bookedPackage => bookedPackage.package_option.includes_ad
    )
  const isFormValid = true

  useEffect(() => {
    if (profile) {
      setContactForStudents(profile.contact_for_students || '')
      setWebsite(profile.website || '')
      setEmployeesSwitzerland(profile.employees_ch || '')
      setEmployeesWorldwide(profile.employees_world || '')
      setAboutUs(profile.about_us || '')
      setFocus(profile.focus || '')
      setInterests(profile.interesting_students)
      setOffers(profile.offers)
      setLocations(profile.locations || '')
    }
  }, [profile])

  const handleLogoFileSelected = e => {
    if (e.target.files.length === 0) {
      setLogoFile(null)
      setLogoError(null)
      return
    }
    const f = e.target.files[0]
    if (f.type !== 'image/svg+xml') {
      setLogoError(
        intl.formatMessage({ id: 'companyPortal.profile.logo.error.filetype' })
      )
      return
    }
    if (f.size > 16000000) {
      setLogoError(
        intl.formatMessage({ id: 'companyPortal.profile.logo.error.filesize' })
      )
      return
    }
    setLogoFile(f)
    setLogoError(null)
  }

  const handleAdvertisementFileSelected = e => {
    if (e.target.files.length === 0) {
      setAdvertisementFile(null)
      setAdvertisementError(null)
      return
    }
    const f = e.target.files[0]
    if (f.type !== 'application/pdf') {
      setAdvertisementError(
        intl.formatMessage({
          id: 'companyPortal.profile.advertisement.error.filetype',
        })
      )
      return
    }
    if (f.size > 16000000) {
      setAdvertisementError(
        intl.formatMessage({
          id: 'companyPortal.profile.advertisement.error.filesize',
        })
      )
      return
    }
    setAdvertisementFile(f)
    setAdvertisementError(null)
  }

  const handleInterests = newValue => {
    setInterests({ ...interests, [newValue.value]: newValue.checked })
  }

  const handleOffers = newValue => {
    setOffers({ ...offers, [newValue.value]: newValue.checked })
  }

  const handleSubmitClicked = e => {
    e.preventDefault()

    if (!isFormValid || isBusy) return

    setIsBusy(true)

    const data = new FormData()
    data.append('contact_for_students', contactForStudents)
    data.append('website', website)
    if (employeesSwitzerland) {
      data.append('employees_ch', employeesSwitzerland)
    }
    if (employeesWorldwide) {
      data.append('employees_world', employeesWorldwide)
    }
    data.append('about_us', aboutUs)
    data.append('focus', focus)
    data.append('interesting_students', JSON.stringify(interests))
    data.append('offers', JSON.stringify(offers))
    data.append('locations', locations)
    data.set('logo', logoFile)
    data.set('advertisement', advertisementFile)

    const updateAction = {
      method: 'PATCH',
      dataType: 'multipart/form-data',
      resource: `company_profiles/${profile.id}`,
      data,
    }
    const previewUrl = `${contractorUrl}/fairguidepage/pdf/${participation.company_id}`

    // Make async requests with an inner function so that the rendering thread
    // is not blocked during that time.
    const makeRequests = async () => {
      try {
        const updateResponse = await makeApiRequest(
          updateAction,
          csrfToken,
          dispatch
        )
        setCurrentProfile(updateResponse)

        const previewResponse = await axios.get(previewUrl, {
          responseType: 'blob',
          timeout: 180000,
        })

        const href = window.URL.createObjectURL(previewResponse.data)
        const a = previewLinkRef.current
        a.download = 'amiv_kontakt_fairguide_preview.pdf'
        a.href = href
        setSuccess(true)
        setError(null)
        a.click()
        a.href = ''
      } catch (err) {
        setSuccess(false)
        setError({
          code: err.response ? err.response.status : 503,
          ...(err.response ? err.response.data : {}),
        })
      } finally {
        setIsBusy(false)
      }
    }

    makeRequests()
  }

  const formContent = (
    <>
      <a ref={previewLinkRef} />
      {profile.logo && (
        <GridRow>
          <img
            className="ui medium image"
            src={`${apiUrl}${profile.logo}`}
            alt="Company Logo"
          />
        </GridRow>
      )}
      <Message warning className={classes.hint}>
        <p>
          <FormattedMessage id="companyPortal.profile.completionHint.text" />
        </p>
      </Message>
      <Grid>
        <Grid.Row columns={1}>
          <Grid.Column>
            <Input
              type="file"
              accept="image/svg+xml, .svg"
              error={logoError}
              label={intl.formatMessage({
                id: 'companyPortal.profile.logo.label',
              })}
              onChange={handleLogoFileSelected}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            <Form.TextArea
              label={intl.formatMessage({
                id: 'companyPortal.profile.contactForStudents',
              })}
              value={contactForStudents}
              placeholder="kontaktmesse@amiv.ethz.ch"
              onChange={e => setContactForStudents(e.target.value)}
            />
          </Grid.Column>
          <Grid.Column>
            <Grid.Row>
              <Form.Input
                fluid
                label={intl.formatMessage({
                  id: 'companyPortal.profile.website',
                })}
                value={website}
                placeholder="amiv.ethz.ch"
                onChange={e => setWebsite(e.target.value)}
              />
            </Grid.Row>
            <Grid.Row>
              <Form.Input
                fluid
                label={intl.formatMessage({
                  id: 'companyPortal.profile.employeesSwitzerland',
                })}
                type="number"
                value={employeesSwitzerland}
                placeholder="20"
                onChange={e => setEmployeesSwitzerland(e.target.value)}
              />
            </Grid.Row>
            <Grid.Row>
              <Form.Input
                fluid
                label={intl.formatMessage({
                  id: 'companyPortal.profile.employeesWorldwide',
                })}
                type="number"
                value={employeesWorldwide}
                placeholder="400"
                onChange={e => setEmployeesWorldwide(e.target.value)}
              />
            </Grid.Row>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            <Form.Group widths="equal">
              <Form.TextArea
                label={intl.formatMessage({
                  id: 'companyPortal.profile.description',
                })}
                value={aboutUs}
                placeholder={intl.formatMessage({
                  id: 'companyPortal.profile.descriptionPlaceholder',
                })}
                onChange={e => setAboutUs(e.target.value)}
              />
            </Form.Group>
          </Grid.Column>
          <Grid.Column>
            <Form.Group widths="equal">
              <Form.TextArea
                label={intl.formatMessage({
                  id: 'companyPortal.profile.focus',
                })}
                value={focus}
                placeholder={intl.formatMessage({
                  id: 'companyPortal.profile.focusPlaceholder',
                })}
                onChange={e => setFocus(e.target.value)}
              />
            </Form.Group>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={1}>
          <Grid.Column>
            <Form.Field
              label={intl.formatMessage({
                id: 'companyPortal.profile.interestingStudents.label',
              })}
            />
            <Form.Group widths="equal">
              <Form.Checkbox
                label={intl.formatMessage({
                  id: 'companyPortal.profile.interestingStudents.itet',
                })}
                value="itet"
                onChange={(_, newValue) => handleInterests(newValue)}
                checked={interests.itet}
              />
              <Form.Checkbox
                label={intl.formatMessage({
                  id: 'companyPortal.profile.interestingStudents.mavt',
                })}
                value="mavt"
                onChange={(_, newValue) => handleInterests(newValue)}
                checked={interests.mavt}
              />
              <Form.Checkbox
                label={intl.formatMessage({
                  id: 'companyPortal.profile.interestingStudents.mtec',
                })}
                value="mtec"
                onChange={(_, newValue) => handleInterests(newValue)}
                checked={interests.mtec}
              />
            </Form.Group>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={1}>
          <Grid.Column>
            <Form.Field
              label={intl.formatMessage({
                id: 'companyPortal.profile.offers.label',
              })}
            />
            <Form.Group widths="equal">
              <Form.Checkbox
                label={intl.formatMessage({
                  id: 'companyPortal.profile.offers.bachelorThesis',
                })}
                value="bachelor"
                onChange={(_, newValue) => handleOffers(newValue)}
                checked={offers.bachelor}
              />
              <Form.Checkbox
                label={intl.formatMessage({
                  id: 'companyPortal.profile.offers.internships',
                })}
                value="internship"
                onChange={(_, newValue) => handleOffers(newValue)}
                checked={offers.internship}
              />
              <Form.Checkbox
                label={intl.formatMessage({
                  id: 'companyPortal.profile.offers.semesterThesis',
                })}
                value="semester"
                onChange={(_, newValue) => handleOffers(newValue)}
                checked={offers.semester}
              />
              <Form.Checkbox
                label={intl.formatMessage({
                  id: 'companyPortal.profile.offers.masterThesis',
                })}
                value="master"
                onChange={(_, newValue) => handleOffers(newValue)}
                checked={offers.master}
              />
              <Form.Checkbox
                label={intl.formatMessage({
                  id: 'companyPortal.profile.offers.fulltime',
                })}
                value="fulltime"
                onChange={(_, newValue) => handleOffers(newValue)}
                checked={offers.fulltime}
              />
            </Form.Group>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={1}>
          <Grid.Column>
            <Form.Group widths="equal">
              <Form.Input
                fluid
                label={intl.formatMessage({
                  id: 'companyPortal.profile.locations',
                })}
                value={locations}
                placeholder={intl.formatMessage({
                  id: 'companyPortal.profile.locationsPlaceholder',
                })}
                onChange={e => setLocations(e.target.value)}
              />
            </Form.Group>
          </Grid.Column>
        </Grid.Row>
        {hasFairguideAdvertisement && (
          <>
            <Grid.Row columns={2}>
              <Grid.Column>
                <Input
                  type="file"
                  accept="application/pdf"
                  label={intl.formatMessage({
                    id: 'companyPortal.profile.advertisement.label',
                  })}
                  error={advertisementError}
                  onChange={handleAdvertisementFileSelected}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={1}>
              <Grid.Column>
                {profile.advertisement ? (
                  <Form.Field
                    label={intl.formatMessage({
                      id: 'companyPortal.profile.advertisement.uploaded',
                    })}
                  />
                ) : (
                  <Form.Field
                    label={intl.formatMessage({
                      id: 'companyPortal.profile.advertisement.notUploaded',
                    })}
                  />
                )}
              </Grid.Column>
            </Grid.Row>
          </>
        )}
      </Grid>
    </>
  )

  return (
    <Form>
      <Segment>
        <h2>
          <FormattedMessage
            id="companyPortal.profile.title"
            values={{ fairName: fair ? fair.kontakt_title : 'Kontakt' }}
          />
        </h2>
        <p>
          <FormattedMessage
            id="companyPortal.profile.text"
            values={{
              companyName: company ? (
                company.company_name
              ) : (
                <FormattedMessage id="companyPortal.yourCompany" />
              ),
            }}
          />
        </p>
        {formContent}
        <ActionBar>
          {success && (
            <Message positive size="small">
              <FormattedMessage id="companyPortal.profile.success" />
            </Message>
          )}
          {error && (
            <Message negative size="small" className={classes.error}>
              <FormattedMessage
                id={`companyPortal.profile.error${error.code}`}
                values={{
                  code: error.code || 0,
                }}
                defaultMessage={
                  <FormattedMessage
                    id="errorUnknown"
                    values={{
                      code: error.code || 0,
                      contactEmail: <EmailLink />,
                    }}
                  />
                }
              />
            </Message>
          )}
          <Button
            className={classes.actionRight}
            size="small"
            color="green"
            disabled={!isFormValid}
            loading={isBusy}
            onClick={handleSubmitClicked}
          >
            <FormattedMessage id="companyPortal.profile.submit" />
          </Button>
        </ActionBar>
      </Segment>
    </Form>
  )
}

ProfileForm.propTypes = {
  fair: PropTypes.object,
  company: PropTypes.object,
  participation: PropTypes.object,
  profile: PropTypes.object,
}

export default ProfileForm
