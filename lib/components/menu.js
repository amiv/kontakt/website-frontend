import React from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'

import classes from './menu.module.less'

const Menu = ({ children, className, ...props }) => {
  return (
    <ul className={clsx(classes.list, className)} {...props}>
      {children}
    </ul>
  )
}

Menu.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
}

export default Menu
