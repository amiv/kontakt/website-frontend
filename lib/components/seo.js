/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react'
import PropTypes from 'prop-types'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'

import { removeLocalePart } from 'lib/utils/intl'
import { getPublicConfig } from 'lib/utils/config'

const SEO = ({ description, title, meta, noindex }) => {
  const { locale, locales, pathname: currentPathname, query } = useRouter()
  const {
    title: siteTitle,
    description: defaultDescription,
    author,
  } = getPublicConfig()
  const { formatMessage } = useIntl()
  const f = id => formatMessage({ id })

  const translatedTitle = title ? f(title) : undefined
  const translatedSiteTitle = f(siteTitle)
  const translatedDescription = f(description || defaultDescription)
  const finalTitle = translatedTitle
    ? `${translatedTitle} | ${translatedSiteTitle}`
    : translatedSiteTitle

  const pathname = removeLocalePart(currentPathname)
  const queryString =
    Object.keys(query).length > 0
      ? `?${Object.keys(query)
          .map(key => `${key}=${query[key]}`)
          .join('&')}`
      : ''

  return (
    <Head>
      <title>{finalTitle}</title>
      <meta name="description" content={translatedDescription} />
      <meta property="og:type" content="website" />
      <meta property="og:title" content={finalTitle} />
      <meta property="og:description" content={translatedDescription} />
      <meta property="og:site_name" content={translatedSiteTitle} />
      <meta property="twitter:card" content="summary" />
      <meta property="twitter:creator" content={author} />
      <meta property="twitter:title" content={finalTitle} />
      <meta property="twitter:description" content={translatedDescription} />
      {noindex && <meta property="robots" content="noindex" />}
      {meta}
      {locales
        .filter(
          otherLocale => otherLocale !== 'catchAll' && otherLocale !== locale
        )
        .map(otherLocale => {
          const link = `/${otherLocale}${pathname}${queryString}`

          return (
            <link
              rel="alternate"
              href={link}
              hrefLang={otherLocale}
              key={otherLocale}
            />
          )
        })}
    </Head>
  )
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string,
  noindex: PropTypes.bool,
}

SEO.defaultProps = {
  meta: [],
  noindex: false,
}

export default SEO
