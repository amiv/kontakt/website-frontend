import React from 'react'
import { MapContainer, TileLayer, Marker } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'

import { iconMarker } from './icons'

import 'leaflet/dist/images/marker-shadow.png'
import 'leaflet/dist/images/marker-icon.png'

const Map = () => {
  return (
    <MapContainer
      center={[47.37796, 8.546553]}
      zoom={17}
      scrollWheelZoom={false}
      style={{ height: 400, width: '100%' }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[47.37776, 8.54663]} icon={iconMarker} />
    </MapContainer>
  )
}

export default Map
