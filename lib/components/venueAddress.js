import React from 'react'
import { FormattedMessage } from 'react-intl'

const VenueAddress = () => {
  return (
    <>
      <table>
        <tr>
          <th align="left">
            <FormattedMessage id="address" />
          </th>
        </tr>
        <tr>
          <td width="500px">CLA</td>
          <td>LEE</td>
        </tr>
        <tr>
          <td>Tannenstrasse 3</td>
          <td>Leonhardstrasse 21</td>
        </tr>
        <tr>
          <td>8092 Zürich</td>
          <td>8092 Zürich</td>
        </tr>
      </table>
    </>
  )
}

export default VenueAddress
