import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { Label } from 'semantic-ui-react'

import { getPublicConfig } from 'lib/utils/config'
import classes from './companyLogo.module.less'

const CompanyLogo = props => {
  const { apiUrl, premiumPartners, businessPartners, profileLogoOverride } =
    getPublicConfig()
  const { logo, name, profileId, boothLocation, website, className } = props

  let additionalLabelText
  let additionalLabelColor

  if (Object.keys(premiumPartners).includes(String(profileId))) {
    additionalLabelText = <FormattedMessage id="companies.partners.premium" />
    additionalLabelColor = 'yellow'
  } else if (Object.keys(businessPartners).includes(String(profileId))) {
    additionalLabelText = <FormattedMessage id="companies.partners.business" />
    additionalLabelColor = undefined
  }

  const logoUrl = profileLogoOverride[profileId] || `${apiUrl}${logo}`

  const content = (
    <div className={[classes.root, className].join(' ')} title={name}>
      <div className={classes.logo}>
        <img src={logoUrl} alt={name} />
      </div>
      {additionalLabelText && (
        <Label attached="bottom left" color={additionalLabelColor}>
          {additionalLabelText}
        </Label>
      )}
      {boothLocation && (
        <Label attached="bottom right">
          <span className={classes.boothLabel}>
            <FormattedMessage id="companies.booth" />
          </span>{' '}
          {boothLocation}
        </Label>
      )}
    </div>
  )

  if (website) {
    return (
      <a
        href={website.startsWith('http') ? website : `http://${website}`}
        target="_blank"
        rel="noopener noreferrer"
      >
        {content}
      </a>
    )
  }

  return content
}

CompanyLogo.propTypes = {
  logo: PropTypes.string,
  name: PropTypes.string.isRequired,
  profileId: PropTypes.number.isRequired,
  website: PropTypes.string,
  boothLocation: PropTypes.string,
  /** @ignore */
  className: PropTypes.string,
}

export default CompanyLogo
