import React from 'react'
import { FormattedMessage } from 'react-intl'

import { getPublicConfig } from 'lib/utils/config'

import classes from './contact.module.less'

const Contact = () => {
  const { contactEmail } = getPublicConfig()

  return (
    <div className={classes.container}>
      <span className={classes.title}>
        <FormattedMessage id="contact.title" />
      </span>
      <br />
      AMIV an der ETH c/o AMIV Kontakt.
      <br />
      CAB E37
      <br />
      Universitätstrasse 6<br />
      8092 Zürich
      <br />
      <br />
      +41 44 632 64 67
      <br />
      {contactEmail}
    </div>
  )
}

export default Contact
