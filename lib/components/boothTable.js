import React from 'react'
import { FormattedMessage } from 'react-intl'

import classes from './boothTable.module.less'

const BoothLayout = () => {
  return (
    <center>
      <img className={classes.boothPlan} src="/images/booth_layout.png" />
      <br />
      <div className={classes.container}>
        <div className={classes.dayContainer}>
          <p>
            <b>
              <FormattedMessage id="tuesday" />
            </b>
          </p>
          <table valign="top">
            <tbody>
              <tr>
                <td>
                  <b>A1</b>
                </td>
                <td>Meteomatics</td>
              </tr>
              <tr>
                <td>
                  <b>A2</b>
                </td>
                <td>Swiss Engineering STV</td>
              </tr>
              <tr>
                <td>
                  <b>A3</b>
                </td>
                <td>Rheinmetall Air Defence AG</td>
              </tr>
              <tr>
                <td>
                  <b>A4</b>
                </td>
                <td>Komax AG</td>
              </tr>
              <tr>
                <td>
                  <b>A5/A6</b>
                </td>
                <td>Sonova AG</td>
              </tr>
              <tr>
                <td>
                  <b>A7</b>
                </td>
                <td>Celeroton AG</td>
              </tr>
              <tr>
                <td>
                  <b>A8/A9</b>
                </td>
                <td>Gritec AG</td>
              </tr>
              <tr>
                <td>
                  <b>A10</b>
                </td>
                <td>Plexim GmbH</td>
              </tr>
              <tr>
                <td>
                  <b>A11</b>
                </td>
                <td>Syntegon Technology</td>
              </tr>
              <tr>
                <td>
                  <b>A12/A13</b>
                </td>
                <td>SBB CFF FFS</td>
              </tr>
              <tr>
                <td>
                  <b>A14</b>
                </td>
                <td>Wüest Partner AG</td>
              </tr>
              <tr>
                <td>
                  <b>A15</b>
                </td>
                <td>Sensirion AG</td>
              </tr>
              <tr>
                <td>
                  <b>A16</b>
                </td>
                <td>Stadler Rail Group</td>
              </tr>
              <tr>
                <td>
                  <b>A17</b>
                </td>
                <td>Zühlke Engineering AG</td>
              </tr>
              <tr>
                <td>
                  <b>A18</b>
                </td>
                <td>APP Unternehmensberatung AG</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <b>B1/B2</b>
                </td>
                <td>Chemspeed Technologies AG</td>
              </tr>
              <tr>
                <td>
                  <b>B3/B4</b>
                </td>
                <td>Open Systems AG</td>
              </tr>
              <tr>
                <td>
                  <b>B5</b>
                </td>
                <td>M&F Engineering AG</td>
              </tr>
              <tr>
                <td>
                  <b>B6/B7</b>
                </td>
                <td>TBF + Partner AG</td>
              </tr>
              <tr>
                <td>
                  <b>B8</b>
                </td>
                <td>Electrosuisse</td>
              </tr>
              <tr>
                <td>
                  <b>B9/B10</b>
                </td>
                <td>Axpo</td>
              </tr>
              <tr>
                <td>
                  <b>B11/B12</b>
                </td>
                <td>Rohde & Schwarz GmbH & Co. KG</td>
              </tr>
              <tr>
                <td>
                  <b>B13</b>
                </td>
                <td>hpo ag</td>
              </tr>
              <tr>
                <td>
                  <b>B14/B15</b>
                </td>
                <td>Siemens Schweiz AG</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <b>C1</b>
                </td>
                <td>Skope Magnetic Resonance Technologies</td>
              </tr>
              <tr>
                <td>
                  <b>C2</b>
                </td>
                <td>Hombrechtikon Systems Engineering AG</td>
              </tr>
              <tr>
                <td>
                  <b>C3</b>
                </td>
                <td>Dufour Aerospace</td>
              </tr>
              <tr>
                <td>
                  <b>C4</b>
                </td>
                <td>Sunflower Labs</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className={classes.dayContainer}>
          <p>
            <b>
              <FormattedMessage id="wednesday" />
            </b>
          </p>
          <table>
            <tbody>
              <tr>
                <td>
                  <b>A1</b>
                </td>
                <td>Meteomatics</td>
              </tr>
              <tr>
                <td>
                  <b>A2</b>
                </td>
                <td>thyssenkrupp Presta AG</td>
              </tr>
              <tr>
                <td>
                  <b>A3</b>
                </td>
                <td>Hombrechtikon Systems Engineering AG</td>
              </tr>
              <tr>
                <td>
                  <b>A4</b>
                </td>
                <td>Hilti AG</td>
              </tr>
              <tr>
                <td>
                  <b>A5/A6</b>
                </td>
                <td>ABB Schweiz AG</td>
              </tr>
              <tr>
                <td>
                  <b>A7</b>
                </td>
                <td>Linde Kryotechnik AG</td>
              </tr>
              <tr>
                <td>
                  <b>A8/A9</b>
                </td>
                <td>Gritec AG</td>
              </tr>
              <tr>
                <td>
                  <b>A10</b>
                </td>
                <td>Plexim GmbH</td>
              </tr>
              <tr>
                <td>
                  <b>A11</b>
                </td>
                <td>Bain & Company</td>
              </tr>
              <tr>
                <td>
                  <b>A12/A13</b>
                </td>
                <td>BELIMO Automation AG</td>
              </tr>
              <tr>
                <td>
                  <b>A14</b>
                </td>
                <td>Wüest Partner AG</td>
              </tr>
              <tr>
                <td>
                  <b>A15</b>
                </td>
                <td>Boston Consulting Group</td>
              </tr>
              <tr>
                <td>
                  <b>A16</b>
                </td>
                <td>maxon</td>
              </tr>
              <tr>
                <td>
                  <b>A17</b>
                </td>
                <td>Geberit</td>
              </tr>
              <tr>
                <td>
                  <b>A18</b>
                </td>
                <td>AWK Group AG</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <b>B1/B2</b>
                </td>
                <td>Chemspeed Technologies AG</td>
              </tr>
              <tr>
                <td>
                  <b>B3</b>
                </td>
                <td>Sentec AG</td>
              </tr>
              <tr>
                <td>
                  <b>B4</b>
                </td>
                <td>IST Innovative Sensor Technology</td>
              </tr>
              <tr>
                <td>
                  <b>B7</b>
                </td>
                <td>HUBER+SUHNER AG</td>
              </tr>
              <tr>
                <td>
                  <b>B8</b>
                </td>
                <td>Electrosuisse</td>
              </tr>
              <tr>
                <td>
                  <b>B9/B10</b>
                </td>
                <td>Hamilton Bonaduz AG</td>
              </tr>
              <tr>
                <td>
                  <b>B11</b>
                </td>
                <td>Swiss Engineering STV</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp; </td>
              </tr>
              <tr>
                <td>
                  <b>C1</b>
                </td>
                <td>Skope Magnetic Resonance Technologies</td>
              </tr>
              <tr>
                <td>
                  <b>C2</b>
                </td>
                <td>Bota Systems AG</td>
              </tr>
              <tr>
                <td>
                  <b>C3</b>
                </td>
                <td>Dufour Aerospace</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </center>
  )
}

export default BoothLayout
