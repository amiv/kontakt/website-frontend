import React from 'react'
import PropTypes from 'prop-types'
import { useIntl } from 'react-intl'

import classes from './supportingProgramItem.module.less'

const SupportingProgramItem = ({
  title,
  date,
  location,
  description,
  eventLink,
}) => {
  const intl = useIntl()
  const t = id => intl.formatMessage({ id })

  const dateFormatOptions = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    weekday: 'long',
    hour: 'numeric',
    minute: 'numeric',
  }

  return (
    <div className={classes.root}>
      <div className={classes.title}>{title}</div>
      <div className={classes.info}>
        <p>
          {t('supportingProgram.when')}:{' '}
          {intl.formatDate(date, dateFormatOptions)}
          <br />
          {t('supportingProgram.where')}: {location}
        </p>
        {eventLink && (
          <p>
            <a href={eventLink} target="_blank" rel="noreferrer">
              ➔ {t('supportingProgram.registration')}
            </a>
          </p>
        )}
      </div>
      <p className={classes.description}>{description}</p>
    </div>
  )
}

SupportingProgramItem.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  location: PropTypes.string.isRequired,
  description: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  eventLink: PropTypes.string.isRequired,
}

export default SupportingProgramItem
