import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import clsx from 'clsx'

import classes from './menuItem.module.less'

const MenuItem = ({ path, label, linkProps, className, ...props }) => {
  const { pathname: currentPath } = useRouter()
  const isActive =
    (path.length <= 4 && currentPath === path) ||
    (path.length > 4 && currentPath.includes(path))

  return (
    <li
      className={clsx(classes.item, className, isActive && 'active')}
      {...props}
    >
      <Link href={path}>
        <a {...linkProps}>
          <FormattedMessage id={label} />
        </a>
      </Link>
    </li>
  )
}

MenuItem.propTypes = {
  className: PropTypes.string,
  linkProps: PropTypes.object,
  path: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
}

export default MenuItem
