import React from 'react'

import { getPublicConfig } from 'lib/utils/config'

const EmailLink = () => {
  const { contactEmail } = getPublicConfig()
  return <a href={`mailto:${contactEmail}`}>{contactEmail}</a>
}

export default EmailLink
