import React from 'react'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import { Message } from 'semantic-ui-react'
import { useIntl } from 'react-intl'

import useAuth from 'lib/hooks/useAuth'
import ResourceState from 'lib/store/resourceState'

import Spinner from './spinner'
import Header from './header'
import SEO from './seo'
import classes from './layout.module.less'

const Layout = ({ seoProps, authenticatedOnly, children }) => {
  const [isAuthenticated, , state] = useAuth()
  const router = useRouter()
  const intl = useIntl()

  let content = children

  if (authenticatedOnly && !isAuthenticated) {
    if (state === ResourceState.Loading || state === ResourceState.Unknown) {
      content = (
        <div className={classes.authenticatedSpinner}>
          <Spinner centered size="large" />
        </div>
      )
    } else {
      router.push('/login')
      content = null
    }
  }

  return (
    <>
      <SEO noindex={authenticatedOnly} {...seoProps} />
      <div className={classes.container}>
        <Header />
        <div className={classes.content}>
          <noscript>
            <Message
              error
              header={intl.formatMessage({ id: 'javascriptDisabled.title' })}
              content={intl.formatMessage({ id: 'javascriptDisabled.text' })}
            />
          </noscript>
          {content}
        </div>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  authenticatedOnly: PropTypes.bool,
  seoProps: PropTypes.object,
}

Layout.defaultProps = {
  authenticatedOnly: false,
}

export default Layout
