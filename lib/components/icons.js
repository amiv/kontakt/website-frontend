import L from 'leaflet'
import 'leaflet/dist/leaflet.css'

const iconMarker = new L.Icon({
  iconUrl: '/images/marker_logo.svg',
  iconAnchor: [30, 80],
  popupAnchor: null,
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: [60, 100],
})

export { iconMarker }
