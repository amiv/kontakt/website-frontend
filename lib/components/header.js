import React, { useState } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { FormattedMessage, useIntl } from 'react-intl'
import { Label } from 'semantic-ui-react'
import clsx from 'clsx'

import Menu from './menu'
import MenuItem from './menuItem'
import BurgerIcon from './burgerIcon'
import classes from './header.module.less'

const Header = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false)
  const [showBurgerHint, setShowBurgerHint] = useState(true)
  const router = useRouter()
  const intl = useIntl()

  const toggleMobileMenu = () => {
    setShowMobileMenu(!showMobileMenu)
    setShowBurgerHint(false)
  }

  const otherLocale = intl.locale.split('-')[0] === 'en' ? 'de' : 'en'

  return (
    <header className={classes.container}>
      <Link href="/">
        <a className={classes.logoLink}>
          <img src="/images/logo_color.svg" alt="AMIV Kontakt Logo" />
        </a>
      </Link>
      <div className={classes.burger}>
        <BurgerIcon active={showMobileMenu} onClick={toggleMobileMenu} />
        <Label
          className={clsx(classes.burgerLabel, showBurgerHint && classes.show)}
          onClick={() => setShowBurgerHint(false)}
          size="large"
          pointing
        >
          <FormattedMessage id="menu.companyInfos" />
        </Label>
      </div>
      <nav
        className={clsx(
          classes.navigation,
          showMobileMenu && classes.navigation_active
        )}
      >
        <Menu className={classes.navigationGroup}>
          <MenuItem
            className={classes.navigationItem}
            path="/fairguide"
            label="menu.fairguide"
          />
          <MenuItem
            className={classes.navigationItem}
            path="/companies"
            label="menu.companies"
          />
          {/* <MenuItem
            className={classes.navigationItem}
            path="/cv-check"
            label="menu.cvCheck"
          /> */}
          <MenuItem
            className={classes.navigationItem}
            path="/supporting-program"
            label="menu.supportingProgram"
          />
          <MenuItem
            className={classes.navigationItem}
            path="/venue"
            label="menu.venue"
          />
        </Menu>
        <Menu className={clsx(classes.navigationGroup, classes.companyMenu)}>
          <MenuItem
            className={classes.navigationItem}
            path="/company-infos"
            label="menu.companyInfos"
          />
          <MenuItem
            className={classes.navigationItem}
            path="/company-portal"
            label="menu.companyPortal"
          />
        </Menu>
      </nav>
      <div className={clsx(classes.languageSwitch, classes.navigationGroup)}>
        <Link href={router.pathname} locale={otherLocale}>
          <a
            language={otherLocale}
            title={intl.formatMessage({ id: 'switchLanguage' })}
          >
            {otherLocale.toUpperCase()}
          </a>
        </Link>
      </div>
    </header>
  )
}

export default Header
