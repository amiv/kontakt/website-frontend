import React from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'

import classes from './burgerIcon.module.less'

/**
 * Burger Icon for use as a button for a mobile menu.
 *
 * **Example**
 * ```
 * const [active, setActive] = React.useState(false)
 * return (
 *     <BurgerIcon active={active} onClick={() => setActive(!active))} />
 * )
 *
 * @component
 */
const BurgerIcon = ({ active, onClick, className }) => {
  return (
    <div
      className={clsx(classes.root, active && classes.root_active, className)}
      onClick={onClick}
    >
      <div />
      <div />
      <div />
    </div>
  )
}

BurgerIcon.propTypes = {
  /** Show burger when false, show cross when true */
  active: PropTypes.bool,
  /** Callback when the user clicked on the icon */
  onClick: PropTypes.func,
  /** Additional class applied to the outermost box. */
  className: PropTypes.string,
}

BurgerIcon.defaultProps = {
  active: false,
  onClick: () => {},
}

export default BurgerIcon
