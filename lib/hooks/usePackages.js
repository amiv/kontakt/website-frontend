import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  loadPackages,
  selectPackages,
  selectState,
  selectError,
} from 'lib/store/slices/packagesSlice'

/**
 * usePackages hook
 *
 * Ensures that the data for the packages is loaded
 */
const usePackages = () => {
  const dispatch = useDispatch()
  const packages = useSelector(selectPackages)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadPackages())
    }
  }, [state])

  const reloadPackages = () => {
    dispatch(loadPackages())
  }

  return [packages, state, error, reloadPackages]
}

export default usePackages
