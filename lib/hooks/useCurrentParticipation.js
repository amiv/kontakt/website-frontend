import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  loadCurrentParticipation,
  selectCurrentParticipation,
  selectState,
  selectError,
} from 'lib/store/slices/currentParticipationSlice'

/**
 * useCurrentParticipation hook
 *
 * Ensures that the data for the current fair is loaded
 */
const useCurrentParticipation = () => {
  const dispatch = useDispatch()
  const participation = useSelector(selectCurrentParticipation)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadCurrentParticipation())
    }
  }, [state])

  const reloadCurrentParticipation = () => {
    dispatch(loadCurrentParticipation())
  }

  return [participation, state, error, reloadCurrentParticipation]
}

export default useCurrentParticipation
