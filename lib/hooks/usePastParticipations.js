import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  loadPastParticipations,
  selectPastParticipations,
  selectState,
  selectError,
} from 'lib/store/slices/pastParticipationsSlice'

/**
 * usePastParticipations hook
 *
 * Ensures that the data for the past fairs is loaded
 */
const usePastParticipations = () => {
  const dispatch = useDispatch()
  const participations = useSelector(selectPastParticipations)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check authentication state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadPastParticipations())
    }
  }, [state])

  const reloadPastParticipations = () => {
    dispatch(loadPastParticipations())
  }

  return [participations, state, error, reloadPastParticipations]
}

export default usePastParticipations
