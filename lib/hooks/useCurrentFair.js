import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  loadCurrentFair,
  selectCurrentFair,
  selectState,
  selectError,
} from 'lib/store/slices/currentFairSlice'

/**
 * useCurrentFair hook
 *
 * Ensures that the data for the current fair is loaded
 */
const useCurrentFair = () => {
  const dispatch = useDispatch()
  const fair = useSelector(selectCurrentFair)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadCurrentFair())
    }
  }, [state])

  const reloadCurrentFair = () => {
    dispatch(loadCurrentFair())
  }

  return [fair, state, error, reloadCurrentFair]
}

export default useCurrentFair
