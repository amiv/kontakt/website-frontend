import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  loadCompany,
  selectCompany,
  selectState,
  selectError,
} from 'lib/store/slices/companySlice'
import { selectAuthenticatedUser } from 'lib/store/slices/authSlice'

/**
 * useCompany hook
 *
 * Ensures that the data for the user's company is loaded
 */
const useCompany = () => {
  const dispatch = useDispatch()
  const user = useSelector(selectAuthenticatedUser)
  const company = useSelector(selectCompany)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (user && user.company_id && state === ResourceState.Unknown) {
      dispatch(loadCompany(user.company_id))
    }
  }, [user, state])

  const reloadCompany = () => {
    dispatch(loadCompany(user.company_id))
  }

  return [company, state, error, reloadCompany]
}

export default useCompany
