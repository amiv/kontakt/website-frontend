import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  selectCurrentFairCompanies,
  selectState,
  selectError,
  loadCurrentFairCompanies,
} from 'lib/store/slices/currentFairCompaniesSlice'

/**
 * useCurrentFairCompanies hook
 *
 * Ensures that the data for the current fair companies is loaded
 */
const useCurrentFairCompanies = () => {
  const dispatch = useDispatch()
  const fairCompanies = useSelector(selectCurrentFairCompanies)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadCurrentFairCompanies())
    }
  }, [state])

  const reloadCurrentFairCompanies = () => {
    dispatch(loadCurrentFairCompanies())
  }

  return [fairCompanies, state, error, reloadCurrentFairCompanies]
}

export default useCurrentFairCompanies
