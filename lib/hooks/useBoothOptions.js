import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  selectBoothOptions,
  selectState,
  selectError,
  loadBoothOptions,
} from 'lib/store/slices/boothOptionsSlice'

/**
 * useBoothOptions hook
 *
 * Ensures that the data for the booth options are loaded
 */
const useBoothOptions = () => {
  const dispatch = useDispatch()
  const boothOptions = useSelector(selectBoothOptions)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadBoothOptions())
    }
  }, [state])

  const reloadBoothOptions = () => {
    dispatch(loadBoothOptions())
  }

  return [boothOptions, state, error, reloadBoothOptions]
}

export default useBoothOptions
