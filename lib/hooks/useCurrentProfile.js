import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  loadCurrentProfile,
  selectCurrentProfile,
  selectState,
  selectError,
} from 'lib/store/slices/currentProfileSlice'

/**
 * useCurrentProfile hook
 *
 * Ensures that the data for the current profile is loaded
 */
const useCurrentProfile = () => {
  const dispatch = useDispatch()
  const profile = useSelector(selectCurrentProfile)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadCurrentProfile())
    }
  }, [state])

  const reloadCurrentProfile = () => {
    dispatch(loadCurrentProfile())
  }

  return [profile, state, error, reloadCurrentProfile]
}

export default useCurrentProfile
