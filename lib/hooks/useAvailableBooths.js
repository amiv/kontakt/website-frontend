import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  loadBooths,
  selectAvailableBooths,
  selectState,
  selectError,
} from 'lib/store/slices/boothsSlice'

/**
 * useAvailableBooths hook
 *
 * Ensures that the data for the available booths is loaded
 */
const useAvailableBooths = () => {
  const dispatch = useDispatch()
  const availableBooths = useSelector(selectAvailableBooths)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadBooths())
    }
  }, [state])

  const reloadAvailableBooths = () => {
    dispatch(loadBooths())
  }

  return [availableBooths, state, error, reloadAvailableBooths]
}

export default useAvailableBooths
