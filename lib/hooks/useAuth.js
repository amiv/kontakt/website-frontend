import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  checkLogin,
  selectAuthenticatedUser,
  selectIsAuthenticated,
  selectState,
  selectError,
} from 'lib/store/slices/authSlice'

import ResourceState from 'lib/store/resourceState'

/**
 * useAuth hook
 *
 * Ensures that the data for the current user is loaded
 */
const useAuth = () => {
  const dispatch = useDispatch()
  const isAuthenticated = useSelector(selectIsAuthenticated)
  const authenticatedUser = useSelector(selectAuthenticatedUser)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check authentication state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(checkLogin())
    }
  }, [state])

  return [isAuthenticated, authenticatedUser, state, error]
}

export default useAuth
