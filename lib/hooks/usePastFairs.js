import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ResourceState from 'lib/store/resourceState'
import {
  loadPastFairs,
  selectPastFairs,
  selectState,
  selectError,
} from 'lib/store/slices/pastFairsSlice'

/**
 * usePastFairs hook
 *
 * Ensures that the data for the past fairs is loaded
 */
const usePastFairs = () => {
  const dispatch = useDispatch()
  const fairs = useSelector(selectPastFairs)
  const state = useSelector(selectState)
  const error = useSelector(selectError)

  // Check state if not checked yet.
  useEffect(() => {
    if (state === ResourceState.Unknown) {
      dispatch(loadPastFairs())
    }
  }, [state])

  const reloadPastFairs = () => {
    dispatch(loadPastFairs())
  }

  return [fairs, state, error, reloadPastFairs]
}

export default usePastFairs
