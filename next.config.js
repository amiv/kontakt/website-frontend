// eslint-disable-next-line import/no-extraneous-dependencies
const path = require('path')
const withPlugins = require('next-compose-plugins')
const withLess = require('next-with-less')
const BundleAnalyzerPlugin = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})
const runtimeConfig = require('./config')

const isDevEnvironment = process.env.NODE_ENV !== 'production'

const plugins = [[withLess], [BundleAnalyzerPlugin]]

module.exports = withPlugins(plugins, {
  publicRuntimeConfig: runtimeConfig,
  reactStrictMode: true,
  images: {
    disableStaticImages: true,
  },
  i18n: {
    // More details about the `default` locale can be found at
    // https://github.com/vercel/next.js/discussions/18419#discussioncomment-1561577
    locales: ['default', 'en', 'de'],
    defaultLocale: 'default',
    localeDetection: false,
  },
  eslint: {
    dirs: ['pages', 'locales', 'lib'],
    ignoreDuringBuilds: true,
  },
  async rewrites() {
    // Proxy for api requests is only necessary during development.
    if (!isDevEnvironment) return []

    return [
      // Proxy /api requests to the local api instance.
      // This is required because of Cookie policies when using localhost.
      {
        source: '/api/:path*',
        destination: `https://api.kontakt-dev.amiv.ethz.ch/:path*`,
        // destination: `http://backend:5000/:path*`,
      },
    ]
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.(png|svg|eot|otf|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 8192,
          publicPath: '/_next/static/',
          outputPath: 'static/',
          name: '[name].[ext]',
        },
      },
    })

    // eslint-disable-next-line no-param-reassign
    config.resolve.alias['../../theme.config$'] = path.join(
      config.context,
      '/semantic/src/theme.config'
    )

    return config
  },
})
