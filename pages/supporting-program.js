import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

import Layout from 'lib/components/layout'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import { loadCurrentFair } from 'lib/store/slices/currentFairSlice'
import { wrapper } from 'lib/store/createStore'

// UNCOMMENT HERE
import SupportingProgramItem from 'lib/components/supportingProgramItem'
import classes from './supporting-program.module.less'

const CompanyInfosPage = () => {
  const currentFairItems = useCurrentFair()
  const intl = useIntl()
  const locale = intl.locale.split('-')[0]

  const [currentFair] = currentFairItems

  return (
    <Layout>
      <h1>
        <FormattedMessage id="supportingProgram.title" />
      </h1>
      {/* TODO: Comment the section below to remove the announcement text. */}
      {
        <p>
          <FormattedMessage
            id="supportingProgram.announcement"
            values={{ fairName: currentFair?.kontakt_title || 'Kontakt' }}
          />
          <br />
          <br />
          <FormattedMessage id="supportingProgram.fairguide_announcement" />
        </p>
      }
      {/*
        TODO: Uncomment the section below and update its content to show
              details for the supporting programm.
              Please note that additional commented lines at the beginning
              of this document must also be uncommented!
      */}
      {/*
      <div className={classes.items}>
        <SupportingProgramItem
          title="Job Search Strategies: Presentation"
          description={
            <>
              Are you looking for a job that matches with your interests,
              skills, and career goals? Do you feel discouraged about finding
              the right opportunity? Are you wondering how to enter the job
              market? Join for a presentation by the ETH Career Center that will
              address these concerns and many more. Common job search myths will
              be debunked and valuable guidance will be provided on how to make
              a smooth transition from university to industry. An apéro will be
              held following the presentation.
            </>
          }
          location="HG E3"
          date={new Date('2024-09-30T17:15:00+0200')}
          eventLink={`https://amiv.ethz.ch/de/events/66f18248d9bf48c8fb718081`}
        />
      </div>
      */}
    </Layout>
  )
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 3600s (1h).
export const getStaticProps = wrapper.getStaticProps(store => async () => {
  await store.dispatch(loadCurrentFair())

  return { props: {}, revalidate: 3600 }
})

export default CompanyInfosPage
