import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { FormattedMessage } from 'react-intl'

import Layout from 'lib/components/layout'
import Contact from 'lib/components/contact'
import EmailLink from 'lib/components/emailLink'

import classes from './_error.module.less'

const Error = ({ statusCode }) => {
  let content

  if (statusCode === 404) {
    content = (
      <>
        <h1>
          <FormattedMessage id="error404.title" />
        </h1>
        <p>
          <FormattedMessage id="error404.text" />
        </p>
        <p>
          <FormattedMessage
            id="error404.action"
            values={{
              a: function FrontpageLink(...chunks) {
                return (
                  <Link href="/">
                    <a>{chunks}</a>
                  </Link>
                )
              },
            }}
          />
        </p>
      </>
    )
  } else {
    content = (
      <>
        <h1>
          <FormattedMessage id="error.title" values={{ code: statusCode }} />
        </h1>
        <p className={classes.text}>
          <FormattedMessage
            id="error.text"
            values={{ contactEmail: <EmailLink /> }}
          />
        </p>
      </>
    )
  }
  return (
    <Layout seoProps={{ noindex: true }}>
      {content}
      <Contact />
    </Layout>
  )
}

Error.propTypes = {
  statusCode: PropTypes.number.isRequired,
}

Error.getInitialProps = ({ res, err }) => {
  let statusCode = 404
  if (res) {
    statusCode = res.statusCode
  } else if (err) {
    statusCode = err.statusCode
  }

  return { statusCode }
}

export default Error
