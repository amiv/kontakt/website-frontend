import React from 'react'

import { FormattedMessage } from 'react-intl'

import Layout from 'lib/components/layout'
import Contact from 'lib/components/contact'

import 'leaflet/dist/leaflet.css'

function Fairguide() {
  return (
    <Layout>
      <p>
        <FormattedMessage
          id="fairguide.missing_announcement"
          values={{ fairName: 'Kontakt' }}
        />
        <br />
        <br />
        <a
          href="/kontakt_24_fairguide.pdf"
          target="_blank"
          rel="noopener noreferrer"
        >
          AMIV Kontakt.24 Fairguide
        </a>
        <br />
        <a
          href="/kontakt_23_fairguide.pdf"
          target="_blank"
          rel="noopener noreferrer"
        >
          AMIV Kontakt.23 Fairguide
        </a>
        <br />
        <a
          href="/kontakt_22_fairguide.pdf"
          target="_blank"
          rel="noopener noreferrer"
        >
          AMIV Kontakt.22 Fairguide
        </a>
        <br />
        <a
          href="/kontakt_21_fairguide.pdf"
          target="_blank"
          rel="noopener noreferrer"
        >
          AMIV Kontakt.21 Fairguide
        </a>
      </p>
      <Contact />
    </Layout>
  )
}

export default Fairguide
