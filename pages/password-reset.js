import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FormattedMessage, useIntl } from 'react-intl'
import { Button, GridRow, Message, Input } from 'semantic-ui-react'
import { useDispatch, useSelector } from 'react-redux'

import { isBrowser } from 'lib/utils'
import { selectCSRFToken } from 'lib/store/slices/authSlice'
import ResourceState from 'lib/store/resourceState'
import { makeApiRequest } from 'lib/utils/api'
import Layout from 'lib/components/layout'
import Contact from 'lib/components/contact'
import Spinner from 'lib/components/spinner'
import EmailLink from 'lib/components/emailLink'
import classes from './password-forgotten.module.less'

const PasswordResetPage = () => {
  const [paramValidationState, setParamValidationState] = useState(
    ResourceState.Unknown
  )
  const [validatedParams, setValidatedParams] = useState(null)
  const [validationError, setValidationError] = useState(null)

  const [password, setPassword] = useState('')
  const [passwordRepeated, setPasswordRepeated] = useState('')
  const [isBusy, setIsBusy] = useState(false)
  const [error, setError] = useState(null)
  const [success, setSuccess] = useState(false)
  const csrfToken = useSelector(selectCSRFToken)
  const dispatch = useDispatch()
  const router = useRouter()
  const intl = useIntl()

  const queryParams = isBrowser ? window.location.search : undefined

  const isPasswordValid = password.length === 0 || password.length >= 8
  const isPasswordRepeatedValid = password === passwordRepeated
  const isFormValid = isPasswordValid && isPasswordRepeatedValid

  // Validate email and token parameters
  useEffect(() => {
    if (!isBrowser && paramValidationState === ResourceState.Loading) return

    const params = new URLSearchParams(queryParams)
    if (params.has('email') && params.has('token')) {
      const emailParam = params.get('email')
      const tokenParam = params.get('token')

      if (
        paramValidationState === ResourceState.Loaded &&
        emailParam === validatedParams.email &&
        tokenParam === validatedParams.token
      ) {
        // do nothing if the parameters are already validated.
        return
      }
      const action = {
        method: 'POST',
        resource: 'auth/check_token',
        data: { email: emailParam, token: tokenParam },
      }
      makeApiRequest(action, csrfToken, dispatch)
        .then(() => {
          setValidatedParams({ email: emailParam, token: tokenParam })
          setParamValidationState(ResourceState.Loaded)
        })
        .catch(err => {
          setParamValidationState(ResourceState.Error)
          setValidationError({
            code: err.response ? err.response.status : 503,
            ...(err.response ? err.response.data : {}),
          })
          setValidatedParams(null)
        })
    } else {
      setParamValidationState(ResourceState.Error)
      setValidationError({ code: 0, reason: 'invalid query parameters' })
      setValidatedParams(null)
    }
  }, [queryParams])

  const handleSubmitClick = e => {
    e.preventDefault()

    if (!isFormValid) return

    setIsBusy(true)

    const action = {
      method: 'POST',
      resource: 'auth/set_password',
      data: {
        email: validatedParams.email,
        token: validatedParams.token,
        new_password: password,
      },
    }
    makeApiRequest(action, csrfToken, dispatch)
      .then(() => {
        setSuccess(true)
        setError(null)
      })
      .catch(err => {
        setError({
          code: err.response ? err.response.status : 503,
          ...(err.response ? err.response.data : {}),
        })
      })
      .finally(() => {
        setIsBusy(false)
      })
  }

  let content

  if (
    paramValidationState === ResourceState.Unknown ||
    paramValidationState === ResourceState.Loading
  ) {
    content = (
      <div>
        <Spinner centered size="large" />
      </div>
    )
  } else if (validationError) {
    content = (
      <GridRow>
        <Message negative size="small" className={classes.message}>
          <FormattedMessage
            id={`passwordReset.error${validationError.code}`}
            values={{ contactEmail: <EmailLink /> }}
            defaultMessage={
              <FormattedMessage
                id="errorUnknown"
                values={{
                  code: validationError.code || 0,
                  contactEmail: <EmailLink />,
                }}
              />
            }
          />
        </Message>
      </GridRow>
    )
  } else if (success) {
    content = (
      <React.Fragment>
        <GridRow>
          <Message positive size="small" className={classes.message}>
            <FormattedMessage
              id="passwordReset.success"
              values={{ email: validatedParams.email }}
            />
          </Message>
        </GridRow>
        <Button className="button" onClick={() => router.push('/login')}>
          <FormattedMessage id="backToLogin" />
        </Button>
      </React.Fragment>
    )
  } else {
    content = (
      <React.Fragment>
        {error && (
          <GridRow>
            <Message negative size="small" className={classes.message}>
              <FormattedMessage
                id={`passwordReset.error${error.code}`}
                defaultMessage={
                  <FormattedMessage
                    id="errorUnknown"
                    values={{
                      code: error.code || 0,
                      contactEmail: <EmailLink />,
                    }}
                  />
                }
              />
            </Message>
          </GridRow>
        )}
        <div>
          <p>
            <FormattedMessage
              id="passwordRequirements"
              values={{ minLength: 8 }}
            />
          </p>
          <form>
            <div className={classes.formField}>
              <Input
                type="password"
                value={password}
                error={!isPasswordValid}
                onChange={e => {
                  setPassword(e.target.value)
                }}
                placeholder={intl.formatMessage({ id: 'password' })}
              />
            </div>
            <div className={classes.formField}>
              <Input
                type="password"
                value={passwordRepeated}
                error={!isPasswordRepeatedValid}
                onChange={e => {
                  setPasswordRepeated(e.target.value)
                }}
                placeholder={intl.formatMessage({ id: 'passwordRepeated' })}
              />
            </div>
            <Button
              primary
              loading={isBusy}
              disabled={!isFormValid}
              onClick={handleSubmitClick}
            >
              <FormattedMessage id="passwordReset.setNewPassword" />
            </Button>
            &nbsp;
            <Link href="/login">
              <a className="button">
                <FormattedMessage id="backToLogin" />
              </a>
            </Link>
          </form>
        </div>
      </React.Fragment>
    )
  }

  return (
    <Layout>
      <h1>
        <FormattedMessage id="passwordReset.title" />
      </h1>
      {content}
      <Contact />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default PasswordResetPage
