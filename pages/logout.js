import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import Link from 'next/link'
import { FormattedMessage } from 'react-intl'
import { Message } from 'semantic-ui-react'

import useAuth from 'lib/hooks/useAuth'
import { logout } from 'lib/store/slices/authSlice'
import ResourceState from 'lib/store/resourceState'
import Layout from 'lib/components/layout'
import Contact from 'lib/components/contact'
import EmailLink from 'lib/components/emailLink'

const LogoutPage = () => {
  const [isAuthenticated, , state, error] = useAuth()
  const dispatch = useDispatch()

  useEffect(() => {
    if (
      isAuthenticated &&
      state !== ResourceState.Loading &&
      state !== ResourceState.Error
    ) {
      dispatch(logout())
    }
  }, [dispatch, isAuthenticated, state])

  const frontpageLink = (
    <Link href="/">
      <a>
        <FormattedMessage id="logout.actionTarget" />
      </a>
    </Link>
  )

  return (
    <Layout>
      <h1>
        <FormattedMessage id="logout.title" />
      </h1>
      {state === ResourceState.Error && (
        <Message negative size="small">
          <React.Fragment>
            <FormattedMessage
              id={`logout.error${error.code}`}
              defaultMessage={
                <FormattedMessage
                  id="errorUnknown"
                  values={{ code: error.code, contactEmail: <EmailLink /> }}
                />
              }
            />
          </React.Fragment>
        </Message>
      )}
      <p>
        {isAuthenticated ? (
          <FormattedMessage id="logout.loadingText" />
        ) : (
          <FormattedMessage
            id="logout.successText"
            values={{ target: frontpageLink }}
          />
        )}
      </p>
      <Contact />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default LogoutPage
