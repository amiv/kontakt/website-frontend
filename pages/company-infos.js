import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'

import Layout from 'lib/components/layout'
import Contact from 'lib/components/contact'
import CompanyInformation from 'lib/components/company-information/company-information'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import { requireDataLoaded } from 'lib/utils/api'
import usePackages from 'lib/hooks/usePackages'
import useBoothOptions from 'lib/hooks/useBoothOptions'
import { wrapper } from 'lib/store/createStore'
import { loadCurrentFair } from 'lib/store/slices/currentFairSlice'
import { loadBoothOptions } from 'lib/store/slices/boothOptionsSlice'
import { loadPackages } from 'lib/store/slices/packagesSlice'

function CompanyInfosPage() {
  const currentFairItems = useCurrentFair()
  const boothOptionsItems = useBoothOptions()
  const packagesItems = usePackages()
  const intl = useIntl()
  const [fair] = currentFairItems
  const [boothOptions] = boothOptionsItems
  const [packages] = packagesItems

  let content = requireDataLoaded([
    currentFairItems,
    boothOptionsItems,
    packagesItems,
  ])

  if (!content) {
    content = (
      <>
        <CompanyInformation
          fair={fair}
          boothOptions={boothOptions}
          packages={packages}
          // TODO: set to `true` when the timeline dates are correct. Set this
          //       value to `false` if you want to hide all timeline related
          //       elements.
          showTimeline={true}
          showTimelineFair={true}
        />
      </>
    )
  }

  const dateFormatOptions = {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  }

  return (
    <Layout>
      <h1>
        <FormattedMessage id="companyInfos.title" />
      </h1>
      {fair && (
        <p>
          <FormattedMessage
            id="companyInfos.text"
            values={{
              fairName: fair.kontakt_title,
              date1: intl.formatDate(new Date(fair.date_start), {
                ...dateFormatOptions,
                year: undefined,
              }),
              date2: intl.formatDate(new Date(fair.date_end), {
                ...dateFormatOptions,
              }),
            }}
          />
        </p>
      )}
      {content}
      <Contact />
    </Layout>
  )
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 3600s (1h).
export const getStaticProps = wrapper.getStaticProps(store => async () => {
  await store.dispatch(loadCurrentFair())
  await store.dispatch(loadBoothOptions())
  await store.dispatch(loadPackages())

  return { props: {}, revalidate: 3600 }
})

export default CompanyInfosPage
