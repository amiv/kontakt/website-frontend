import React from 'react'

import dynamic from 'next/dynamic'

import { FormattedMessage } from 'react-intl'
import { Image, Label } from 'semantic-ui-react'

import Layout from 'lib/components/layout'
import Contact from 'lib/components/contact'
import VenueAddress from 'lib/components/venueAddress'

import 'leaflet/dist/leaflet.css'

function Venue() {
  const VenuePageNoSSR = dynamic(() => import('lib/components/map'), {
    ssr: false,
  })
  return (
    <Layout>
      <h1>
        <FormattedMessage id="venue.title" />
      </h1>
      <p>
        <FormattedMessage id="venue.content" />
      </p>
      <div>
        <main>
          <div id="map">
            <VenuePageNoSSR />
          </div>
          <br />
        </main>
        <VenueAddress />
      </div>
      <h2>
        <FormattedMessage id="venue.subtitle" />
      </h2>
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/CLA_aussen.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption1" />
        </Label>
      </div>
      &nbsp;&nbsp;&nbsp;
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/LEE_aussen.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption2" />
        </Label>
      </div>
      &nbsp;&nbsp;&nbsp;
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/Kontakt_aussen.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption3" />
        </Label>
      </div>
      <br />
      <br />
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/Map_Zentrum_cropped.jpg" size="huge" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.map" />
        </Label>
      </div>
      <h2>
        <FormattedMessage id="venue.subtitle2" />
      </h2>
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/CLA_D_edited.jpg" size="large" />
        <Label color="black" attached="bottom">
          CLA
        </Label>
      </div>
      &nbsp;&nbsp;&nbsp;
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/LEE_E_edited.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption4" />
        </Label>
      </div>
      &nbsp;&nbsp;&nbsp;
      <div style={{ position: 'relative', display: 'inline-block' }}>
        <Image src="/images/LEE_F_edited.jpg" size="large" />
        <Label color="black" attached="bottom">
          <FormattedMessage id="venue.caption5" />
        </Label>
      </div>
      {/*
        TODO: Uncomment the section below to show the booth layout
              and the booth overview table.
              Make sure to adjust the booth overview table and the
              booth layout too! The import at the beginning of the
              file is also required.
        */}
      {/* <h2>
        <FormattedMessage id="venue.subtitle2" />
      </h2>

      <BoothLayout /> */}
      <Contact />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default Venue
