import React from 'react'
import NextDocument, { Html, Head, Main, NextScript } from 'next/document'
// eslint-disable-next-line import/no-extraneous-dependencies
import { randomBytes } from 'crypto'

import { getPublicConfig } from 'lib/utils/config'

const prod = process.env.NODE_ENV === 'production'

function getCsp(nonce, apiDomain, contractorDomain) {
  let csp = ``
  csp += `base-uri 'self';`
  csp += `form-action 'self' ${apiDomain};`
  csp += `default-src 'self';`
  csp += `script-src 'self' ${prod ? '' : "'unsafe-eval'"};` // NextJS requires 'unsafe-eval' in dev (faster source maps)
  csp += `style-src 'self' 'unsafe-inline';`
  csp += `img-src 'self' ${apiDomain} https://*.tile.openstreetmap.org;`
  csp += `font-src 'self' data:;`
  csp += `connect-src 'self' ${apiDomain} ${contractorDomain} blob:;`
  csp += `upgrade-insecure-requests;`
  csp += `object-src blob:;`
  csp += `frame-src 'none';`
  csp += `child-src 'none';`
  csp += `worker-src 'none';`
  return csp
}

const referrer = 'strict-origin'

export default class Document extends NextDocument {
  // eslint-disable-next-line class-methods-use-this
  render() {
    const { apiUrl, apiIsSameOrigin, contractorUrl } = getPublicConfig()
    // Regenerated every render:
    const nonce = randomBytes(8).toString('base64')
    return (
      <Html>
        <Head nonce={nonce}>
          <meta
            httpEquiv="Content-Security-Policy"
            content={getCsp(
              nonce,
              apiIsSameOrigin ? '' : apiUrl,
              contractorUrl
            )}
          />
          <meta name="referrer" content={referrer} />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/favicon-16x16.png"
          />
          <link rel="manifest" href="/site.webmanifest" />
        </Head>
        <body>
          <Main />
          <div id="modal" />
          <NextScript nonce={nonce} />
        </body>
      </Html>
    )
  }
}
