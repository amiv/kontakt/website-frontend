import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Segment } from 'semantic-ui-react'

import CompanyPortalLayout from 'lib/components/company-portal/layout'
import useCurrentParticipation from 'lib/hooks/useCurrentParticipation'
import useCurrentProfile from 'lib/hooks/useCurrentProfile'
import CreateProfileComponent from 'lib/components/company-portal/profile/create'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import ProfileForm from 'lib/components/company-portal/profile/profileForm'
import useCompany from 'lib/hooks/useCompany'
import { requireDataLoaded } from 'lib/utils/api'

const CompanyPortalProfilePage = () => {
  const [fair] = useCurrentFair()
  const [company] = useCompany()
  const [participation] = useCurrentParticipation()
  const currentProfileItems = useCurrentProfile()
  const [profile] = currentProfileItems

  let content = requireDataLoaded([currentProfileItems])

  if (!content) {
    if (!participation) {
      content = (
        <p>
          <FormattedMessage id="companyPortal.profile.notRegistered" />
        </p>
      )
    } else if (!profile) {
      content = <CreateProfileComponent company={company} />
    } else {
      return (
        <CompanyPortalLayout activeItem="profile">
          <ProfileForm
            fair={fair}
            company={company}
            participation={participation}
            profile={profile}
          />
        </CompanyPortalLayout>
      )
    }
  }

  return (
    <CompanyPortalLayout activeItem="profile">
      <Segment>
        <h2>
          <FormattedMessage
            id="companyPortal.profile.title"
            values={{ fairName: fair ? fair.kontakt_title : 'Kontakt' }}
          />
        </h2>
        <p>
          <FormattedMessage
            id="companyPortal.profile.text"
            values={{
              companyName: company ? (
                company.company_name
              ) : (
                <FormattedMessage id="companyPortal.yourCompany" />
              ),
            }}
          />
        </p>
        {content}
      </Segment>
    </CompanyPortalLayout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default CompanyPortalProfilePage
