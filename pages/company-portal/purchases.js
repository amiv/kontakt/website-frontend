import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Divider, Segment } from 'semantic-ui-react'

import useCurrentParticipation from 'lib/hooks/useCurrentParticipation'
import usePastParticipations from 'lib/hooks/usePastParticipations'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import useCompany from 'lib/hooks/useCompany'
import CompanyPortalLayout from 'lib/components/company-portal/layout'
import PurchaseCard from 'lib/components/company-portal/purchaseCard'
import { requireDataLoaded } from 'lib/utils/api'

const CompanyPortalPurchasesPage = () => {
  const [fair] = useCurrentFair()
  const [company] = useCompany()
  const currentParticipationItems = useCurrentParticipation()
  const pastParticipationsItems = usePastParticipations()
  const intl = useIntl()

  const [currentParticipation] = currentParticipationItems
  const [pastParticipations] = pastParticipationsItems

  let content = requireDataLoaded([
    currentParticipationItems,
    pastParticipationsItems,
  ])

  if (!content) {
    content = (
      <React.Fragment>
        <p>
          <FormattedMessage id="companyPortal.purchases.text" />
        </p>
        {currentParticipation && (
          <PurchaseCard participation={currentParticipation} fair={fair} />
        )}
        {pastParticipations.length > 0 && (
          <React.Fragment>
            <Divider horizontal>
              <FormattedMessage id="companyPortal.purchases.past" />
            </Divider>
            {pastParticipations.map(participation => (
              <PurchaseCard
                key={participation.id}
                participation={participation}
                fair={participation.fair}
              />
            ))}
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }

  return (
    <CompanyPortalLayout activeItem="purchases">
      <Segment>
        <h2>
          <FormattedMessage
            id="companyPortal.purchases.title"
            values={{
              companyName: company
                ? company.company_name
                : intl.formatMessage({ id: 'companyPortal.yourCompany' }),
            }}
          />
        </h2>
        {content}
      </Segment>
    </CompanyPortalLayout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default CompanyPortalPurchasesPage
