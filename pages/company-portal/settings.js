import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage } from 'react-intl'
import { Button, Message, Segment } from 'semantic-ui-react'

import { selectCSRFToken } from 'lib/store/slices/authSlice'
import useCompany from 'lib/hooks/useCompany'
import { makeApiRequest } from 'lib/utils/api'
import CompanyPortalLayout from 'lib/components/company-portal/layout'
import SettingsForm from 'lib/components/company-portal/settingsForm'
import EmailLink from 'lib/components/emailLink'
import ActionBar from 'lib/components/company-portal/actionBar'

const CompanyPortalSettingsPage = () => {
  const [company] = useCompany()
  const [formData, setFormData] = useState({})
  const [isFormValid, setIsFormValid] = useState(true)
  const [isBusy, setIsBusy] = useState(false)
  const [error, setError] = useState(null)
  const [success, setSuccess] = useState(false)
  const csrfToken = useSelector(selectCSRFToken)
  const dispatch = useDispatch()

  const handleOnChange = ({ value, isValid }) => {
    setFormData(value)
    setIsFormValid(isValid)
  }

  const handleSubmitClick = e => {
    e.preventDefault()

    if (!isFormValid) return

    setIsBusy(true)

    const action = {
      method: 'PATCH',
      resource: `companies/${company.id}`,
      data: formData,
    }
    makeApiRequest(action, csrfToken, dispatch)
      .then(() => {
        setSuccess(true)
        setError(null)
      })
      .catch(err => {
        setError({
          code: err.response ? err.response.status : 503,
          ...(err.response ? err.response.data : {}),
        })
      })
      .finally(() => {
        setIsBusy(false)
      })
  }

  return (
    <CompanyPortalLayout activeItem="settings">
      <Segment>
        <h2>
          <FormattedMessage id="companyPortal.settings.title" />
        </h2>
        <p>
          <FormattedMessage
            id="companyPortal.settings.text"
            values={{ companyName: company ? company.company_name : '' }}
          />
        </p>
        <SettingsForm company={company} onChange={handleOnChange} />
      </Segment>
      <ActionBar>
        {success && (
          <Message positive size="small">
            <FormattedMessage id="companyPortal.settings.success" />
          </Message>
        )}
        {error && (
          <Message negative size="small">
            <FormattedMessage
              id={`companyPortal.settings.error${error.code}`}
              defaultMessage={
                <FormattedMessage
                  id="errorUnknown"
                  values={{
                    code: error.code || 0,
                    contactEmail: <EmailLink />,
                  }}
                />
              }
            />
          </Message>
        )}
        <Button
          color="green"
          floated="right"
          loading={isBusy}
          disabled={!isFormValid}
          onClick={handleSubmitClick}
        >
          <FormattedMessage id="companyPortal.update" />
        </Button>
      </ActionBar>
    </CompanyPortalLayout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default CompanyPortalSettingsPage
