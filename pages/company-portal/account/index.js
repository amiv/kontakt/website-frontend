import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage, useIntl } from 'react-intl'
import { validate as validateEmail } from 'email-validator'
import { Button, Form, Message } from 'semantic-ui-react'

import {
  selectCSRFToken,
  selectAuthenticatedUser,
} from 'lib/store/slices/authSlice'
import { makeApiRequest } from 'lib/utils/api'
import CompanyPortalLayout from 'lib/components/company-portal/layout'
import AccountLayout from 'lib/components/company-portal/accountLayout'
import EmailLink from 'lib/components/emailLink'
import Textbox from 'lib/components/company-portal/textbox'

const AccountPage = () => {
  const authenticatedUser = useSelector(selectAuthenticatedUser)
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [isBusy, setIsBusy] = useState(false)
  const [error, setError] = useState(null)
  const [success, setSuccess] = useState(false)
  const csrfToken = useSelector(selectCSRFToken)
  const dispatch = useDispatch()
  const intl = useIntl()

  const isEmailValid = email.length === 0 || validateEmail(email)
  const isFormValid = isEmailValid

  useEffect(() => {
    if (authenticatedUser) {
      setFirstname(authenticatedUser.first_name)
      setLastname(authenticatedUser.last_name)
      setEmail(authenticatedUser.email_address)
      setPhone(authenticatedUser.phone || '')
    }
  }, [authenticatedUser])

  const handleSubmitClick = e => {
    e.preventDefault()

    if (!isFormValid) return

    setIsBusy(true)

    const action = {
      method: 'PATCH',
      resource: `users/${authenticatedUser.id}`,
      data: {
        first_name: firstname,
        last_name: lastname,
        email_address: email,
        phone,
      },
    }
    makeApiRequest(action, csrfToken, dispatch)
      .then(() => {
        setSuccess(true)
        setError(null)
      })
      .catch(err => {
        setError({
          code: err.response ? err.response.status : 503,
          ...(err.response ? err.response.data : {}),
        })
      })
      .finally(() => {
        setIsBusy(false)
      })
  }

  return (
    <CompanyPortalLayout activeItem="account">
      <AccountLayout
        activeItem="contact"
        actionComponent={
          <React.Fragment>
            {success && (
              <Message positive size="small">
                <FormattedMessage id="companyPortal.account.contactInformation.success" />
              </Message>
            )}
            {error && (
              <Message negative size="small">
                <FormattedMessage
                  id={`companyPortal.account.contactInformation.error${error.code}`}
                  defaultMessage={
                    <FormattedMessage
                      id="errorUnknown"
                      values={{
                        code: error.code || 0,
                        contactEmail: <EmailLink />,
                      }}
                    />
                  }
                />
              </Message>
            )}
            <Button
              color="green"
              floated="right"
              loading={isBusy}
              disabled={!isFormValid}
              onClick={handleSubmitClick}
            >
              <FormattedMessage id="companyPortal.update" />
            </Button>
          </React.Fragment>
        }
      >
        <Form>
          <Textbox marginBottom>
            <FormattedMessage id="companyPortal.account.contactInformation.text" />
          </Textbox>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label={intl.formatMessage({ id: 'firstname' })}
              placeholder={intl.formatMessage(
                { id: 'companyPortal.account.contactInformation.placeholder' },
                { field: intl.formatMessage({ id: 'firstname' }) }
              )}
              value={firstname}
              onChange={e => setFirstname(e.target.value)}
            />
            <Form.Input
              fluid
              label={intl.formatMessage({ id: 'lastname' })}
              placeholder={intl.formatMessage(
                { id: 'companyPortal.account.contactInformation.placeholder' },
                { field: intl.formatMessage({ id: 'lastname' }) }
              )}
              value={lastname}
              onChange={e => setLastname(e.target.value)}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label={intl.formatMessage({ id: 'email' })}
              placeholder={intl.formatMessage(
                { id: 'companyPortal.account.contactInformation.placeholder' },
                { field: intl.formatMessage({ id: 'email' }) }
              )}
              type="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              error={!isEmailValid}
            />
            <Form.Input
              fluid
              label={intl.formatMessage({ id: 'phone' })}
              placeholder="+41 xx xxx xx xx"
              value={phone}
              onChange={e => setPhone(e.target.value)}
            />
          </Form.Group>
        </Form>
      </AccountLayout>
    </CompanyPortalLayout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default AccountPage
