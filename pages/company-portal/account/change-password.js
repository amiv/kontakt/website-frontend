import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormattedMessage, useIntl } from 'react-intl'
import { Button, Form, Message } from 'semantic-ui-react'

import { selectCSRFToken } from 'lib/store/slices/authSlice'
import { makeApiRequest } from 'lib/utils/api'
import CompanyPortalLayout from 'lib/components/company-portal/layout'
import AccountLayout from 'lib/components/company-portal/accountLayout'
import EmailLink from 'lib/components/emailLink'
import Textbox from 'lib/components/company-portal/textbox'

const ChangePasswordPage = () => {
  const [oldPassword, setOldPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [newPasswordRepeated, setNewPasswordRepeated] = useState('')
  const [isBusy, setIsBusy] = useState(false)
  const [error, setError] = useState(null)
  const [success, setSuccess] = useState(false)
  const csrfToken = useSelector(selectCSRFToken)
  const dispatch = useDispatch()
  const intl = useIntl()

  const isOldPasswordValid = true
  const isNewPasswordValid = newPassword.length === 0 || newPassword.length >= 8
  const isNewPasswordRepeatedValid = newPassword === newPasswordRepeated
  const isFormValid =
    isOldPasswordValid && isNewPasswordValid && isNewPasswordRepeatedValid

  const handleSubmitClick = e => {
    e.preventDefault()

    if (!isFormValid) return

    setIsBusy(true)

    const action = {
      method: 'POST',
      resource: 'auth/change_password',
      data: {
        old_password: oldPassword,
        new_password: newPassword,
      },
    }
    makeApiRequest(action, csrfToken, dispatch)
      .then(() => {
        setSuccess(true)
        setError(null)
      })
      .catch(err => {
        setError({
          code: err.response ? err.response.status : 503,
          ...(err.response ? err.response.data : {}),
        })
      })
      .finally(() => {
        setIsBusy(false)
      })
  }

  return (
    <CompanyPortalLayout activeItem="account">
      <AccountLayout
        activeItem="password"
        actionComponent={
          <React.Fragment>
            {success && (
              <Message positive size="small">
                <FormattedMessage id="companyPortal.account.changePassword.success" />
              </Message>
            )}
            {error && (
              <Message negative size="small">
                <FormattedMessage
                  id={`companyPortal.account.changePassword.error${error.code}`}
                  defaultMessage={
                    <FormattedMessage
                      id="errorUnknown"
                      values={{
                        code: error.code || 0,
                        contactEmail: <EmailLink />,
                      }}
                    />
                  }
                />
              </Message>
            )}
            <Button
              color="green"
              floated="right"
              loading={isBusy}
              disabled={!isFormValid}
              onClick={handleSubmitClick}
            >
              <FormattedMessage id="companyPortal.update" />
            </Button>
          </React.Fragment>
        }
      >
        <Form>
          <Textbox marginBottom>
            <FormattedMessage id="companyPortal.account.changePassword.text" />{' '}
            <FormattedMessage
              id="passwordRequirements"
              values={{ minLength: 8 }}
            />
          </Textbox>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label={intl.formatMessage({
                id: 'companyPortal.account.oldPassword',
              })}
              placeholder={intl.formatMessage({
                id: 'companyPortal.account.oldPasswordPlaceholder',
              })}
              type="password"
              error={!isOldPasswordValid}
              onChange={e => setOldPassword(e.target.value)}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              fluid
              label={intl.formatMessage({
                id: 'companyPortal.account.newPassword',
              })}
              placeholder={intl.formatMessage({
                id: 'companyPortal.account.newPasswordPlaceholder',
              })}
              title={intl.formatMessage(
                {
                  id: 'passwordRequirements',
                },
                { minLength: 8 }
              )}
              type="password"
              error={!isNewPasswordValid}
              onChange={e => setNewPassword(e.target.value)}
            />
            <Form.Input
              fluid
              label={intl.formatMessage({
                id: 'companyPortal.account.repeatNewPassword',
              })}
              placeholder={intl.formatMessage({
                id: 'companyPortal.account.repeatNewPasswordPlaceholder',
              })}
              title={intl.formatMessage({
                id: 'companyPortal.account.repeatNewPasswordHint',
              })}
              type="password"
              error={!isNewPasswordRepeatedValid}
              onChange={e => setNewPasswordRepeated(e.target.value)}
            />
          </Form.Group>
        </Form>
      </AccountLayout>
    </CompanyPortalLayout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default ChangePasswordPage
