import React from 'react'
import Link from 'next/link'
import { FormattedMessage } from 'react-intl'
import { Message, Segment } from 'semantic-ui-react'

import useCurrentParticipation from 'lib/hooks/useCurrentParticipation'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import CompanyPortalLayout from 'lib/components/company-portal/layout'
import RegistrationHint from 'lib/components/company-portal/registrationHint'
import CompanyProfileHint from 'lib/components/company-portal/companyProfileHint'

const CompanyPortalPage = () => {
  const [fair] = useCurrentFair()
  const [participation] = useCurrentParticipation()

  return (
    <CompanyPortalLayout activeItem="home">
      <Segment>
        <h2>
          <FormattedMessage
            id="companyPortal.title"
            values={{ fairName: fair ? fair.kontakt_title : 'Kontakt' }}
          />
        </h2>
        <p>
          <FormattedMessage
            id="companyPortal.text"
            values={{
              a: function CompanyInfosLink(...chunks) {
                return (
                  <Link href="/company-infos">
                    <a>{chunks}</a>
                  </Link>
                )
              },
            }}
          />
        </p>
        {/*
          TODO: Uncomment the following line when the timeline dates are
                updated in the database.
          */}
        {<RegistrationHint fair={fair} participation={participation} />}
        <CompanyProfileHint fair={fair} participation={participation} />

        <Message warning>
          <Message.Header>
            <FormattedMessage id="companyPortal.updateHint.title" />
          </Message.Header>
          <p>
            <FormattedMessage
              id="companyPortal.updateHint.text"
              values={{
                accountLink: function AccountLink(...chunks) {
                  return (
                    <Link href="/company-portal/account">
                      <a>{chunks}</a>
                    </Link>
                  )
                },
                companySettingsLink: function CompanySettingsLink(...chunks) {
                  return (
                    <Link href="/company-portal/settings">
                      <a>{chunks}</a>
                    </Link>
                  )
                },
              }}
            />
          </p>
        </Message>
      </Segment>
    </CompanyPortalLayout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default CompanyPortalPage
