import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Segment } from 'semantic-ui-react'

import useCompany from 'lib/hooks/useCompany'
import usePackages from 'lib/hooks/usePackages'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import useAvailableBooths from 'lib/hooks/useAvailableBooths'
import useCurrentParticipation from 'lib/hooks/useCurrentParticipation'
import CompanyPortalLayout from 'lib/components/company-portal/layout'
import RegistrationForm from 'lib/components/company-portal/registration/registrationForm'
import RegistrationCheckoutExistingForm from 'lib/components/company-portal/registration/registrationCheckoutExistingForm'

import EmailLink from 'lib/components/emailLink'
import Contact from 'lib/components/contact'
import { requireDataLoaded } from 'lib/utils/api'

const CompanyPortalRegistrationPage = () => {
  const [fair] = useCurrentFair()
  const [company] = useCompany()
  const [participation] = useCurrentParticipation()
  const packagesItems = usePackages()
  const boothsItems = useAvailableBooths()

  const [packages] = packagesItems
  const [booths] = boothsItems
  const intl = useIntl()

  let content = requireDataLoaded([packagesItems, boothsItems])

  if (!content) {
    if (
      participation &&
      participation.accepted_participation_terms &&
      participation.accepted_permitted_items_terms
    ) {
      // Show readonly overview
      content = (
        <Segment>
          <RegistrationCheckoutExistingForm
            fair={fair}
            company={company}
            title={intl.formatMessage(
              { id: 'companyPortal.registration.title' },
              {
                companyName: company
                  ? company.company_name
                  : intl.formatMessage({ id: 'companyPortal.yourCompany' }),
              }
            )}
            participation={participation}
            readOnly
          />
        </Segment>
      )
    } else {
      const now = Date.now()
      const registrationStart = new Date(fair.registration_start)
      const registrationEnd = new Date(fair.registration_end)

      const dateFormatOptions = {
        weekday: 'long',
        day: '2-digit',
        month: 'long',
        year: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
      }

      if (registrationStart >= now) {
        content = (
          <Segment>
            <h2>
              <FormattedMessage
                id="companyPortal.registration.notStarted.title"
                values={{ fairName: fair ? fair.kontakt_title : 'Kontakt' }}
              />
            </h2>
            <p>
              <FormattedMessage
                id="companyPortal.registration.notStarted.text"
                values={{
                  date: (
                    <b>
                      {intl.formatDate(registrationStart, dateFormatOptions)}
                    </b>
                  ),
                }}
              />
            </p>
            <Contact />
          </Segment>
        )
      } else if (registrationEnd < now && !participation) {
        content = (
          <Segment>
            <h2>
              <FormattedMessage
                id="companyPortal.registration.ended.title"
                values={{ fairName: fair ? fair.kontakt_title : 'Kontakt' }}
              />
            </h2>
            <p>
              <FormattedMessage
                id="companyPortal.registration.ended.text"
                values={{
                  contactEmail: <EmailLink />,
                }}
              />
            </p>
            <Contact />
          </Segment>
        )
      } else {
        content = (
          <RegistrationForm
            fair={fair}
            company={company}
            participation={participation}
            availableTimeslots={booths}
            availablePackages={packages}
          />
        )
      }
    }
  }

  return (
    <CompanyPortalLayout activeItem="registration">
      {content}
    </CompanyPortalLayout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default CompanyPortalRegistrationPage
