import React from 'react'
import { IntlProvider } from 'react-intl'
import { useRouter } from 'next/router'

import '../semantic/dist/semantic.min.css'
// import all locales through barrel file
import * as locales from 'locales'
import { wrapper } from 'lib/store/createStore'

// eslint-disable-next-line react/prop-types
const MyApp = ({ Component, pageProps }) => {
  const router = useRouter()
  let { locale } = router
  const messages = { ...locales.en, ...locales[locale] }

  if (locale === 'en') {
    locale = 'en-US'
  }

  return (
    <IntlProvider locale={locale} defaultLocale="en" messages={messages}>
      <Component {...pageProps} />
    </IntlProvider>
  )
}

export default wrapper.withRedux(MyApp)
