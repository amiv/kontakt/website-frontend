import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FormattedMessage, useIntl } from 'react-intl'
import { Icon } from 'semantic-ui-react'
import Swiper from 'react-id-swiper'
import 'swiper/swiper.min.css'
import SwiperCore, { Autoplay } from 'swiper'

import Menu from 'lib/components/menu'
import MenuItem from 'lib/components/menuItem'
import SEO from 'lib/components/seo'
import { getPublicConfig } from 'lib/utils/config'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import { loadCurrentFair } from 'lib/store/slices/currentFairSlice'
import { wrapper } from 'lib/store/createStore'
import { requireDataLoaded } from 'lib/utils/api'
import classes from './index.module.less'

SwiperCore.use([Autoplay])

const IndexPage = () => {
  const { apiUrl, premiumPartners, businessPartners } = getPublicConfig()
  const currentFairItems = useCurrentFair()
  const router = useRouter()
  const intl = useIntl()

  const [currentFair] = currentFairItems

  const content = requireDataLoaded([currentFairItems])

  if (content) {
    return content
  }

  const otherLocale = intl.locale === 'de' ? 'en' : 'de'

  const premiumPartnerSlider = Object.keys(premiumPartners).length > 0 && (
    <div className={classes.sliderContainer}>
      <Swiper
        containerClass="swiper"
        autoplay={{
          delay: 2000,
          disableOnInteraction: false,
        }}
        centeredSlides={true}
        spaceBetween={10}
        onSlideChange={() => console.log('slide change')}
      >
        {Object.entries(premiumPartners).map(([profileId, item]) => (
          <div key={profileId}>
            <img
              alt={item.name}
              src={
                item.logoUrl || `${apiUrl}/company_profiles/${profileId}/logo`
              }
            />
          </div>
        ))}
      </Swiper>
    </div>
  )

  const dateFormatOptionsOne = {
    day: 'numeric',
  }
  const dateFormatOptionsTwo = {
    ...dateFormatOptionsOne,
    month: 'short',
  }

  /* const fairDateString = `${intl.formatDate(
    currentFair.date_one,
    dateFormatOptionsOne
  )}${intl.locale === 'de' ? '.' : ''} - ${intl.formatDate(
    currentFair.date_two,
    dateFormatOptionsTwo
  )}, 11:00 - 17:00 ${intl.formatMessage({ id: 'frontpage.time' })}` // -> both days in one month */
  const fairDateString = `${intl.formatDate(
    currentFair.date_start,
    dateFormatOptionsTwo
  )} - ${intl.formatDate(
    currentFair.date_end,
    dateFormatOptionsTwo
  )}, 11:00 - 17:00 ${intl.formatMessage({ id: 'frontpage.time' })}` // fair days in different months

  const titleParts = currentFair.kontakt_title.split('.', 2)
  const numberParts = titleParts.length > 1 ? titleParts[1].split(' ', 2) : ['']

  const fairTitle = (
    <>
      {titleParts[0]}
      {titleParts.length > 1 && (
        <>
          {'.'}
          <span className={classes.fairNameNumber}>{numberParts[0]}</span>
          {numberParts.length > 1 && ` ${numberParts[1]}`}
        </>
      )}
    </>
  )

  return (
    <>
      <SEO />
      <div className={classes.background}>
        <div className={classes.gradient} />
        <img src="images/background.jpg" alt="" />
      </div>
      <div className={classes.container}>
        <header className={classes.header}>
          <div className={classes.logo}>
            <img src="/images/logo_white.svg" alt="AMIV Kontakt Logo" />
          </div>
          <div className={classes.languageSwitch}>
            <Link href={router.pathname} locale={otherLocale}>
              <a language={otherLocale}>
                <Icon name="long arrow alternate right" />
                <FormattedMessage id="switchLanguage" />
              </a>
            </Link>
          </div>
        </header>
        {premiumPartnerSlider}
        <div className={classes.space} />
        <div className={classes.title_wrapper}>
          <h1 className={classes.title}>{fairTitle}</h1>
          <div className={classes.subtitle}>
            <div>{fairDateString}</div>
            <div className={classes.place}>
              ETH Zurich, CLA + LEE {intl.formatMessage({ id: 'frontpage.building' })}
            </div>
          </div>
        </div>
        <nav>
          <Menu className={classes.navigation}>
            {/* TODO: enable this menu item when the new fairguide is available. */}
            {
              <li className={classes.navigationItem}>
                <a href="/fairguide">
                  <FormattedMessage id="menu.fairguide" />
                </a>
              </li>
            }
            {/* <MenuItem
              className={classes.navigationItem}
              path="/cv-check"
              label="menu.cvCheck"
            /> */}
            <MenuItem
              className={classes.navigationItem}
              path="/companies"
              label="menu.companies"
            />
            <MenuItem
              className={classes.navigationItem}
              path="/supporting-program"
              label="menu.supportingProgram"
            />
            <MenuItem
              className={classes.navigationItem}
              path="/venue"
              label="menu.venue"
            />
            <MenuItem
              className={classes.navigationItem}
              path="/company-infos"
              label="menu.companyInfos"
            />
          </Menu>
        </nav>

        {Object.entries(businessPartners).length > 0 && (
          <p className={classes.business_description}>
            {intl.formatMessage({ id: 'frontpage.businessPartners' })}: <br />
            {Object.entries(businessPartners)
              .map(([, name]) => name)
              .join(', ')}
          </p>
        )}

        <p className={classes.description}>
          <FormattedMessage id="site.description" />
        </p>
      </div>
    </>
  )
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 3600s (1h).
export const getStaticProps = wrapper.getStaticProps(store => async () => {
  await store.dispatch(loadCurrentFair())

  return { props: {}, revalidate: 3600 }
})

export default IndexPage
