import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Icon } from 'semantic-ui-react'

import useCurrentFairCompanies from 'lib/hooks/useCurrentFairCompanies'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import { requireDataLoaded } from 'lib/utils/api'
import Layout from 'lib/components/layout'
import { wrapper } from 'lib/store/createStore'
import { loadCurrentFair } from 'lib/store/slices/currentFairSlice'
import { loadCurrentFairCompanies } from 'lib/store/slices/currentFairCompaniesSlice'
import classes from './companies.module.less'

const CVCheckPage = () => {
  const currentFairItems = useCurrentFair()
  const currentFairCompaniesItems = useCurrentFairCompanies()

  let content = requireDataLoaded([currentFairItems, currentFairCompaniesItems])

  if (!content) {
    // TODO: update the URLs for CV Check and CV Photos if needed.
    const cvCheckUrl = 'https://cv-check.amiv.ethz.ch'
    const cvPhotosUrl =
      'https://calendly.com/izedin/amiv-contact-bewerbungsfotos'

    content = (
      <>
        <h2>
          <FormattedMessage id="companies.cvCheck.title" />
        </h2>
        <div>
          <FormattedMessage id="companies.cvCheck.text1" />
        </div>
        <div>
          <a target="_blank" rel="noopener noreferrer" href={cvCheckUrl}>
            <Icon name="long arrow alternate right" />
            <FormattedMessage id="companies.cvCheck.link" />
          </a>
        </div>
        <div className={classes.cvPhotos}>
          <FormattedMessage id="companies.cvCheck.text2" />
        </div>
        <div>
          <a target="_blank" rel="noopener noreferrer" href={cvPhotosUrl}>
            <Icon name="long arrow alternate right" />
            <FormattedMessage id="companies.cvCheck.link" />
          </a>
        </div>
      </>
    )
  }

  return <Layout>{content}</Layout>
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 3600s (1h).
export const getStaticProps = wrapper.getStaticProps(store => async () => {
  await store.dispatch(loadCurrentFair())
  await store.dispatch(loadCurrentFairCompanies())

  return { props: {}, revalidate: 3600 }
})

export default CVCheckPage
