import React, { useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FormattedMessage, useIntl } from 'react-intl'
import { Button, Message, Input } from 'semantic-ui-react'
import { useDispatch } from 'react-redux'
import { validate as validateEmail } from 'email-validator'

import useAuth from 'lib/hooks/useAuth'
import { login } from 'lib/store/slices/authSlice'
import ResourceState from 'lib/store/resourceState'
import Layout from 'lib/components/layout'
import Contact from 'lib/components/contact'
import EmailLink from 'lib/components/emailLink'
import classes from './login.module.less'

const LoginPage = () => {
  const [isAuthenticated, , state, error] = useAuth()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()
  const router = useRouter()
  const intl = useIntl()

  const isEmailValid = email.length > 0 && validateEmail(email)
  const isPasswordValid = password.length > 0
  const isFormValid = isEmailValid && isPasswordValid

  if (isAuthenticated) {
    router.push('/company-portal')
  }

  const handleSubmit = e => {
    e.preventDefault()
    if (state !== ResourceState.Loading && isFormValid) {
      dispatch(login(email, password))
    }
  }

  return (
    <Layout>
      <h1>
        <FormattedMessage id="login.title" />
      </h1>
      <Message warning size="small">
        <FormattedMessage
          id="login.hint"
          values={{ contactEmail: <EmailLink /> }}
        />
      </Message>
      {state === ResourceState.Error && (
        <Message negative size="small">
          <FormattedMessage
            id={`login.error${error.code}`}
            defaultMessage={
              <FormattedMessage
                id="errorUnknown"
                values={{ code: error.code, contactEmail: <EmailLink /> }}
              />
            }
          />
        </Message>
      )}

      <div>
        <form>
          <div className={classes.formField}>
            <Input
              type="email"
              required
              value={email}
              error={email.length > 0 && !isEmailValid}
              onChange={e => setEmail(e.target.value)}
              placeholder={intl.formatMessage({ id: 'email' })}
            />
          </div>
          <div className={classes.formField}>
            <Input
              type="password"
              required
              value={password}
              error={password.length > 0 && !isPasswordValid}
              onChange={e => setPassword(e.target.value)}
              placeholder={intl.formatMessage({ id: 'password' })}
            />
          </div>
          <Button
            primary
            loading={state === ResourceState.Loading}
            disabled={!isFormValid}
            onClick={handleSubmit}
          >
            <FormattedMessage id="login.button" />
          </Button>
          &nbsp;
          <Link href="/password-forgotten">
            <a className="button">
              <FormattedMessage id="login.passwordForgotten" />
            </a>
          </Link>
        </form>
      </div>
      <Contact />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default LoginPage
