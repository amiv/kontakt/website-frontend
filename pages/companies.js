import React from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import { Form, Icon } from 'semantic-ui-react'

import useCurrentFairCompanies from 'lib/hooks/useCurrentFairCompanies'
import useCurrentFair from 'lib/hooks/useCurrentFair'
import { requireDataLoaded } from 'lib/utils/api'
import Layout from 'lib/components/layout'
import CompanyLogo from 'lib/components/companyLogo'
import { wrapper } from 'lib/store/createStore'
import { loadCurrentFair } from 'lib/store/slices/currentFairSlice'
import { loadCurrentFairCompanies } from 'lib/store/slices/currentFairCompaniesSlice'
import classes from './companies.module.less'

const CompaniesPage = () => {
  const currentFairItems = useCurrentFair()
  const currentFairCompaniesItems = useCurrentFairCompanies()
  const intl = useIntl()

  const [currentFair] = currentFairItems
  const [currentFairCompanies] = currentFairCompaniesItems

  const dateFormatOptions = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    weekday: 'long',
  }

  let content = requireDataLoaded([currentFairItems, currentFairCompaniesItems])

  if (!content) {
    // TODO: update the URLs for CV Check and CV Photos if needed.
    const cvCheckUrl = 'https://cv-check.amiv.ethz.ch'
    const cvPhotosUrl = 'https://izedin.ch/amiv/'

    // TODO uncomment this to show CV check
    content = (
      // Placeholder until companies are known. Comment out, and uncomment following block for actual companies
      <p>
        <FormattedMessage
          id="companies.announcement"
          values={{ fairName: currentFair?.kontakt_title || 'Kontakt' }}
        />
        <br />
        <br />
        <FormattedMessage id="companies.fairguide_announcement" />
      </p>
      /* <>
        <h2>
          <FormattedMessage id="companies.cvCheck.title" />
        </h2>
        <div>
          <FormattedMessage id="companies.cvCheck.text1" />
        </div>
        <div>
          <a target="_blank" rel="noopener noreferrer" href={cvCheckUrl}>
            <Icon name="long arrow alternate right" />
            <FormattedMessage id="companies.cvCheck.link" />
          </a>
        </div>
        <div className={classes.cvPhotos}>
          <FormattedMessage id="companies.cvCheck.text2" />
        </div>
        <div>
          <a target="_blank" rel="noopener noreferrer" href={cvPhotosUrl}>
            <Icon name="long arrow alternate right" />
            <FormattedMessage id="companies.cvCheck.link" />
          </a>
        </div>
        <h2>{intl.formatDate(currentFair.date_one, dateFormatOptions)}</h2>
        <div className={classes.companies}>
          {currentFairCompanies.FIRST.map(item => (
            <CompanyLogo
              key={`${item.profile_id}_${item.booth}`}
              boothLocation={item.booth}
              profileId={item.profile_id}
              name={item.company_name}
              logo={item.logo}
              website={item.website}
            />
          ))}
        </div>
        <h2>{intl.formatDate(currentFair.date_two, dateFormatOptions)}</h2>
        <div className={classes.companies}>
          {currentFairCompanies.SECOND.map(item => (
            <CompanyLogo
              key={`${item.profile_id}_${item.booth}`}
              boothLocation={item.booth}
              profileId={item.profile_id}
              name={item.company_name}
              logo={item.logo}
              website={item.website}
            />
          ))}
        </div>
      </> */
    )
  }

  return (
    <Layout>
      <h1>
        <FormattedMessage id="companies.title" />
      </h1>
      {/* TODO: Comment the section below to remove the announcement text. */}
      {/*
        <p>
          <FormattedMessage
            id="companies.announcement"
            values={{ fairName: currentFair?.kontakt_title || 'Kontakt' }}
          />
        </p>
    */}
      {/*
        TODO: Uncomment the section below and update its content to show
              the list of participating companies.
              Please note that additional commented lines at the beginning
              of this document must also be uncommented!
        */}
      {content}
    </Layout>
  )
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 3600s (1h).
export const getStaticProps = wrapper.getStaticProps(store => async () => {
  await store.dispatch(loadCurrentFair())
  await store.dispatch(loadCurrentFairCompanies())

  return { props: {}, revalidate: 3600 }
})

export default CompaniesPage
