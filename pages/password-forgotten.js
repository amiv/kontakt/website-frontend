import React, { useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FormattedMessage, useIntl } from 'react-intl'
import { Button, GridRow, Message, Input } from 'semantic-ui-react'
import { useDispatch, useSelector } from 'react-redux'
import { validate as validateEmail } from 'email-validator'

import { selectCSRFToken } from 'lib/store/slices/authSlice'
import { makeApiRequest } from 'lib/utils/api'
import Layout from 'lib/components/layout'
import Contact from 'lib/components/contact'
import EmailLink from 'lib/components/emailLink'
import classes from './password-forgotten.module.less'

const PasswordForgottenPage = () => {
  const [isBusy, setIsBusy] = useState(false)
  const [error, setError] = useState(null)
  const [success, setSuccess] = useState(false)
  const [email, setEmail] = useState('')
  const csrfToken = useSelector(selectCSRFToken)
  const dispatch = useDispatch()
  const router = useRouter()
  const intl = useIntl()

  const isEmailValid = email.length === 0 || validateEmail(email)

  const handleSubmitClick = e => {
    e.preventDefault()

    if (isBusy || !isEmailValid) return

    setIsBusy(true)

    const action = {
      method: 'POST',
      resource: 'auth/request_password_reset',
      data: { email },
    }
    makeApiRequest(action, csrfToken, dispatch)
      .then(() => {
        setSuccess(true)
        setError(null)
      })
      .catch(err => {
        setError({
          code: err.response ? err.response.status : 503,
          ...(err.response ? err.response.data : {}),
        })
      })
      .finally(() => {
        setIsBusy(false)
      })
  }

  let content

  if (success) {
    content = (
      <React.Fragment>
        <GridRow>
          <Message positive size="small" className={classes.message}>
            <FormattedMessage
              id="passwordForgotten.success"
              values={{ email }}
            />
          </Message>
        </GridRow>
        <Button className="button" onClick={() => router.push('/login')}>
          <FormattedMessage id="backToLogin" />
        </Button>
      </React.Fragment>
    )
  } else {
    content = (
      <React.Fragment>
        {error && (
          <GridRow>
            <Message negative size="small" className={classes.message}>
              <FormattedMessage
                id={`passwordForgotten.error${error.code}`}
                defaultMessage={
                  <FormattedMessage
                    id="errorUnknown"
                    values={{ code: error.code, contactEmail: <EmailLink /> }}
                  />
                }
              />
            </Message>
          </GridRow>
        )}
        <div>
          <form>
            <div className={classes.formField}>
              <Input
                type="email"
                required
                value={email}
                error={!isEmailValid}
                onChange={e => setEmail(e.target.value)}
                placeholder={intl.formatMessage({ id: 'email' })}
              />
            </div>
            <Button
              primary
              loading={isBusy}
              disabled={!isEmailValid}
              onClick={handleSubmitClick}
            >
              <FormattedMessage id="passwordForgotten.requestPasswordReset" />
            </Button>
            &nbsp;
            <Link href="/login">
              <a className="button">
                <FormattedMessage id="backToLogin" />
              </a>
            </Link>
          </form>
        </div>
      </React.Fragment>
    )
  }

  return (
    <Layout>
      <h1>
        <FormattedMessage id="passwordForgotten.title" />
      </h1>
      {content}
      <Contact />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default PasswordForgottenPage
