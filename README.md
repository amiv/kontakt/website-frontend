# AMIV Kontakt Website Frontend

This is the home of the amiv Kontakt website.

## 🚀 Quick start

1. **Development**

   Navigate into the project directory, install all dependencies and run the development server using the following commands:

   ```shell
   # For initializing the project. Only required once.
   yarn install
   yarn run semantic:build
   # For starting the development server
   yarn run dev
   ```

   You can visit the website now at http://localhost:3000

   **IMPORTANT:** You also have to run the backend project on your machine! It is not possible to use an externally hosted backend.

2. **Development with Devcontainer (VSCode)**

   You can run the prepared development environment with docker and VSCode without the need to install anything project-specific on your system.

   Requirements:

   - _(Windows only)_ [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/)
   - [Docker Engine](https://docs.docker.com/engine/install/)
   - [Visual Studio Code](https://code.visualstudio.com/)(VSCode)
   - [Remote Development Extension Pack for VSCode](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)

   Open the project directory with the prepared devcontainer by clicking the green button in the left button corner of the VSCode window and select `Remote-Containers: Open Folder in Container...`.

   On startup, `yarn install && yarn run semantic:build` is run automatically. You can start the next development process with `yarn run dev`.

   You can visit the website now at http://localhost:3000

   **IMPORTANT:** You also have to run the backend project on your machine! It is not possible to use an externally hosted backend.

3. **Linting & Formatting**

   We use `eslint` and `prettier` for linting and formatting of the javascript code. You can use the following commands:

   ```shell
   npm run eslint
   npm run format
   ```

4. **Modify Semantic-UI/Fomantic-UI Theme**

   We initially used `semantic-ui` for our components. Because the project is not maintained anymore, we switched to the fork of the project named `fomantic-ui`. Everything is still called `semantic` within the package.

   If you do any changes to the theme (in `semantic/src/`), you have to run `yarn run semantic:build` to update the CSS files.

5. **Production Build**

   You can prepare a production build manually and serve it using next's built-in webserver. This will run the server-side rendering part.

   It is very possible, that some things do not work correctly when ssr is used, so you should definitively test this one before deploying a new version!

   ```shell
   yarn run build
   yarn run start
   ```

   You can analyze the content of the JavaScript bundle by running the following command instead:

   ```shell
   ANALYZE=true yarn run build
   ```

   For more information about deployment, read the section about deployment below.

## 🧐 What's inside?

A quick look at the most important files and directories of the project.

```ascii
.
├── next.config.js
├── config.js
├── public/
├── pages/
├── locales/
├── semantic/
└── lib/
    ├── components
    ├── hooks
    ├── store
    └── utils
```

1. **`next.config.js`**: This is the main configuration file for the Next.js website. See [next.config.js Documentation](https://nextjs.org/docs/api-reference/next.config.js/introduction) for details.

2. **`config.js`**: This is a custom configuration file for static values which should be modifiable very easily.

3. **`public/`**: This directory contains all static files such as images, font files or other documents.

4. **`pages/`**: This directory contains files which are converted to routes by next.js. See [Routing Documentation](https://nextjs.org/docs/routing/introduction) for details.

5. **`locales/`**: This directory contains all translations of texts as json files.

6. **`semantic/`**: Contains a custom theme for semantic-ui.

7. **`lib/`**: This directory contains all source code not converted into pages/routes by next.js.

   1. **`lib/components`**: Contains all components used somewhere on the website.
   2. **`lib/hooks`**: Custom React hooks.
   4. **`lib/store`**: Contains source files related to data handling using `react-redux`.
   5. **`lib/utils`**: Contains source files providing various utility functions.

## 💫 Deployment

The environment variable `NEXT_PUBLIC_API_URL` should be set according to the deployment setup!

: Add information about the deployment process of the project.

## Troubleshoot

Here you can find common issues and possible solutions.
Please add things here if you encounter an issue not yet described below.

1. **Translations are not updated.**

   _Just delete the `.next` folder and restart the next develop process._

## ⚙ Technologies

### Frontend Frameworks & Libraries

- [next.js](https://nextjs.org/)
- [React](https://reactjs.org/)
- [react-intl](https://github.com/formatjs/react-intl)
- [React-Redux](https://react-redux.js.org/)

### Backend

*For local development, you should also run an instance of the backend service!*

- [AMIV Kontakt Website Backend](https://gitlab.ethz.ch/amiv/kontakt/website-backend)

### Build Tools

- [yarn](https://yarnpkg.com/)
- [gatsby-cli](https://www.npmjs.com/package/gatsby-cli)

### Development Tools

- [ESlint](https://github.com/eslint/eslint)
- [Prettier](https://github.com/prettier/prettier)

  Most IDEs have plugins for those tools. VS Code is the recommended IDE.
