# This Dockerfile uses a multi-stage build. Only the first stage requires
# all dependencies, the final image will contain only the output files

# --------------------------------------------------------------------------
# First stage: Build project
FROM node:16-alpine AS build

ENV NODE_ENV production

WORKDIR /app

## Copy package.json and install dependencies 
#  > Please note that `production` is set to `false` so that devDependencies
#  > are installed too.
COPY package.json ./
COPY yarn.lock ./
COPY postinstall.sh ./
RUN yarn install --ignore-scripts --frozen-lockfile --production=false --cache-folder .yarn \
  && yarn run postinstall

## Copy source files
COPY ./ ./


## Run production build and disable telemetry
RUN yarn run build
RUN npx next telemetry disable

# --------------------------------------------------------------------------
# Second stage: Base image with installed node dependencies
FROM node:16-alpine as base

ENV NODE_ENV production

WORKDIR /app
COPY --from=build /app/package.json ./
COPY --from=build /app/yarn.lock ./
COPY --from=build /app/postinstall.sh ./
COPY --from=build /app/.yarn ./.yarn

## Install dependencies to run the next server
RUN yarn install --ignore-scripts --frozen-lockfile --production=true --cache-folder .yarn \
    && yarn run postinstall

# --------------------------------------------------------------------------
# Third stage: Server to deliver website
FROM node:16-alpine

ENV NODE_ENV production
ENV PORT 3000

## Install bash and curl which are needed by the start-up script
RUN apk add --no-cache bash curl

## Copy files from other stages
WORKDIR /app
COPY --from=base /app/package.json ./
COPY --from=base /app/yarn.lock ./
COPY --from=base /app/node_modules ./node_modules
COPY --chown=node:node --from=build /app/.next ./.next
COPY --from=build /app/public ./public
COPY next.config.js ./
COPY config.js ./

## Add start-up script
COPY docker-entrypoint.sh /usr/local/bin/

EXPOSE 3000
USER node

## Default run command: starts the Next.js server
CMD [ "docker-entrypoint.sh" ]
