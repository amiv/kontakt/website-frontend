/** Public website runtime configuration */

module.exports = {
  /**
   * Dictionary with profile IDs, name and (optional) custom logo URL
   * of our premium partners.
   * `logoUrl` is optional. Will use the logo provided by the company
   * profile by default.
   * THIS DICT HAS TO BE UPDATED EVERY YEAR!
   *
   *
   */
  premiumPartners: {
    /* 1337: {
      name: 'Axpo',
    },
    1347: {
      name: 'ABB Schweiz AG',
    },
    1353: {
      name: 'Endress+Hauser',
    },
    1345: {
      name: 'BELIMO Automation AG',
    },
    1325: {
      name: 'MAN Energy Solutions Switzerland Ltd.',
    }, */
  },
  /**
   *
   * Dictionary with profile IDs and name (shown on frontpage) of our business partners.
   * THIS DICT HAS TO BE UPDATED EVERY YEAR!
   */
  businessPartners: {
    // 1335: 'Helbling Business Advisors AG & Helbling Technik AG',
    // 1320: 'Leica Geosystems - part of Hexagon',
  },

  /**
   * Dictionary to override the database-provided logo
   * with a custom image (e.g. if there is no SVG logo
   * available or the website and/or the fairguide needs
   * some minor adjustments that it looks good).
   * Place the logos in `./public/images/logos` and use
   * the profile ID as the key in the dictionary.
   * Please make sure to remove unused images again.
   * THIS DICT HAS TO BE UPDATED EVERY YEAR!
   */
  profileLogoOverride: {
    // 101: '/images/logos/sunflower_labs.svg',
    // 196: '/images/logos/linde.png',
    // 193: '/images/logos/dufour_aerospace.jpg',
  },

  /**
   * Website metadata
   * Title and Description are translated automatically.
   * To change them, see directory `./locales`.
   */
  title: 'site.title',
  description: 'site.description',
  author: 'AMIV an der ETH, Sandro Lutz',

  /**
   * Email address for contacting the organizers.
   */
  contactEmail: 'kontaktmesse@amiv.ethz.ch',

  /**
   * Url for the backend/api.
   */
  apiUrl: process.env.NEXT_PUBLIC_API_DOMAIN
    ? `https://${process.env.NEXT_PUBLIC_API_DOMAIN}`
    : '/api',

  /**
   * Specifies whether the backend/api is accessible on the same domain as the website itself.
   */
  apiIsSameOrigin: !process.env.NEXT_PUBLIC_API_DOMAIN,

  /**
   * Url for the contractor primarily used for downloading the fairguide preview.
   */
  contractorUrl: `https://${
    process.env.NEXT_PUBLIC_CONTRACTOR_DOMAIN || 'contractor-dev.amiv.ethz.ch'
  }`,
}
